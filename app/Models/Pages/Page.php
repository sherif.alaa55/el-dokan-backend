<?php

namespace App\Models\Pages;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = [
        "slug",
        "title_en",
        "title_ar",
        "content_ar",
        "content_en",
        "image_en",
        "image_ar",
        "active",
    ];
    public static $validation = [
        "active" => "sometimes|nullable|in:0,1",
        "title_en" => "required|string",
        "title_ar" => "required|string",
        "content_ar" => "required|string",
        "content_en" => "required|string",
        "image_en" => "sometimes|nullable|string",
        "image_ar" => "sometimes|nullable|string",
    ];
    protected $casts = [
        'active' => 'integer',
    ];

    public function getTitle()
    {
        if (request()->header('lang') == 2) {
            return $this->title_ar ?: $this->title_en;
        }
        return $this->title_en;
    }

    public function getContent()
    {
        if (request()->header('lang') == 2) {
            return $this->content_ar ?: $this->content_en;
        }
        return $this->content_en;
    }

    public function getImage()
    {
        if (request()->header('lang') == 2) {
            return $this->image_ar ?: $this->image_en;
        }
        return $this->image_en;
    }


}
