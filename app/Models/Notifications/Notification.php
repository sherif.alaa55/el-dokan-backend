<?php

namespace App\Models\Notifications;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    
    protected $fillable = ["title", "body", "user_id", "type", "item_id", "image","details"];

    const ORDERTYPE = 1;

    public function getDetailsAttribute($value) {
        $attribute = $value ;
        if ($this->type == 10){
            $attribute = json_decode($value);
        }elseif ($this->type == 11){
            $attribute =["link"=>$value];
        }
        return $attribute;
    }
}
