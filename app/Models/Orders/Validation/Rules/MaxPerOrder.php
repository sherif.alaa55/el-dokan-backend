<?php

namespace App\Models\Orders\Validation\Rules;

use App\Models\Orders\OrderProduct;
use App\Models\Orders\OrderState;
use App\Models\Orders\Validation\ValidationError;
use App\Models\Products\Product;
use Facades\App\Models\Repositories\CartRepository;
use Illuminate\Support\Facades\DB;

class MaxPerOrder implements RulesInterface
{
	public $name = "schedule_date";
	private $order_data;
	private $user;
    private $lang;

	public function __construct($order_data, $user, $lang = 1)
	{
		$this->order_data = $order_data;
		$this->user = $user;
        $this->lang = $lang;
	}

    public function validate()
    {
        $items = CartRepository::getUserCartItems();
        foreach ($items as $item) {
            $product = Product::findOrFail($item["id"]);
            if ($product) {
                $user = $this->user;
                $order_item = OrderProduct::with("order", "product")->select("*", DB::raw("SUM(amount) as sale_count"))->whereHas("order", function ($q) use ($user) {
                    $q->whereNotIn("state_id", [OrderState::CANCELLED, OrderState::INCOMPLETED])->where("user_id", $user->id);
                })->where("product_id", $item["id"])->where("created_at", ">", now()->subDays($product->min_days))->groupby("product_id")->first();

                $total_qty = $item["amount"] + ($order_item ? $order_item->sale_count : 0);

                if ($total_qty > $product->parent->max_per_order && $product->parent->max_per_order != null) {
                    return new ValidationError(trans("mobile.errorMaxProduct", ["product" => $product->getName($this->lang), "quantity" => $product->parent->max_per_order]), 423);
                }

            }
        }
    }
}
