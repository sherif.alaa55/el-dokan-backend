<?php

namespace App\Models\Orders\Validation\Rules;

use App\Models\Orders\Validation\ValidationError;
use Facades\App\Models\Services\OrdersService;
use App\Models\Settings\Settings;

class ExceptCodAmount implements RulesInterface
{
	public $name = "schedule_date";
	private $order_data;
	private $user;

	public function __construct($order_data, $user)
	{
		$this->order_data = $order_data;
		$this->user = $user;
	}

    public function validate()
    {
        $request = request();
        $settings = Settings::getSystemSettings();
        $totalAmount = OrdersService::getTotalAmount($request->all());
        if (!is_null($settings->except_cod_amount) && ($totalAmount / 100) >= $settings->except_cod_amount && $request->payment_method == 1) {
            return new ValidationError("Cash method allowed for orders with total lower than {$settings->except_cod_amount}", 423);
        }
    }
}
