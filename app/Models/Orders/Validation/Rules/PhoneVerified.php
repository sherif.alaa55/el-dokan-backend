<?php

namespace App\Models\Orders\Validation\Rules;

use App\Models\Orders\Validation\ValidationError;

class PhoneVerified implements RulesInterface
{
	public $name = "phone_exists";
	private $order_data;
	private $user;

	public function __construct($order_data, $user)
	{
		$this->order_data = $order_data;
		$this->user = $user;
	}

    public function validate()
    {
    	if (!$this->user->phone_verified) {
    		return new ValidationError(trans('mobile.errorUpdateApp'), 409);
    	}
    }
}
