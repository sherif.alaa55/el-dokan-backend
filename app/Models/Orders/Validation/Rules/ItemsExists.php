<?php

namespace App\Models\Orders\Validation\Rules;

use App\Models\Orders\Validation\ValidationError;
use Facades\App\Models\Repositories\CartRepository;
use Illuminate\Support\Facades\Auth;

class ItemsExists implements RulesInterface
{

    public function validate()
    {
        $items = CartRepository::getUserCartItems();
        if (!count($items)) {
    		return new ValidationError(trans('mobile.errorItemsNotExists'), 422);
    	}
    }
}
