<?php

namespace App\Models\Orders;

use App\Models\Orders\OrderSchedule;
use App\Models\Payment\Invoice;
use App\Models\Products\Product;
use App\Models\Users\Address;
use App\Models\Users\Deliverer;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class Transaction extends Model
{
    protected $fillable = ["order_details", "order_pay_id", "card_info","transaction_response","transaction_status","total_amount","customer_id", "user_agent"];
    protected $casts = [
        "order_details"=>"json",
        "transaction_response"=>"json"
    ];
    public function customer()
    {
    	return $this->belongsTo(User::class, "customer_id");
    }
}
