<?php

namespace App\Models\Orders;

use App\Models\Orders\OrderSchedule;
use App\Models\Payment\Invoice;
use App\Models\Products\Product;
use App\Models\Shipping\OrderPickup;
use App\Models\Users\Address;
use App\Models\Users\Deliverer;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    const DELIVERY_FEES = 15;
    protected $appends = ["payment_method_name"];
    protected $fillable = ["user_id", "payment_method", "address_id", "state_id", "cancellation_id", "cancellation_text", "notes", "scheduled_at", "admin_notes","transaction_id","source","phone","admin_id","user_agent"];

    public function state()
    {
    	return $this->belongsTo(OrderState::class, "state_id");
    }
    public function order_pickup()
    {
        return $this->hasOne(OrderPickup::class, "order_id")->orderBy("created_at", "DESC");
    }
    public function transaction()
    {
        return $this->hasOne(Transaction::class, 'id',"transaction_id");
    }

    public function order_pickups()
    {
        return $this->hasMany(OrderPickup::class, "order_id");
    }

    public function sub_state()
    {
        return $this->belongsTo(OrderState::class, "sub_state_id");
    }

    public function CancellationReason()
    {
        return $this->belongsTo(OrderCancellationReason::class, "cancellation_id");
    }

    public function customer()
    {
    	return $this->belongsTo(User::class, "user_id");
    }
    public function admin()
    {
        return $this->belongsTo(User::class, "admin_id");
    }

    public function deliverer()
    {
    	return $this->belongsTo(Deliverer::class, "deliverer_id");
    }

    public function items()
    {
        return $this->hasMany(OrderProduct::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, "order_products", "order_id", "product_id");
    }

    public function history()
    {
        return $this->hasMany(OrderHistory::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class, "address_id")->withTrashed();
    }

    public function invoice()
    {
        return $this->hasOne(Invoice::class, "order_id");
    }

    public function schedule()
    {
        return $this->hasOne(OrderSchedule::class);
    }

    public function reorders()
    {
        return $this->hasMany(self::class, "parent_id");
    }

    public function parentOrder()
    {
        return $this->belongsTo(self::class, "parent_id");
    }

    public function isActive()
    {
        return !in_array($this->state_id, [4, 6, 7, 9]);
    }

    public function getTotal()
    {
        $items = $this->items()->with("product", "price")->where("missing", "!=", 1)->get();

        return $items->reduce(function ($carry, $item) {
            if (!is_null($item->discount_price)) {
                return $carry + ($item->discount_price);
            }

            return $carry + ($item->price);
        });
    }

    public function getOrderWightAttribute(){
        $attributes = 0;
        $orderItems =  $this->items;
        foreach ($orderItems as $item){
            $product = $item->product;
            $attributes += $product->weight * $item->amount;
        }
        return $attributes;
    }
    public function getOrderNumberOfPiscesAttribute(){
        $attributes = 0;
        $orderItems =  $this->items;
        foreach ($orderItems as $item){
            $product = $item->product;
            $attributes += $product->weight * $item->amount;
        }
        return $attributes;
    }
    public function getStates($lang = 1)
    {
        $processingLabel = 0;
        $deliveringLabel = 0;
        $deliveredLabel = 0;
        $preparedLabel = 0;

        if ($this->state_id == OrderState::DELIVERED) {
            $processingLabel = 1;
            $deliveringLabel = 1;
            $deliveredLabel = 1;
            $preparedLabel = 1;
        } elseif ($this->state_id == OrderState::ONDELIVERY) {
            $processingLabel = 1;
            $deliveringLabel = 1;
            $preparedLabel = 1;
        } elseif ($this->state_id == OrderState::PREPARED) {
            $processingLabel = 1;
            $preparedLabel = 1;
        } elseif ($this->state_id == OrderState::PROCESSING) {
            $processingLabel = 1;
        }

        $created_state = OrderState::find(OrderState::CREATED);
        $prepared_state = OrderState::find(OrderState::PREPARED);
        $processing_state = OrderState::find(OrderState::PROCESSING);
        $delivering_state = OrderState::find(OrderState::ONDELIVERY);
        $delivered_state = OrderState::find(OrderState::DELIVERED);

        return [
            [
                "id" => 1,
                "name" => $created_state->getName($lang)
            ],
            [
                "id" => $processingLabel,
                "name" => $processing_state->getName($lang)
            ],
            [
                "id" => $preparedLabel,
                "name" => $prepared_state->getName($lang)
            ],
            [
                "id" => $deliveringLabel,
                "name" => $delivering_state->getName($lang)
            ],
            [
                "id" => $deliveredLabel,
                "name" => $delivered_state->getName($lang)
            ]
        ];
    }

    public function getArea()
    {
        return $this->address->area;
    }

    public function createSchdule($scheduleData)
    {
        $schedule = $this->schedule()->create([
            "interval" => $scheduleData["interval"],
            "time" => $scheduleData["time"]
        ]);

        if ($scheduleData["interval"] == OrderSchedule::WEEKLY || $scheduleData["interval"] == OrderSchedule::MONTHLY) {
            foreach ($scheduleData["days"] as $day) {
                $schedule->days()->create(["day" => $day]);
            }
        }
    }
    public function getPaymentMethodNameAttribute()
    {
        $attribute = null;
        switch ($this->payment_method) {
            case 1:
                $attribute = "CASH";
                break;
            case 2:
                $attribute = "VISA";
                break;
            case 3:
                $attribute = "VALU";
                break;
            case 4:
                $attribute = "FAWRY";
                break;
        }
        return $attribute;
    }
}
