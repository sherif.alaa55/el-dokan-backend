<?php

namespace App\Models\Transformers;

use App\Http\Resources\Admin\ProductVariantResource;
use Facades\App\Models\Services\UploadService;
use Illuminate\Http\Request;

/**
 *
 */
class ProductFullTransformer extends Transformer
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    function transform($product)
    {
        $variants = $product->productVariants;
        $stock = $product->stock + $variants->sum('stock');

        return [
            "id" => $product->id,
            "name" => $product->name,
            "name_ar" => $product->name_ar,
            "description" => $product->description,
            "meta_title" => $product->meta_title,
            "meta_title_ar" => $product->meta_title_ar,
            "meta_description" => $product->meta_description,
            "meta_description_ar" => $product->meta_description_ar,
            "keywords" => $product->keywords,
            "preorder" => (boolean)$product->preorder,
            "preorder_price" => $product->preorder_price,
            "preorder_start_date" => $product->preorder_start_date,
            "preorder_end_date" => $product->preorder_end_date,
            "description_ar" => $product->description_ar,
            "long_description_ar" => $product->long_description_ar,
            "long_description_en" => $product->long_description_en,
            "image" => $product->image,
            "thumbnail_image" => $product->thumbnail,
            "last_editor" => $product->updator,
            "last_editor_id" => $product->last_editor,
            "video" => $product->video,
            "available_soon" => (boolean)$product->available_soon,
            "brand_id" => $product->brand_id,
            "images" => $product->images->map(function($image) {
                return [
                    "id" => $image->id,
                    "product_id" => $image->product_id,
                    "url" => $image->url,
                    "thumbnail" => UploadService::getImageThumbnailLink($image->url),
                ];
            }),
            "price" => $product->price,
            "discount_price" => ($product->active_discount || $product->promotion_id) ? $product->discount_price : null,
            "creator" => $product->creator,
            "created_at" => (string)$product->created_at,
            "category" => $product->getCategoryTree(),
            "optional_category" => $product->optional_sub_category_id ? $product->getOptionalCategoryTree() : null,
            "category_id" => $product->category_id,
            "optional_sub_category_id" => $product->optional_sub_category_id,
            "promotion" => isset($product->brand->activePromotion) ? $product->brand->activePromotion->getName($this->request->header("lang")) : null,
            "tags" => $product->tags,
            "options" => $product->adminAttributesWithVlues(),
            "product_variant_options" => $product->productVariantOptions()->with('values')->get()->unique(),
//            "product_variant_options" => $product->getAdminVariantOptionsList($product->parent_id ? [$product] : $variants),
            "product_variants" => ProductVariantResource::collection($product->ProductVariants),
            "active" => $product->active,
            "deactivation_notes" => $product->deactivation_notes,
            "sku" => $product->sku,
            "stock" => $stock,
            "weight" => $product->weight,
            "stock_alert" => $product->stock_alert,
            "rate" => $product->rate,
            "rates" => $product->allRates,
            "ratesAvg" => $product->ratesApprovedAvg(),
            "history" => $product->getHistory(),
            "bundleProduct" => $this->transformCollection($product->bundleProduct),
            "relatedProducts" => $this->transformCollection($product->relatedProducts),
            "type" => $product->type,
            "order" => $product->order,
            "max_per_order" => $product->max_per_order,
            "min_days" => $product->min_days,
            "bundle_checkout" => $product->bundle_checkout,
            "has_stock" => $product->has_stock
        ];
    }
}
