<?php

namespace App\Models\Transformers;

use Illuminate\Http\Request;
use App\Models\Payment\Promo;
use App\Models\Products\Product;
use Illuminate\Support\Facades\DB;
use App\Models\Repositories\CartRepository;
use Facades\App\Models\Services\UploadService;
use App\Http\Resources\Customer\GroupsResource;
use App\Http\Resources\Admin\ProductVariantResource;
use App\Http\Resources\Customer\GroupsWithoutSubCategoriesResource;

/**
*
*/
class ProductDetailTransformer extends Transformer
{
	private $request;
	private $cartRepo;

	public function __construct(Request $request, CartRepository $cartRepo)
	{
		$this->request = $request;
		$this->cartRepo = $cartRepo;
	}


	function transform($product)
	{
        $firstVariant = $product->productVariants->first() ?? $product;
        $variants = $product->availableProductVariants()->orderBy('order')->get();
        
        if ($product->parent_id) {
            $stock = $firstVariant->stock;
        } else {
            $stock = $variants->sum('stock');
        }

        $promos = Promo::whereNotNull('list_id')
            ->where('active', 1)
            ->whereDate('expiration_date', '>=', now())
            ->whereHas('list', function ($q) use ($product) {
                $q->whereHas('products', function ($q) use ($product) {
                    $q->where('products.id', $product->parent_id ? $product->parent->id : $product->id);
                });
            })->get();

        
        if (!$product->amount && 999999 !== auth()->id()) {
            $cartItems = $this->cartRepo->getUserCartItems();
            foreach ($cartItems as $item) {
                if ($item['id'] == $product->id) {
                    $product->amount = $item['amount'];
                }
            }
        }


		return [
            "id" => $product->id,
            "parent_id" => $product->parent_id,
            "sku" => $product->sku,
			"name" => $product->getName($this->request->header("lang")),
            "name_en" => $product->name,
            "name_ar" => $product->name_ar,
			"description" => $firstVariant->getDescription($this->request->header("lang")),
            "meta_title" => $firstVariant->getMetaTitle($this->request->header("lang")),
            "meta_description" => $firstVariant->meta_description,
            "keywords" => $firstVariant->keywords,
            "preorder" => (boolean) $product->active_preorder,
            "preorder_price" => $product->preorder_price,
            "long_description" => $firstVariant->getLongDescription($this->request->header("lang")),
			"image" => $firstVariant->image,
            "thumbnail_image" => $product->thumbnail,
            "video" => $product->video,
            "available_soon" => (boolean)($firstVariant->parent_id ? $firstVariant->parent->available_soon : $firstVariant->available_soon),
			"related_skus" => $product->relatedSkus->pluck('sku')->toarray(),
			"price" => (double)$firstVariant->price,
            "discount_price" => ($firstVariant->active_discount || $product->promotion_id) ? (double)$product->discount_price : null,
            "direct_discount" => !is_null($product->getOriginal("discount_price")),
			"category" => $product->getCategoryTree($this->request->header("lang")),
            "optional_sub_category_id" => $product->optional_sub_category_id,
            "group" => isset($product->category->group[0]) ? new GroupsWithoutSubCategoriesResource($product->category->group[0]) : null,
			"details" => $this->getDetails($firstVariant),
            "in_stock" => $stock > 0,
			"stock" => $stock,
            "weight" => $product->weight,
			"is_favourite" => $product->is_favourite,
            "is_compare" => $product->is_compare,
            "promos" => $promos,
            // "promotion" => isset($product->brand->activePromotion) ? $product->brand->activePromotion->getName($this->request->header("lang")) : null,
			"share_link" => $product->getShareLink($this->request->header("lang")),
			"rate" => $product->rate,
            // "reviews" => $product->approvedRates,
            "order" => $product->order,
            // "bundleProduct" =>$this->transformCollection($product->bundleProduct),
            "type" => $product->type,
//            "product_variants_options" => $product->customerVariantOptionsLists($this->request->header('lang')),
            "product_variants_options" => $product->getVariantOptionsList($product->parent_id ? [$product] : $product->productVariants),
            "product_variants" => ProductVariantResource::collection($variants),
            // "reviewsAvg" => $product->ratesApprovedAvg(),
			"active" => (bool)$product->active,
			"amount" => $product->amount
		];
	}

    public function getDetails($product)
    {
        $details = [
            // "images" => $product->images->pluck("url")->toArray(),
            "description" => $product->getDescription($this->request->header("lang")),
            "brand" => $product->brand,
            "attributes" => $product->customerAttributesWithVlues($product->brand),
            // "tags" => $this->getTags($product)
        ];
        if(isset($product->is_favourite)){
            $details["is_favourite"] = $product->is_favourite;
        }
        return $details;
    }

    public function getTags($product)
    {
        $productTags = $product->tags->where('status',1);
        $finalData = [];
        foreach ($productTags as $productTag){
            if ($this->request->header("lang") == 2){
                $finalData[] = [
                    'name' => $productTag->name_ar,
                    'description'  => $productTag->description_ar
                ];
            }else{
                $finalData[] = [
                    'option' => $productTag->name_en,
                    'value'  => $productTag->description_en
                ];
            }
        }
        return $finalData;
    }
}
