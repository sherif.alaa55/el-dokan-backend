<?php

namespace App\Models\Transformers;

use Facades\App\Models\Transformers\ProductTransformer;

/**
*
*/
class CustomerFullTransformer extends Transformer
{

	function transform($user)
	{
        $cartItems = optional($user->cart()->with('cartItems.product')->first())->cartItems;
		return [
			"id" => $user->id,
			"name" => $user->name,
			"email" => $user->email,
			"phone" => $user->phone,
			"birthdate" => $user->birthdate,
            "cart_items" => optional($cartItems)->map(function ($item) {
                return ProductTransformer::transform($item->product) + ['cart_amount' => $item->amount];
            }),
            "orders" => $user->orders->load("products"),
			"active" => $user->active,
            "language" => $user->settings->language ?? null,
			"deactivation_notes" => $user->deactivation_notes,
			"addresses" => $user->addresses->load("area", "city"),
			"phone_verified" => $user->phone_verified,
			"closed_payment_methods" => $user->closedPaymentMethods
		];
	}
}
