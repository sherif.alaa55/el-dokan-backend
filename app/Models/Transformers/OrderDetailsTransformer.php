<?php

namespace App\Models\Transformers;

use App\Http\Resources\Admin\ShipmentResource;
use App\Models\Transformers\OrderItemTransformer;
use App\Http\Resources\Admin\OrderItemResource;
use App\Models\Payment\PaymentMethod;
use App\Http\Resources\Admin\PaymentMethodsResource;

class OrderDetailsTransformer extends Transformer
{
	private $itemsTrans;

	public function __construct(OrderItemTransformer $itemsTrans)
	{
		$this->itemsTrans = $itemsTrans;
	}


	function transform($order)
	{
		return [
			"id" => $order->id,
			"user" => $order->customer,
			"notes" => $order->notes,
			"phone" => $order->phone,
			"admin_notes" => $order->admin_notes,
			"admin_id" => $order->admin_id,
			"admin" => $order->admin,
			"state_id" => $order->state_id,
			"sub_state_id" => $order->sub_state_id,
			"states" => $order->getStates(1),
            "cancellation_id" => $order->cancellation_id,
            "cancellation_reason" => $order->CancellationReason,
            "cancellation_text" => $order->cancellation_text ?? optional($order->CancellationReason)->text,
            "invoice" => $order->invoice,
            "user_agent" => $order->user_agent,
            "paid_amount" => $order->invoice->paid_amount,
			"amount" => (!is_null($order->invoice->discount) ? $order->invoice->discount : $order->invoice->total_amount),
			"actual_amount" => $order->invoice->total_amount,
			"delivery_fees" => $order->invoice->delivery_fees,
			"discount_amount" => $order->invoice->discount,
			"remaining" => $order->invoice->remaining ?: 0,
			"payment_method" => $order->payment_method,
			"payment_method_object" => new PaymentMethodsResource(PaymentMethod::find($order->payment_method)),
			"created_at" => (string)$order->created_at,
			"scheduled_at" => (string)$order->scheduled_at,
			"address" => $order->address,
			"items" => $this->itemsTrans->transformCollection($order->items->load("product")),
			"history" => $order->history->load("state", "sub_state", "user"),
			"rate" => $order->rate,
			"deliverer" => $order->deliverer ? $order->deliverer->load("delivererProfile") : null,
			"promo" => $order->invoice->promo,
			"updated_at" => (string)$order->updated_at,
			"feedback" => $order->feedback,
			"schedule" => $order->schedule ? $this->transformSchedule($order->schedule->load("days")) : null,
            "parent_id" => $order->parent_id,
            "transaction_id" => $order->transaction_id,
            "reorder_count" => $order->reorders()->count(),
            "is_shipped" => (boolean)$order->order_pickup,
			"shipped_data" => isset($order->order_pickup) ?  new ShipmentResource($order->order_pickup) : null,
			"shipments" => ShipmentResource::collection($order->order_pickups)
		];
	}

	private function transformSchedule($schedule)
    {
        return [
            "id" => $schedule->id,
            "interval" => $schedule->interval,
            "time" => $schedule->time,
            "days" => $this->transformDays($schedule->days)
        ];
    }

    private function transformDays($days)
    {
        return array_map(function ($item) {
            return $item["day"];
        }, $days->toArray());
    }
}
