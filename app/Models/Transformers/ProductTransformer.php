<?php

namespace App\Models\Transformers;

use App\Http\Resources\Admin\ProductVariantResource;
use App\Models\Payment\Promo;
use App\Models\Repositories\CartRepository;
use Illuminate\Http\Request;

class ProductTransformer extends Transformer
{
    protected $cartRepo;
    private $request;

    public function __construct(Request $request, CartRepository $cartRepo)
    {
        $this->request = $request;
        $this->cartRepo = $cartRepo;
    }

    public function transform($product)
    {
        $variants = $product->availableProductVariants()->orderBy('order')->get();

        $promos = Promo::whereNotNull('list_id')
            ->where('active', 1)
            ->whereDate('expiration_date', '>=', now())
            ->whereHas('list', function ($q) use ($product) {
                $q->whereHas('products', function ($q) use ($product) {
                    $q->where('products.id', $product->parent_id ? $product->parent->id : $product->id);
                });
            })->get();

        if (is_null($product->parent_id)) {
            $firstVariant = $product->productVariants()->orderBy("stock", "DESC")->first();
        } else {
            $firstVariant = $product;
        }

        if (!$firstVariant) {
            $firstVariant = $product;
        }
        if (null == $product->parent_id) {
            $stock = $variants->sum('stock');
        } else {
            $stock = $product->stock;
        }

        if (!$product->amount && auth()->id() !== 999999) {
            $cartItems = $this->cartRepo->getUserCartItems();
            foreach ($cartItems as $item) {
                if ($item['id'] == $product->id) {
                    $product->amount = $item['amount'];
                }
            }
        }

        return [
            'id' => $product->id,
            'parent_id' => $product->parent_id,
            'sku' => $product->sku,
            'name' => $product->getName($this->request->header('lang')),
            'name_en' => $product->name,
            'name_ar' => $product->name_ar,
            'slug' => $product->getSlug($this->request->header('lang')),
            'description' => $firstVariant->getDescription($this->request->header('lang')),
            'meta_title' => $firstVariant->meta_title,
            'meta_description' => $firstVariant->meta_description,
            'keywords' => $firstVariant->keywords,
            'preorder' => (bool) $product->active_preorder,
            'preorder_price' => $firstVariant->preorder_price,
            'preorder_start_date' => $product->preorder_start_date,
            'preorder_end_date' => $product->preorder_end_date,
            'image' => $firstVariant->image,
            'images' => $firstVariant->images,
            'thumbnail_image' => $product->thumbnail,
            'video' => $firstVariant->video,
            'available_soon' => (bool) ($firstVariant->parent_id ? $firstVariant->parent->available_soon : $firstVariant->available_soon),
            'price' => !is_null($firstVariant->price) ? (double)round($firstVariant->price, 2) : null,
            'discount_price' => ($firstVariant->active_discount || $firstVariant->promotion_id) ? (double)$firstVariant->discount_price : null,
            'discount_start_date' => $product->discount_start_date,
            'discount_end_date' => $product->discount_end_date,
            'direct_discount' => !is_null($product->getOriginal('discount_price')),
            'category' => $product->getCategoryTree($this->request->header('lang')),
            'optional_sub_category_id' => $product->optional_sub_category_id,
            'in_stock' => $stock > 0,
            'stock' => $stock,
            'promotion' => isset($product->brand->activePromotion) ? $product->brand->activePromotion->getName($this->request->header('lang')) : null,
            'is_favourite' => $product->is_favourite,
            'is_compare' => $product->is_compare,
            'share_link' => $product->getShareLink($this->request->header('lang')),
            'rate' => $product->rate,
            'promos' => $promos,
            // "options" => $product->customerAttributesWithVlues(),
            //"product_variants_options" => $product->customerVariantOptionsLists($this->request->header('lang')),
            'product_variants_options' => $product->getVariantOptionsList($product->parent_id ? [$product] : $product->productVariants),
            'product_variants' => ProductVariantResource::collection($variants),
            'order' => $product->order,
            'weight' => $product->weight,
            'active' => (bool) $product->active,
            // 'bundleProduct' => $this->transformCollection($product->bundleProduct),
            'type' => $product->type,
            'amount' => $product->amount,
            'variants_stock_count' => $product->variants_stock_count,
        ];
    }
}
