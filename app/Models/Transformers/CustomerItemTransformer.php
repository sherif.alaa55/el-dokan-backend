<?php 

namespace App\Models\Transformers;

/**
* 
*/
class CustomerItemTransformer extends Transformer
{
	
	function transform($item)
	{
		$price_instance = $item->price;
		$product = $item->product;
		return [
			"id" => $item->product->id,
			"amount" => $item->amount,
            "serial_number" => $item->serial_number,
			"sku" => $item->product->sku,
			"name" => $item->product->name,
			"image" => $item->product->image,
			"price" => (double)$item->price,
			"discount_price" => (double)$item->discount_price,
			"active" => (bool)$item->product->active,
			"rate" => $item->rate,
			"options" => $product->customerVariantOptionsLists(request()->header('lang')),
		];
	}
}