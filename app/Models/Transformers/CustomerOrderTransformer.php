<?php

namespace App\Models\Transformers;

use Illuminate\Http\Request;
use App\Models\Payment\PaymentMethod;
use App\Http\Resources\Customer\PaymentMethodsResource;

/**
*
*/
class CustomerOrderTransformer extends Transformer
{
	private $itemTrans;
	private $request;

	public function __construct(CustomerItemTransformer $itemTrans, Request $request)
	{
		$this->itemTrans = $itemTrans;
		$this->request = $request;
	}


	function transform($order)
	{
        $cancelReasonLang = $this->request->header('lang') == 2 ? 'text_ar' : 'text';

        return [
			"id" => $order->id,
			"date" => (string)$order->created_at,
			"payment_method" => $order->payment_method,
            "user_agent" => $order->user_agent,
            "state_id" => $order->state_id,
            "phone" => $order->phone,
			"state" => $order->state ? $this->getState($order->state, $this->request->header("lang")) : null,
            "cancellation_id" => $order->cancellation_id,
            "cancellation_reason" => $order->CancellationReason,
            "cancellation_text" => $order->cancellation_text ?? optional($order->CancellationReason)->{$cancelReasonLang},
            "items" => $this->itemTrans->transformCollection($order->items),
			"states" => $order->getStates($this->request->header("lang")),
			"rate" => $order->rate,
			"address" => $order->address,
			"notes" => $order->notes,
			"total" => ((!is_null($order->invoice->discount) ? $order->invoice->discount : $order->invoice->total_amount) + $order->invoice->delivery_fees),
			"item_total" => $order->invoice->total_amount,
            "delivery_fees" => $order->invoice->delivery_fees,
            "discount" => (!is_null($order->invoice->discount) ? $order->invoice->total_amount - $order->invoice->discount : 0),
			"active" => $order->isActive(),
			"scheduled_at" => $order->scheduled_at,
			"schedule" => $order->schedule ? $this->transformSchedule($order->schedule->load("days")) : null,
            "reorder_count" => $order->reorders()->count(),
            "feedback" => $order->feedback,
            "shipped_data" => isset($order->order_pickup) ? "https://www.aramex.com/us/en/track/results?mode=0&ShipmentNumber=" . $order->order_pickup->shipping_id : null,
            "fawry_ref" => $order->fawry_ref,
            "payment_method_object" => new PaymentMethodsResource(PaymentMethod::find($order->payment_method)),
		];
	}

	private function transformSchedule($schedule)
    {
        return [
            "id" => $schedule->id,
            "interval" => $schedule->interval,
            "time" => $schedule->time,
            "days" => $this->transformDays($schedule->days)
        ];
    }

    private function transformDays($days)
    {
        return array_map(function ($item) {
            return $item["day"];
        }, $days->toArray());
    }

    public function getState($state, $lang)
    {
    	return [
    		"id" => $state->id,
    		"name" => $state->getName($lang),
    	];
    }
}
