<?php 

namespace App\Models\Transformers;

/**
* 
*/
class OrderItemTransformer extends Transformer
{
	
	function transform($item)
	{
		$price_instance = $item->price;
		$product = $item->product;
		return [
			"id" => $item->product_id,
			"product" => $product,
			"sku" => $product->sku,
			"serial_number" => $item->serial_number,
			"amount" => $item->amount,
			"sub_category" => $product->parent_id ? $product->parent->category->name : $product->category->name,
			"category" => $product->parent_id ? $product->parent->category->parent->name : $product->category->parent->name,
			"image" =>  $product->image,
			"price" => $item->price,
			"discount_price" => $item->discount_price,
			"options" => $product->customerVariantOptionsLists(request()->header('lang')),
		];
	}
}