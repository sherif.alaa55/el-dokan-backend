<?php

namespace App\Models\Services;

use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

class SmsService
{
    
    public function __construct()
    {
        $this->client = new \GuzzleHttp\Client(["cookies" => true, 'defaults' => ['verify' => false]]);
    }

    public function sendSms($number, $message)
    {

        $sms = [
            "UserName" => env("VICTORYLINK_USER"),
            "Password" => env("VICTORYLINK_PASSWORD"),
            "SMSText" => $message,
            "SMSLang" => "e",
            "SMSSender" => env("VICTORYLINK_SENDER"),
            "SMSReceiver" => $number
        ];
        Log::info($sms);
        if (app()->environment() === 'production') {
            $res = $this->client->post('https://smsvas.vlserv.com/KannelSending/service.asmx/SendSMSWithDLR', [
                "form_params" => $sms
            ]);
            $response = $res->getBody();
            return $response;
        }else{
            return true;
        }


    }

    public function checkCredit()
    {
        $credentials = [
            "UserName" => env("VICTORYLINK_USER"),
            "Password" => env("VICTORYLINK_PASSWORD")
        ];

        $res = $this->client->post('https://smsvas.vlserv.com/KannelSending/service.asmx/CheckCredit', [
            "form_params" => $credentials
        ]);


        $response = simplexml_load_string((string)$res->getBody());

        return (string)$response;
    }
}
