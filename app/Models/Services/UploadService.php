<?php

namespace App\Models\Services;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class UploadService
{
    private $imageExtensions = ['jpg', 'jpeg', 'gif', 'png', 'bmp', 'svg', 'svgz', 'cgm', 'djv', 'djvu', 'ico', 'ief','jpe', 'pbm', 'pgm', 'pnm', 'ppm', 'ras', 'rgb', 'tif', 'tiff', 'wbmp', 'xbm', 'xpm', 'xwd'];

    private function uploadFile($file, $originalName = false)
    {
        $random = Str::random(6);
        if($originalName) {
            $fileName = preg_replace("/\s/", "-", $file->getClientOriginalName());
        } else {
            $fileName = "{$random}-" . time() . "." . $file->getClientOriginalExtension();
        }
        $filePath = Storage::disk('public')->putFileAs('uploads', $file, $fileName);
        if(in_array($file->getClientOriginalExtension(), $this->imageExtensions)) {
            $thumpPath = Storage::disk('public')->putFileAs('uploads/thumbnails', $file, $fileName);
            $img = Image::make($file)
                ->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->stream();
            Storage::disk('public')->put($thumpPath, $img);
            return ["filePath" => url(Storage::url($filePath)), "thumbnail" => url(Storage::url($thumpPath)), "name" => Storage::url($filePath)];
        }
        return ["filePath" => url(Storage::url($filePath)), "name" => Storage::url($filePath)];
    }

    public function upload(Request $request) {
        $request->validate([ "file" => "required" ]);
        return $this->uploadFile($request->file);
    }

    public function uploadFiles(Request $request)
    {
        $request->validate([ "files" => "required" ]);

        $files = [];
        foreach ($request->files->get("files") as $file) {
            $files[] = $this->uploadFile($file, true)['filePath'];
        }
        return $files;
    }

    public function getImageThumbnailLink($imageLink)
    {
        $imgArr = explode('/', $imageLink);
        $last = end($imgArr);
        array_pop($imgArr);
        $imgArr[] = 'thumbnails';
        $imgArr[] = $last;
        if(Storage::disk('public')->exists("uploads/thumbnails/{$last}")) {
            return implode('/', $imgArr);
        }
        return null;
    }

    public function getUploads(Request $request)
    {
        $request->validate([ "page" => "required|int" ]);

        $fullDirName = "public/uploads";
        $files = Storage::files($fullDirName);

        $files = array_map(function ($item) use ($fullDirName)
        {
            return [
                "name" => str_replace($fullDirName . "/" , "", $item),
                "url" => Url::to('') . Storage::url($item)
            ];
        }, $files);
        array_shift($files);
        return array_slice($files, (($request->page - 1) * 8), 8);
    }

}
