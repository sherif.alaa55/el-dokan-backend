<?php

namespace App\Models\Services;

use App\Models\Users\Settings;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class SettingsService
{

    public function getSystemSettings()
    {
        return Settings::first();
    }
}
