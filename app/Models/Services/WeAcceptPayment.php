<?php

namespace App\Models\Services;

use App\Models\Orders\Transaction;
use Facades\App\Models\Services\OrdersService;
use App\Models\Payment\PaymentMethods;
use App\Models\Products\Product;
use App\Models\Users\Address;
use Facades\App\Models\Repositories\CartRepository;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;

class WeAcceptPayment
{
    private $authApiKey;
    private $merchantID;
    private $integrationID;
    private $iframeID;
    private $hmacHash;

    public function __construct(SmsService $smsService)
    {
        $request = request();
        if($request->payment_method == PaymentMethods::VISA) {
            $this->iframeID = env('WE_ACCEPT_IFRAME_ID');
            $this->integrationID = env('WE_ACCEPT_INTEGRATION_ID');
        } else if($request->payment_method == PaymentMethods::VALU) {
            $this->iframeID = env('VALU_IFRAME_ID');
            $this->integrationID = env('VALU_INTEGRATION_ID');
        } else if($request->payment_method == PaymentMethods::PREMIUM) {
            $this->iframeID = env('PREMIUM_IFRAME_ID');
            $this->integrationID = env('PREMIUM_INTEGRATION_ID');
        }

        $this->smsService = $smsService;
        $this->authApiKey = env('WE_ACCEPT_PAYMENT_API_KEY');
        $this->merchantID = env('MERCHANT_ID');
        $this->hmacHash = env('HMAC_HASH');
    }

    public function Pay($totalAmount, $orderData)
    {
        $token = $this->Authentication();
        if (!$token) {
            return false;
        }
        $transaction = $this->CreateTransaction($totalAmount, null, $orderData);
        if (!$transaction) {
            return false;
        }

        $orderCreation = $this->OrderCreation($token, $totalAmount, $transaction);
        if (!$orderCreation) {
            return false;
        }
        $transaction->update(['order_pay_id' => $orderCreation->id]);
        $iframeToken = $this->PaymentKeyRequest($token, $totalAmount, $orderCreation->id, $orderData['address_id'], $transaction);
        $url = 'https://accept.paymobsolutions.com/api/acceptance/iframes/' . $this->iframeID . '?payment_token=' . $iframeToken;

        return $url;
        return redirect($url)->send();
    }

    public function Authentication()
    {
        $host = 'https://accept.paymobsolutions.com/api/auth/tokens';
        $client = new Client([
            'headers' => ['Content-Type' => 'application/json']
        ]);


        $postData = ['api_key' => $this->authApiKey];
        try {
            $apiRequest = $client->post($host,
                ['body' => json_encode($postData)]
            );
            $response = json_decode($apiRequest->getBody());
            return $response->token;
        } catch (HttpException $ex) {
            return false;
        }
    }

    public function OrderCreation($token, $totalAmount, $transaction)
    {
        $host = 'https://accept.paymobsolutions.com/api/ecommerce/orders';
        $client = new Client([
            'headers' => ['Content-Type' => 'application/json']
        ]);
        $postData = [
            'auth_token' => $token,
            'delivery_needed' => false,
            'merchant_order_id' => $transaction->id,
            'merchant_id' => $this->merchantID,
            'amount_cents' => (integer)$totalAmount,
            'currency' => "EGP",
            'items' => [],
        ];
        $items = CartRepository::getUserCartItems();
        foreach ($items as $item) {
            $product = Product::find($item['id']);
            $postData['items'][] = [
                "name" => strip_tags($product->description),
                "description" => $product->name,
                "amount_cents" => 00,
                "quantity" => $item['amount']
            ];
        }

        try {
            $apiRequest = $client->post($host,
                ['body' => json_encode($postData)]
            );
            $response = json_decode($apiRequest->getBody());
            $this->logResponse($host, $response, $postData, 'order_creation');
            return $response;
        } catch (HttpException $ex) {
            return false;
        }
    }

    public function getOrderData($id)
    {
        $token = $this->Authentication();
        $host = "https://accept.paymobsolutions.com/api/acceptance/transactions/{$id}";
        $client = new Client([
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => $token
            ]
        ]);

        try {
            $apiRequest = $client->get($host);
            $response = json_decode($apiRequest->getBody());
            $this->logResponse($host, $response, [], 'get_order_data');
            return $response;
        } catch (HttpException $ex) {
            return false;
        }
    }

    public function PaymentKeyRequest($token, $totalAmount, $orderPayId, $addressID)
    {
        $host = 'https://accept.paymobsolutions.com/api/acceptance/payment_keys';
        $client = new Client([
            'headers' => ['Content-Type' => 'application/json']
        ]);
        $address = Address::find($addressID);
        $user = auth()->User();
        $name = explode(' ',$user->name);

        $firstName = isset($name[0]) ? $name[0] : '';
        $lastName = isset($name[1]) ? $name[1] : $name[0];

        $postData = [
            'auth_token' => $token,
            'amount_cents' => (integer)$totalAmount,
            'expiration' => 3600,
            'order_id' => $orderPayId,
            'currency' => "EGP",
            'integration_id' => $this->integrationID,
            'billing_data' => [
                "apartment" => $address->apartment,
                "email" => $user->email,
                "floor" => $address->floor,
                "first_name" => $firstName,
                "phone_number" => $user->phone,
                "city" => $address->city->name,
                "state" => $address->area->name,
                "street" => $address->address,
                "building" => $address->address,
                "last_name" => $lastName,
                "country" => "EG",
            ],
        ];
        try {
            $apiRequest = $client->post($host,
                ['body' => json_encode($postData)]
            );
            $response = json_decode($apiRequest->getBody());
            $this->logResponse($host, $response, $postData, 'payment_key_request');
            return $response->token;
        } catch (HttpException $ex) {
            return false;
        }
    }

    public function calculateHMAC($requestData)
    {
        //unset($requestData['hmac']);
        ksort($requestData);
        //dd($hashed);
        $requestValues = '';
        foreach ($requestData as $key => $val) {
            $requestValues .= $val;
        }
        //$hashed = hash('sha512', $requestValues);
        $sig = hash_hmac('sha512', $requestValues, $this->hmacHash);
        Log::info($sig);
        return $sig;
        //$hashed = hash("sha512", $this->hmacHash,$requestValues);
    }

    public function payCallBack($request)
    {
        $hmac = $this->calculateHMAC($request->all());
        if ($hmac != $request->hmac) {
            //return false;
        }
        $transaction = Transaction::where('order_pay_id',$request->order)->first();
        if (!$transaction) {
            return redirect(env("APP_URL").'/online_order?is_success=false&message=transaction not found')->send();
        }
        $data = [
            'transaction_response' => $request->all(),
            'transaction_status' => $request->success,
            'card_info' => null,
        ];
        $transaction->update($data);
        Log::channel("we_accept")->info("pay_callback \n" . json_encode(['request' => $request->all(), 'transaction' => $transaction]));
        return $transaction;
    }

    /**
     * log operations
     *
     * @param $url
     * @param $finalOutput
     * @param $options
     * @param string $type
     */
    private function logResponse($url, $finalOutput, $options, $type = 'page')
    {
        Log::channel("we_accept")->info(
            "{$type} \n" .
            json_encode([
                'requestData' => request()->all(),
                'http' => [
                    'url' => $url,
                    'response' => $finalOutput,
                    'request' => $options,
                ],
            ])
        );
    }

    public function CreateTransaction($totalAmount, $orderPayId, $orderData)
    {
        $data = [
            'order_details' => $orderData,
            'order_pay_id' => $orderPayId,
            'total_amount' => $totalAmount,
            'customer_id' => auth()->User()->id,
            'user_agent' => OrdersService::getUSerAgent(isset($orderData['device_type']) ? $orderData['device_type'] : null)
        ];
        $transaction = Transaction::create($data);
        return $transaction;
    }
}
