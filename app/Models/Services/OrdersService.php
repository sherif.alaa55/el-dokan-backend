<?php

namespace App\Models\Services;

use App\Models\Orders\Validation\Rules\ItemsExists;
use App\Models\Orders\Validation\Rules\RequiredFields;
use Carbon\Carbon;
use App\Mail\OrderCreated;
use App\Models\Users\User;
use App\Models\Orders\Cart;
use Jenssegers\Agent\Agent;
use App\Models\Orders\Order;
use Illuminate\Http\Request;
use App\Models\Payment\Promo;
use App\Models\Users\Address;
use App\Models\Products\Product;
use App\Models\Orders\OrderState;
use App\Models\Settings\Settings;
use App\Models\Payment\PromoTypes;
use App\Notifications\OrderPlaced;
use Illuminate\Support\Facades\DB;
use App\Models\Orders\OrderManager;
use App\Models\Orders\OrderProduct;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Models\Payment\PaymentMethods;
use App\Models\Payment\PromoValidator;
use App\Models\Payment\PromoCalculator;
use App\Models\Services\LocationService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;
use App\Models\Orders\Validation\OrderValidator;
use App\Models\Orders\Validation\Rules\PromoValid;
use App\Models\Orders\Validation\Rules\MaxPerOrder;
use App\Models\Orders\Validation\Rules\PhoneExists;
use Facades\App\Models\Repositories\CartRepository;
use App\Models\Orders\Validation\Rules\AddressValid;
use App\Models\Orders\Validation\Rules\ScheduleDate;
use App\Models\Orders\Validation\Rules\PhoneVerified;
use App\Models\Orders\Validation\Rules\StockAvailable;
use App\Models\Orders\Validation\Rules\ExceptCodAmount;

class OrdersService
{
    private $locationService;
    private $promotionsService;
    private $aramexService;

    public function __construct(AramexService $aramexService, LocationService $locationService, PushService $pushService, PromotionsService $promotionsService)
    {
        $this->locationService = $locationService;
        $this->promotionsService = $promotionsService;
        $this->pushService = $pushService;
        $this->aramexService = $aramexService;
    }

    public function validateOrder($order_data, $user, $lang = 1)
    {
        $validator = new OrderValidator();
        $validator->addRules([
            new AddressValid($order_data, $user),
            new ScheduleDate($order_data, $user),
            new PhoneExists($order_data, $user),
            new PhoneVerified($order_data, $user),
            new MaxPerOrder($order_data, $user, $lang),
            new RequiredFields($order_data),
            new ItemsExists(),
            new StockAvailable($order_data, $user),
            // new PromoValid($order_data, $user),
            // new ExceptCodAmount($order_data, $user)
        ]);

        return $validator;
    }

    public function getTotalAmount($orderData, $separateResults = false)
    {
        $user = \Auth::user();
        $settings = Settings::getSystemSettings();
        $total = 0;
        $weight = 0;
        $numberOfPiece = 0;
        $products = collect([]);
        $items = CartRepository::getUserCartItems();
        foreach ($items as $key => $item) {
            $product = Product::find($item["id"]);
            $product->amount = $item["amount"];
            $products->push($product);
        }
        $items = $this->promotionsService->checkForDiscounts($products);
        foreach ($items as $item) {
            $product = Product::find($item["id"]);
            $order_item = OrderProduct::with("order", "product")->select("*", DB::raw("SUM(amount) as sale_count"))->whereHas("order", function ($q) use ($user) {
                $q->whereNotIn("state_id", [OrderState::CANCELLED, OrderState::INCOMPLETED])->where("user_id", $user->id);
            })->where("product_id", $item["id"])->where("created_at", ">", now()->subDays($product->min_days))->groupby("product_id")->first();
            $total_qty = $item["amount"] + ($order_item ? $order_item->sale_count : 0);
            // validate if quantity + count user orders in last n days less than product max
            if ($product->preorder) {
                $productTotal = $product->price;
            } else if ($product->active_discount) {
                $productTotal = $product->discount_price * $item["amount"];
            } else {
                $productTotal = $product->price * $item["amount"];
            }
            $weight += $product->weight * $item['amount'];
            $numberOfPiece += $item['amount'];
            $total += $productTotal;
        }
        $address = Address::findOrFail($orderData['address_id']);
        if (!is_null($settings->min_order_amount) && (integer)$settings->min_order_amount <= $total) {
            $delivery_fees = 0;
        } else {
            $delivery_fees = $this->aramexService->calculateFees($address, $weight, $numberOfPiece);
        }

        if (isset($orderData['promo']) && $orderData['promo'] != null) {
            $promo = Promo::where("name", $orderData['promo'])->first();
            if ($promo) {
                $amount = $total;
                PromoValidator::validate($promo, \Auth::user(), $total, $items);
                if ($promo->list_id) {
                    $amount = 0;
                    foreach ($items as $item) {
                        if ($promo->list->products()->where("products.id", $item["id"])->orWhere("products.id", $item["parent_id"])->exists()) {
                            $product = Product::find($item["id"]);
                            $price = $product->active_discount ? $product->discount_price : $product->price;
                            $amount += $price;
                        }
                    }
                }
                if ($promo->type == PromoTypes::FREE_DELIVERY) {
                    $delivery_fees = 0;
                } else {
                    $discount = PromoCalculator::calculate($promo, $amount);
                    $total = $total - $discount;
                }
            }
        }
        if($separateResults) {
            return ['total' => ($total * 100), 'delivery_fees' => $delivery_fees];
        }

        $total = $total + $delivery_fees;
        return ($total * 100);

    }

    public function getUSerAgent($deviceType=null) {
        // $agent = new Agent();
        // if($agent->isAndroidOS() || strpos(request()->header("User-Agent"), 'okhttp') !== false) {
        //     return 'android';
        // } else if ($agent->is('iPhone')) {
        //     return 'ios';
        // } else if ($agent->isDesktop()) {
        //     return 'web';
        // } else {
        //     return request()->header("User-Agent");
        // }
        switch ($deviceType) {
            case 1 : return 'web'; break;
            case 2 : return 'android'; break;
            case 3 : return 'ios'; break;
            default : return 'web';
    }
    }
     public function createOrder($orderData, $transaction_id = null, $userData,$admin_id = null)
     {
        $request = request();
        DB::beginTransaction();
        $settings = Settings::getSystemSettings();
        $weight = 0;
        $numberOfPiece = 0;
        // create order
        try {
            $order = Order::create([
                "user_id" => $userData->id,
                "admin_id" => $admin_id,
                "phone" => $userData->phone,
                "transaction_id" => $transaction_id,
                "payment_method" => isset($orderData['payment_method']) ? $orderData['payment_method'] : null,
                "address_id" => $orderData['address_id'],
                "notes" => isset($orderData['notes']) ? $orderData['notes'] : null,
                "scheduled_at" => isset($orderData['schedule_date']) ? $orderData['schedule_date'] : null,
                "user_agent" => $request->transaction_user_agent ?? $this->getUSerAgent(isset($orderData['device_type']) ? $orderData['device_type'] : null)
            ]);

            // add created state record
            $order->state_id = OrderState::CREATED;
            $order->history()->create(["state_id" => OrderState::CREATED]);
            //if ($orderData['payment_method'] == 2 || $orderData['payment_method'] == 4) {
            //    $order->state_id = OrderState::CREATED;
            //    $order->history()->create(["state_id" => OrderState::CREATED]);
            //} else {
            //    $order->state_id = OrderState::CREATED;
            //    $order->history()->create(["state_id" => OrderState::CREATED]);
            //}
            $order->save();
            $products = collect([]);
            $items = CartRepository::getUserCartItems();
            foreach ($items as $key => $item) {
                $product = Product::find($item["id"]);
                $product->amount = $item["amount"];
                if (!is_null($product->discount_price)) {
                }
                $products->push($product);
            }
            $items = $this->promotionsService->checkForDiscounts($products);

            Log::info("ORDER CREATED");

            // add items
            foreach ($items as $item) {
                $product = Product::findOrFail($item->id);
                !isset($item->promotion_id) ?: $dataArr['promotion_id'] = $item->promotion_id;
                if ($product->parent->type == 2) {
                    if (!$product->parent->bundle_checkout) {
                        $highPriceBundle =   $product->bundleProduct()->orderBy('price','DESC')->first();
                        $lowPriceBundles = $product->bundleProduct()->where('product_id','!=',$highPriceBundle->id)->get();
                        $dataArr = [
                            "product_id" => $highPriceBundle->id,
                            "amount" => $item->amount,
                            "price_id" => $product->getCurrentPriceId(),
                            "price" => $product->price,
                            "discount_price" => !is_null($product->discount_price) ? $product->discount_price : null,
                            // preorder
                            "preorder" => $product->preorder,
                            "bundle_id" => $item->id
                            // "preorder_price" => $product->preorder_price,
                            // "remaining" => $product->price - $product->preorder_price
                        ];
                        $order->items()->create($dataArr);
                        foreach ($lowPriceBundles as $bundleProduct){
                            $dataArr = [
                                "product_id" => $bundleProduct->id,
                                "amount" => $item->amount,
                                "price_id" => null,
                                "price" => 0,
                                "discount_price" => null,
                                // preorder
                                "preorder" => null,
                                "preorder_price" => null,
                                "remaining" => null,
                                "bundle_id" => $item->id
                            ];
                            $order->items()->create($dataArr);
                        }

                    }else {
                        $dataArr = [
                            "product_id" => $item->id,
                            "amount" => $item->amount,
                            "price_id" => $product->getCurrentPriceId(),
                            "price" => $item->price,
                            "discount_price" => !is_null($item->discount_price) ? $item->discount_price : null,
                            // preorder
                            "preorder" => $item->preorder,
                            // "preorder_price" => $item->preorder_price,
                            // "remaining" => $product->price - $item->preorder_price
                        ];
                        $order->items()->create($dataArr);
                    }
                }else {
                    $dataArr = [
                        "product_id" => $item->id,
                        "amount" => $item->amount,
                        "price_id" => $product->getCurrentPriceId(),
                        "price" =>  $item->price,
                        "discount_price" => !is_null($item->discount_price) ? $item->discount_price : null,
                        // preorder
                        "preorder" => $item->preorder,
                        // "preorder_price" => $item->preorder_price,
                        // "remaining" => $product->price - $item->preorder_price
                    ];
                    $order->items()->create($dataArr);
                }

                $product->increment("orders_count");

                if ($product->parent->type == 2) {
                    if ($product->parent->has_stock) {
                        $product->decrement("stock", $item->amount);
                    }else {
                        foreach ($product->bundleProduct as $bundleProduct){
                            $bundleProduct->decrement("stock", $item->amount);
                        }
                    }
                }else {
                    $product->decrement("stock", $item->amount);
                }
                $weight += $product->weight * $item->amount;
                $numberOfPiece += $item->amount;
            }

            $total = $order->getTotal();

            if (!is_null($settings->min_order_amount) && (integer)$settings->min_order_amount <= $total) {
                $delivery_fees = 0;
            } else {
                $delivery_fees = $this->aramexService->calculateFees($order->address, $weight, $numberOfPiece);
            //    $delivery_fees = $this->locationService->getAddressDeliveryFees($order->address);
            }

            // generate invoice
            $invoiceTotal = $orderData['payment_method'] == 5 ? 0 : $total;
            if ($orderData['payment_method'] == 3) {
                $invoiceTotal = $request->valu_down_payment + $request->valu_purchase_fees;
            }
            $invoice = $order->invoice()->create(["total_amount" => $invoiceTotal, "delivery_fees" => $delivery_fees]);
            Log::info("INVOICE CREATED");

            if (isset($orderData['promo']) && $orderData['promo'] != null) {
                $promo = Promo::where("name", $orderData['promo'])->first();
                $referer = User::where('referal', $orderData['promo'])->first();

                if ($promo) {
                    $amount = $total;

                    PromoValidator::validate($promo, $userData, $total, $items);
                    if ($promo->list_id) {
                        $amount = 0;
                        foreach ($items as $item) {
                            if ($promo->list->products()->where("products.id", $item["id"])->orWhere("products.id", $item["parent_id"])->exists()) {
                                $product = Product::find($item["id"]);
                                $price = $product->active_discount ? $product->discount_price : $product->price;
                                $amount += $price;
                            }
                        }
                    }
                    if ($promo->type == PromoTypes::FREE_DELIVERY) {
                        $invoice->update(["delivery_fees" => 0]);
                    } else {
                        $discount = PromoCalculator::calculate($promo, $amount);
                        $invoice->discount = $invoice->total_amount - $discount;
                        $invoice->promo_id = $promo->id;
                        $invoice->save();
                    }
                    $userData->userPromos()->attach($promo->id, ["use_date" => Carbon::now()]);
                } elseif ($referer) {
                    $order->referal = $referer->referal;
                    $order->save();
                }
            }

            $grandTotal = (!is_null($invoice->discount) ? $invoice->discount : $total) + $delivery_fees;
            $invoice->update(["grand_total" => $grandTotal]);

            if (isset($orderData['schedule']) && $orderData['schedule'] != null) {
                $order->createSchdule($orderData['schedule']);
            }

            if (!isset($orderData['scheduled_at'])) {
                $this->pushService->notifyAdmins("New Order", "You have received a new order {$order->id}");
            }
//            if (isset($orderData['payment_method']) && $orderData['payment_method'] == PaymentMethods::VISA) {
                Notification::send($order->customer, new OrderPlaced($order));
//            }


            try {
                $userData->settings ? app()->setLocale($userData->settings->language) : app()->setLocale('en');
                Mail::to($userData)->send(new OrderCreated($order));
            } catch (\Exception $e) {
                Log::error($e->getMessage());
            }

            $userData->update(["first_order" => 1]);

            Cart::where('user_id', auth()->id())->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e->getMessage());
            return response()->json([
                "code" => 425,
                "message" => $e->getMessage(),
                "errors" => [
                    "errorMessage" => '',
                    "errorDetails" => $e->getTrace(),
                ]
            ], 200)->send();
        }

        return $order;
    }

    public function updateOrder($id, $order_data)
    {
        $order = Order::findOrFail($id);
        $weight = 0;
        $numberOfPiece = 0;
        $order->update([
            "address_id" => $order_data["address_id"],
            "payment_method" => $order_data["payment_method"],
            "admin_notes" => $order_data["admin_notes"] ?? null,
            "notes" => $order_data["notes"] ?? null
        ]);



        if (isset($order_data["deleted_items"])) {
            foreach ($order_data["deleted_items"] as $deletedItem) {
                $order_item = $order->items()->where("product_id", $deletedItem)->first();
                if ($order_item) {
                    $product = Product::findOrFail($deletedItem);
                    $product->increment("stock", $order_item->amount);
                    $order_item->delete();
                }
            }
        }

        $products = collect([]);
        $items = $order_data["items"];
        foreach ($items as $key => $item) {
            $product = Product::find($item["id"]);
            $product->amount = $item["amount"];
            $products->push($product);
            $numberOfPiece += $item['amount'];
            $weight += $product->weight * $item['amount'];
        }
        $items = $this->promotionsService->checkForDiscounts($products);

        foreach ($items as $item) {
            $order_item = $order->items()->where("product_id", $item->id)->first();
            if (isset($order_item)) {
                $product = Product::findOrFail($item->id);
                if ($order_item->amount > $item->amount) {
                    $product->increment("stock", $order_item->amount - $item->amount);
                } else if ($order_item->amount < $item->amount) {
                    $product->decrement("stock", $item->amount - $order_item->amount);
                }
                $order_item->update(["amount" => $item->amount, "price" => $item->price, "discount_price" => $item->discount_price]);
            } else {
                $product = Product::findOrFail($item->id);
                $product->update(["stock" => $product->stock - $item->amount]);
                $order->items()->create(["product_id" => $item->id, "amount" => $item->amount, "price_id" => $product->getCurrentPriceId(), "price" => $item->price, "discount_price" => $item->discount_price]);
            }
        }

        $total = $order->getTotal();
        $invoice = $order->invoice;

        if (isset($order_data["overwrite_fees"]) && $order_data["overwrite_fees"]) {
            $fees = $order_data["delivery_fees"] ?? null ;
        }else{
            $fees = $this->aramexService->calculateFees($order->address, $weight, $numberOfPiece);
        }

        $orderData = [
            "delivery_fees" => $fees,
            "admin_discount" => $order_data["admin_discount"] ?? null,
            "total_amount" => $total
        ];
        $invoice->update($orderData);
        if (isset($order_data["remove_promo"])) {
            $invoice->update(["promo_id" => null, "discount" => null]);
        }

        $discount = 0;
        if (isset($invoice->promo)) {
            $promo = $invoice->promo;

            if ($promo->list_id) {
                $total = 0;
                foreach ($items as $item) {
                    if ($promo->list->products()->where("products.id", $item->id)->exists()) {
                        $product = Product::find($item->id);
                        $price = $item->active_discount ? $item->discount_price : $item->price;
                        $total += $price;
                    }
                }
            }

            $discount = PromoCalculator::calculate($promo, $total);

            $invoice->discount = $invoice->total_amount - $discount;
            $invoice->save();
        }

        return $order;
    }

    public function activeDiscount($product)
    {
        return (bool)($product->discount_start_date >= Carbon::now() && $product->discount_end_date <= Carbon::now());
    }
}
