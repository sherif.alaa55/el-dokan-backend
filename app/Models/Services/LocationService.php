<?php 

namespace App\Models\Services;

use App\Models\Locations\Area;
use App\Models\Locations\City;
use App\Models\Users\Address;

class LocationService
{
	
	public function getAddressDeliveryFees(Address $address)
	{
		if ($address->district) {
			return $address->district->delivery_fees;
		} elseif ($address->area) {
			return $address->area->delivery_fees;
		} else {
			return $address->city->delivery_fees;
		}
	}

	public function isLastNode($city_id, $area_id = null, $district_id = null)
	{
		$city = City::with("areas.districts")->findOrFail($city_id);

		if ($city->areas->count() && !$area_id) {
			return false;
		}

		if ($area_id) {
			$area = Area::findOrFail($area_id);
			if ($area->districts->count() && !$district_id) {
				return false;
			}
		}

		return true;
	}
}
