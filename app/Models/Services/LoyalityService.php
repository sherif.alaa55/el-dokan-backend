<?php 

namespace App\Models\Services;

use App\Models\Users\Settings;
use App\Models\Users\User;

class LoyalityService
{
	private $settings;

	public function __construct()
	{
		$this->settings = Settings::first();
	}

	public function addUserPoints(User $user, $amount, $order_id)
	{
		$points = $this->calculatePoints($amount, $user->isGold());

		if ($points) {
			return $user->points()->create([
				"total_points" => $points,
				"remaining_points" => $points,
				"amount_spent" => $amount,
				"expiration_date" => $this->nextExpirationDate(),
				"activation_date" => now()->addDays($this->settings->pending_days),
				"order_id" => $order_id
	 		]);
		}
	}
	
	public function calculatePoints($amount, $gold = false)
	{
		if (!$this->settings->ex_rate_egp) {
			return 0;
		}

		$rate = $this->settings->ex_rate_pts / $this->settings->ex_rate_egp;

		if ($gold) {
			$rate = $rate + $rate * ($this->settings->ex_rate_gold / 100);
		}

		return $amount * $rate;
	}

	public function updateUserSpending(User $user, $amount)
	{
		$user->spent = $user->spent + $amount;
		$user->save();
	}

	public static function nextExpirationDate()
	{
        if (now() < now()->setDate(date("Y"), 6, 30)) {
            $next_expiry_date = now()->setDate(date("Y"), 12, 31);
        } else {
            $next_expiry_date = now()->setDate(date("Y"), 6, 30)->addYear();
        }

        return $next_expiry_date;
	}
}
