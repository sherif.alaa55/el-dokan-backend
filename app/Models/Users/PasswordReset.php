<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    
    public $timestamps = false;
    protected $primaryKey = "email";
	public $incrementing = false;
    protected $fillable = ["email", "token"];
}
