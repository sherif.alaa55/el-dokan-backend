<?php

namespace App\Models\Home;

use App\Models\Products\Lists;
use App\Models\Repositories\ProductRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;
use App\Models\Transformers\ProductFullTransformer;

class Section extends Model
{

//    protected $appends =['items'];
    protected $fillable = [
        "list_id",
        "active",
        "order",
        "name_en",
        "name_ar",
        "image_en",
        "image_ar",
        "description_ar",
        "description_en",
        "type",
    ];
    public static $validation = [
        "list_id" => "required_if:type,0|integer|exists:lists,id",
        "active" => "required|in:0,1",
        "order" => "required|integer",
        "name_en" => "required|string",
        "name_ar" => "required|string",
        "description_ar" => "required|string",
        "description_en" => "required|string",
        "image_en" => "sometimes|nullable|string",
        "image_ar" => "sometimes|nullable|string",
        "type" => "required|integer|in:0,1,2,3",
    ];
    // type 0 =  to select from lists
    // type 1 =  MostRecent
    // type 2 =  MostBought
    // type 3 =  Discounted(

    protected $casts = [
        'active' => 'integer',
    ];

    public function list()
    {
        return $this->belongsTo(Lists::class)->with('products');
    }

    public function products()
    {


        if ($this->type == 0) {
            $products = $this->list->products;
        }
// elseif ($this->type == 1) {
//            $products = $productRepo->getMostRecent();
//        } elseif ($this->type == 2) {
//            $products = $productRepo->getMostBought();
//        } elseif ($this->type == 3) {
//            $products = $productRepo->getDiscounted();
//        }
        return $products;
    }

    public function TenProducts()
    {
        $products = $this->list->availableProducts()->with("productVariants.images")->active()->orderBy("order", "ASC")->take(10)->get();

        $products = $products->map(function ($item) {
            return ($item->parent_id ? $item : $item->productVariants()->orderBy("stock", "DESC")->first());
        });

        return $products;
    }

    public function getName()
    {
        if (request()->header('lang') == 2) {
            return $this->name_ar ?: $this->name_en;
        }
        return $this->name_en;
    }

    public function getDescription()
    {
        if (request()->header('lang') == 2) {
            return $this->description_ar ?: $this->description_en;
        }
        return $this->description_en;
    }

    public function getImage()
    {
        if (request()->header('lang') == 2) {
            return $this->image_ar ?: $this->image_en;
        }
        return $this->image_en;
    }


}
