<?php

namespace App\Models\Repositories;

use App\Models\Payment\Promo;
use App\Models\Payment\Promotion;
use App\Models\Products\Category;
use App\Models\Products\ListItems;
use App\Models\Products\Product;
use App\Models\Products\Sort\SorterBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
*
*/
class ProductRepository
{

	public function getAllProducts()
	{
		return Product::MainProduct()->available()->all();
	}

	public function getAllProductsPaginated()
	{
		return Product::MainProduct()->available()->paginate(10)->orderBy("order", "ASC")->items();
	}

	public function getProductsByCategory($category_id)
	{
		return Product::MainProduct()->available()->where("category_id", $category_id)->where("active", 1)->orderBy("order", "ASC")->paginate(10)->items();
	}

	public function getProductsByCategoryCount($category_id)
	{
		return Product::MainProduct()->available()->where("category_id", $category_id)->where("active", 1)->orderBy("order", "ASC")->paginate(10)->total();
	}

    public function getProductsOptionsValues($products)
    {
        $valuesWithOption = $products->pluck('productOptionValuesForFilter')->flatten(1)->unique('id')->where('option.appear_in_search', 1);
        $values = collect();
        $options = collect();
        foreach ($valuesWithOption as $val) {
            if(!$values->where('id', $val->id)->first()) {
                $values->push($val);
                if($option = $options->where('id', $val->option->id)->first()) {
                    $option['values'] = collect($option['values'])->push(collect($val)->except('option'));
                    continue;
                }
                $option = collect($val->option)->put('values', [collect($val)->except('option')]);
                $options->push($option);
            }
        }
        return $options;
    }

    public function fullSearch(Request $request)
    {
        $query = Product::query()->MainProduct()->with("availableProductVariants.images","productVariantOptions", "productVariants", "productOptionValuesForFilter.option","productVariantValues",'ProductOptionValues','brand','category','category.parent','category.group','tags','lists')->select("*", DB::raw("COALESCE(discount_price, price) AS final_price"))->active();
        $price_from = isset($request) && is_numeric($request->price_from) ? $request->price_from : null;
        $price_to = isset($request) && is_numeric($request->price_to) ? $request->price_to : null;

        $query->whereHas("productVariants", function ($q) {
            $q->where("active", 1);
        });

        if($request->q) {
            $tnt = Product::tnt();
            $tnt->fuzziness = true;
            $tnt->fuzzy_distance = 1;
            $res = $tnt->search($request->q);

            $query->where(function($q) use ($res, $request) {
                return $q->whereIn("id", $res["ids"])->orWhere("sku", "LIKE", "%{$request->q}%");
            });
        }

        $query->when($request->brand_ids, function ($q) use ($request) {
            $q->whereIn("brand_id", $request->brand_ids);
        });

        $query->when($price_from, function ($q) use ($price_from) {
            $q->whereHas('productVariants', function ($q) use ($price_from) {
                $q->whereRaw("COALESCE(discount_price, price) >= ". ($price_from * 100));  //
            });
        });

        $query->when($price_to, function ($q) use ($price_to) {
            $q->whereHas('productVariants', function ($q) use ($price_to) {
                $q->whereRaw("COALESCE(discount_price, price) <= ". ($price_to * 100));
            });
        });


        $query->when($request->category_id, function ($q) use ($request) {
            $q->where(function ($query) use ($request) {
                $query->whereHas('category', function ($q) use ($request) {
                    $q->where('parent_id', $request->category_id);
                })->orWhereHas('optionalSubCategory', function ($q) use ($request) {
                    $q->where('parent_id', $request->category_id);
                });
            });
        });
        $query->when($request->sub_categories, function ($q) use ($request) {
            $q->where(function ($query) use ($request) {
                $query->whereIn('category_id', $request->sub_categories)
                    ->orWhereIn('optional_sub_category_id', $request->sub_categories);
            });
        });
        $query->when($request->group_id, function ($q) use ($request) {
            $q->whereHas('category.group', function ($q) use ($request) {
                $q->whereIn('groups.id', $request->group_id);
            });
        });
        $query->when($request->rate, function ($q) use ($request) {
            $q->where('rate', '>=', $request->rate);
        });

        //$query->when($request->options_values_ids, function ($q) use ($request) {
        //    $q->whereHas('optionValues', function ($q) use ($request) {
        //        $q->whereIn('option_values.id', $request->options_values_ids);
        //    });
        //});

        $query->when($request->promotions, function ($q){
            $listsArray = Promotion::active()->pluck('list_id')->toArray();
            $q->whereHas('lists', function ($q) use ($listsArray) {
                $q->whereIn('lists.id', $listsArray);
            })->orWhere( function ($q) {
                $now = date('Y-m-d');
                $q->whereNotNull('discount_price');
//                $q->where('reservation_from', '>=', $now);
//                $q->where('reservation_from', '<=', $to);
//                $q->whereBetween(, []);
            });
        });


        $query->when($request->tags_ids, function ($q) use ($request) {
            $q->whereHas('tags', function ($q) use ($request) {
                $q->whereIn('tags.id', $request->tags_ids);
            });
        });

//        $query->when($request->option_ids, function ($q) use ($request) {
//            $q->whereHas('allOptions', function ($q) use ($request) {
//                $q->whereIn('options.id', $request->option_ids);
//            });
//        });
//
//        $query->when($request->options_values_ids, function ($q) use ($request) {
//            $q->whereHas('productOptionValuesForFilter', function ($q) use ($request) {
//                $q->whereIn('option_values.id', $request->options_values_ids);
//            });
//        });

        $query->when($request->list_ids, function ($q) use ($request) {
            $list = ListItems::whereIn('list_id',$request->list_ids)->pluck('item_id')->toArray();
            $uniqueItemsIDS = array_unique($list);
            $q->whereIn("id", $uniqueItemsIDS);
        });

        $query->when(!$request->q || $request->q == null || empty($request->q), function ($q) use ($request) {
//            $q->available();
//            $q->orderBy("order", "ASC");
        });

        if ($request->sort) {
            $sorter = SorterBuilder::build();
            $query = $sorter->sort($query, $request->sort);
            return $query->get();
            // $query->when($request->sort, function ($q) use ($request) {
            //     $sorter = SorterBuilder::build();
            //     $query = $sorter->sort($q, $request->sort);
            //     return $query->get();
            // });
        } else {

            if ($request->q) {
                if (count($res["ids"])) {
                    $ids = implode(", ", $res["ids"]);
                    $query->orderByRaw("FIELD(id, {$ids})");
                }
            } else {
                $query->orderBy("order", "DESC");
            }
        }

        if($request->q || $request->q != null || !empty($request->q)){
            $products = $query->get();
        }else{
//            $products = $query->get()->sortByDesc('variants_stock_count');
            $products = $query->get();
        }

        // $products = $query->get();

        return $products;
    }

    public function fullSearchVariants(Request $request)
    {
        $query = Product::query()->variantProduct()->with("parent", "availableProductVariants.images","productVariantOptions", "productVariants", "productOptionValuesForFilter.option","productVariantValues",'ProductOptionValues','brand','category','category.parent','category.group','tags','lists')->select("*", DB::raw("COALESCE(discount_price, price) AS final_price"), DB::raw("stock > 0 as in_stock"))->active();
        $price_from = isset($request) && is_numeric($request->price_from) ? $request->price_from : null;
        $price_to = isset($request) && is_numeric($request->price_to) ? $request->price_to : null;

        if($request->q) {
            $tnt = Product::tnt();
            $tnt->fuzziness = true;
            $tnt->fuzzy_distance = 1;
            $res = $tnt->search($request->q);
            $query->where(function($q) use ($res, $request) {
                return $q->whereIn("id", $res["ids"])->orWhere("sku", "LIKE", "%{$request->q}%");
            });
        }

        $query->when($request->brand_ids, function ($q) use ($request) {
            $q->whereIn("brand_id", $request->brand_ids);
        });

        $query->when($price_from, function ($q) use ($price_from) {
            $q->whereRaw("COALESCE(discount_price, price) >= ". ($price_from * 100));  //
        });

        $query->when($price_to, function ($q) use ($price_to) {
            $q->whereRaw("COALESCE(discount_price, price) <= ". ($price_to * 100));
        });
        if ($request->in_stock !== null) {
            if($request->in_stock) {
                $query->where('stock', '>', 0);
            } else {
                $query->where('stock', '<', 1);
            }
        }

        $query->when($request->category_id, function ($q) use ($request) {
            $q->where(function ($query) use ($request) {
                $query->whereHas('category', function ($q) use ($request) {
                    $q->where('parent_id', $request->category_id);
                })->orWhereHas('optionalSubCategory', function ($q) use ($request) {
                    $q->where('parent_id', $request->category_id);
                });
            });
        });
        $query->when($request->sub_categories, function ($q) use ($request) {
            $q->where(function ($query) use ($request) {
                $query->whereIn('category_id', $request->sub_categories)
                    ->orWhereIn('optional_sub_category_id', $request->sub_categories);
            });
        });
        $query->when($request->group_id, function ($q) use ($request) {
            $q->where(function ($q) use ($request) {
                $q->whereHas('category', function ($q) use ($request) {
                    $q->whereHas('group', function($q) use ($request) {
                        $q->whereIn('groups.id', $request->group_id);
                    });
                })->orWhereHas("optionalSubCategory", function ($q) use ($request) {
                    $q->whereHas('group', function ($q) use ($request) {
                        $q->whereIn('groups.id', $request->group_id);
                    });
                });
            });
        });
        $query->when($request->rate, function ($q) use ($request) {
            $q->where('rate', '>=', $request->rate);
        });
        $query->when($request->promotions, function ($q){
            $q->where( function ($q2){
                $q2->whereHas('parent', function ($q2) {
                    $q2->whereHas('lists', function ($q3){
                            $listsPromotionsArray = Promotion::active()->whereDate('expiration_date','>=',now())->pluck('list_id')->toArray();
                            $listsPromosArray = Promo::active()->whereDate('expiration_date','>=',now())->pluck('list_id')->toArray();
                            $listsArray = array_merge(array_filter($listsPromotionsArray),array_filter($listsPromosArray));
                            $q3->whereIn('lists.id', $listsArray)->orWhereIn('lists.id', $listsArray)->orWhereHas("promos", function ($q4) {
                                $q4->where("active", 1)->where("expiration_date", ">", now());
                            });
                    })->orWhere("type", 2);
                })->orWhere( function ($q3) {
                    $q3->whereNotNull('discount_price');
                })->orWhere("type", 2);
            });
        });
        $query->when($request->tags_ids, function ($q) use ($request) {
            $q->whereHas('tags', function ($q) use ($request) {
                $q->whereIn('tags.id', $request->tags_ids);
            });
        });

        $query->when($request->list_ids, function ($q) use ($request) {
            $list = ListItems::whereIn('list_id',$request->list_ids)->pluck('item_id')->toArray();
            $uniqueItemsIDS = array_unique($list);
            $q->whereIn("parent_id", $uniqueItemsIDS);
        });

        if ($request->sort) {
            $sorter = SorterBuilder::build();
            $query = $sorter->sort($query, $request->sort);
            return $query->get();
            // $query->when($request->sort, function ($q) use ($request) {
            //     $sorter = SorterBuilder::build();
            //     $query = $sorter->sort($q, $request->sort);
            //     return $query->get();
            // });
        } else {

            if ($request->q) {
                if (count($res["ids"])) {
                    $ids = implode(", ", $res["ids"]);
                    $query->orderByRaw("FIELD(id, {$ids})");
                }
            } else {
                $query->orderBy("in_stock", "DESC")->orderBy("order", "DESC");
            }
        }

        // if($request->q || $request->q != null || !empty($request->q)){
        //     $products = $query->get();
        // }else{
        //     $products = $query->orderBy("in_stock", "DESC")->orderBy("order", "ASC")->get();
        // }

        $products = $query->get();

        return $products;
    }

    public function searchProducts($query)
	{
        $tnt = Product::tnt();
        $tnt->fuzziness = true;
        $res = $tnt->search($query);

        $products = Product::MainProduct()->available()->with("category.parent")->active()->where(function ($q) use ($query, $res) {
			$q->where(function ($qu) use ($query)
			{
				$qu->where("name", "LIKE", "%{$query}%")->orWhere("description", "LIKE", "%{$query}%")->orWhere("description_ar", "LIKE", "%{$query}%")->orWhere("name_ar", "LIKE", "%{$query}%");
			})->orWhereHas("category", function ($qu) use ($query)
			{
				$qu->where("name", "LIKE", "%${query}%")->orWhere("description", "LIKE", "%{$query}%");
			})->orWhereHas("brand", function ($qu) use ($query)
			{
				$qu->where("name", "LIKE", "%${query}%");
			})->orWhereIn("id", $res["ids"]);
		})->limit(100)->orderBy("order", "ASC")->get();

		return $products;
	}

	public function getProductById($id)
	{
		return Product::with("productVariantOptions", "productVariants.productVariantOptions", "productVariants.images", "category.parent")->findOrFail($id);
	}

	public function getMostBought()
	{
		return Product::variantProduct()->with("category.parent", "productVariants.productVariantOptions", "productVariants.images")->active()->orderBy("orders_count", "DESC")->limit(10)->get();
	}

	public function getMostRecent()
	{
		return Product::variantProduct()->with("category.parent", "productVariants.productVariantOptions", "productVariants.images")->active()->orderBy("created_at", "DESC")->limit(10)->get();
	}

	public function getDiscounted()
	{
		return Product::variantProduct()->with("category.parent", "productVariants.productVariantOptions", "productVariants.images")->active()->where("discount_price", ">", 0)->orderBy("discount_price", "DESC")->limit(10)->get();
	}

	public function getSimilarProducts($product)
	{
		$category = $product->category;
        $price = null;
		if($vProduct = optional($product->productVariants)->first()) {
            $price = $vProduct->discount_price ?? $vProduct->price;
        }

        $products = Product::active()->variantProduct()->whereHas('parent', function ($q) use ($category){
            return $q->where('category_id', $category->id);
        })->with("productVariantOptions", "productVariants.productVariantOptions", "productVariants.images", "category.parent");
        !$price ?: $products->select('*', DB::raw("ABS((if(discount_price is null, price, discount_price) / 100) - {$price}) AS similar_price"))->orderBy('similar_price')->havingRaw('similar_price is not null');
        $products = $products->where("id", "!=", $product->id)->orderBy("order", "ASC")->take(10)->get();
		return $products;
	}
}
