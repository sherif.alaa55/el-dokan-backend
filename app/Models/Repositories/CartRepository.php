<?php

namespace App\Models\Repositories;

use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class CartRepository
{
    public $cartItems = [];

    public function getUserCartItems($user = null)
    {
        $items = request('items');
        if ($this->cartItems) {
            return $this->cartItems;
        }
        
        if (!$items) {
            $user = Auth::user() ?? $user;
            $carts = $user->cart()->with('cartItems')->get();
            $items = optional($carts->pluck('cartItems')->first())->map(function ($item) {
                return [
                    'id' => $item->product_id,
                    'amount' => $item->amount,
                ];
            });
            $items = optional($items)->count() ? $items->toArray() : [];
        }

        if (count($items)) {
            $this->cartItems = $items;
            return $items;
        }

        return [];
        // throw ValidationException::withMessages([ 'items' => ['Cart is empty'] ]);
    }
}
