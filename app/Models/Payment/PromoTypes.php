<?php 

namespace App\Models\Payment;

/**
 * 
 */
abstract class PromoTypes
{
	const AMOUNT = 1;
	const PERCENTAGE = 2;	
	const FREE_DELIVERY = 3;	
}