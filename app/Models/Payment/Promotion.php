<?php

namespace App\Models\Payment;

use Carbon\Carbon;
use App\Models\Products\Brand;
use App\Models\Products\Lists;
use App\Models\Products\Product;
use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $fillable = ["name", "name_ar", "discount", "brand_id", "expiration_date", "active", "qty", "discount_qty", "list_id", "target_list_id", "qty2"];

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function list()
    {
        return $this->belongsTo(Lists::class, "list_id");
    }

    public function targetList()
    {
        return $this->belongsTo(Lists::class, "target_list_id");
    }

    public function scopeActive($query)
    {
        return $query->where("active", 1)->whereDate("expiration_date", ">=", Carbon::today()->subDay(1));
    }

    public function getName($lang = 1)
    {
        if ($lang == 2) {
            return $this->name_ar ?: $this->name;
        }
        return $this->name;
    }

    public function hasProduct($id)
    {
        $product_ids = $this->list->products->pluck('id')->toArray();
        
        return Product::whereIn("parent_id", $product_ids)->where("id", $id)->exists();
    }

    public function hasTargetProduct($id)
    {
        $product_ids = $this->targetList->products->pluck('id')->toArray();
        
        return Product::whereIn("parent_id", $product_ids)->where("id", $id)->exists();
    }
}
