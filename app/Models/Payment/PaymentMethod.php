<?php
namespace App\Models\Payment;

use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class PaymentMethod extends Model
{
    protected $hidden = ['updated_at', 'created_at'];
    protected $fillable = ['name', 'name_ar', 'is_online', 'icon', 'active', 'deactivation_notes'];

    public function getName($lang = 1)
    {
        if ($lang == 2) {
            return $this->name_ar ?: $this->name;
        }

        return $this->name;
    }

    public function closed()
    {
        return $this->belongsToMany(User::class, 'closed_payment_methods');
    }

    public function scopeNotClosedMethods($query)
    {
        return $query->whereDoesntHave('closed', function ($qu) {
            return $qu->where('user_id', Auth::id());
        })->where('active', 1);
    }

    public function getIconAttribute($value)
    {
        return url($value);
    }
}
