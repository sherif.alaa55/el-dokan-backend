<?php

namespace App\Models\Medical;

use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class Prescription extends Model
{
    
    public static $validation = [
    	// "name" => "required",
    	// "note" => "required",
    	"image" => "required"
    ];

    protected $hidden = ["updated_at"];

    protected $fillable = ["name", "note", "image"];

    public function user()
    {
        return $this->belongsTo(User::class, "user_id");
    }

    public function getImageAttribute()
    {
        if(isset($this->attributes["image"])){
            $image = explode("/", $this->attributes["image"]);
            $name = array_pop($image);
            $image = implode("/", $image) . "/" . rawurlencode($name);

            if(preg_match("/https?:\/\//", $this->attributes["image"])) {
                return $image;
            }

            return URL::to('') . "/" . $image;
        }
    }
}
