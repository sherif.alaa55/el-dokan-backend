<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        // Define the response
        $code = 500;
        $response = [
            'code' => $code,
            "message" => trans("exceptions.500"),
            "errors" => []
        ];

        if (!app()->environment('production')) {
            $response["errors"]["trace"] = $exception->getTrace();
            $response["errors"]["message"] = $exception->getMessage();
        }

        // $code = $exception->getStatusCode();
        // If this exception is an instance of HttpException
        if ($this->isHttpException($exception)) {
            // Grab the HTTP status code from the Exception
            $response["internalMessage"] = $exception->getMessage() ?: "Not Found";
            $code = $exception->getStatusCode();
        }

        if ($exception instanceof Tymon\JWTAuth\Exceptions\TokenExpiredException) {
            $response["userMessage"] = "Invalid Token";
            $response["internalMessage"] = "token_expired";
        } else if ($exception instanceof Tymon\JWTAuth\Exceptions\TokenInvalidException) {
            $response["userMessage"] = "Invalid Token";
            $response["internalMessage"] = "token_invalid";
        } else if ($exception instanceof ValidationException) {
            $response["message"] = current(current($exception->errors()));
            $response["errors"]["errorMessage"] = "Invalid Data";
            $response["errors"]["errorDetails"] = $exception->errors();
            $code = 422;
        }

        if ($exception instanceof AuthenticationException ) {
            $code = 403;
        }

        if ($exception instanceof ModelNotFoundException) {
            $response["message"] = $exception->getModel() . " not found with the id: " . implode($exception->getIds(), ", ");
            $code = 404;
        }

        // Return a JSON response with the response array and status code
        $response["code"] = $code;
        return response()->json($response, 200);
    }
}
