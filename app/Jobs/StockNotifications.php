<?php

namespace App\Jobs;

use App\Models\Products\Product;
use App\Models\Services\PushService;
use App\Models\Users\User;
use App\Notifications\ProductAvailable;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Notification;
use Mail;

class StockNotifications implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $product;
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $users = $this->product->stock_notifiers;
        foreach ($users as $user){
            Notification::send($user, new ProductAvailable($this->product));
        }
        $this->product->stock_notifiers()->sync([]);
    }
}
