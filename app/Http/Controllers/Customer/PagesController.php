<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Pages\Page;
use App\Models\Transformers\Customer\PagesTransformer;

class PagesController extends Controller
{
    private $pageTrans;

    public function __construct(PagesTransformer $pageTrans)
    {
        $this->pageTrans = $pageTrans;
    }

    public function getPageDetails($slug)
    {
        $page = Page::where('slug',$slug)->where('active',1)->first();
        if (!$page){
            $message = 'page Not Found';
            return $this->errorResponse($message, "Invalid data", 422);
        }
        $pageTransform = $this->pageTrans->transform($page);
        return $this->jsonResponse("Success", $pageTransform);

    }
    public function getAllPages()
    {
        $page = Page::where('active',1)->get();
        $pageTransform = $this->pageTrans->transformCollection($page);
        return $this->jsonResponse("Success", $pageTransform);

    }
}
