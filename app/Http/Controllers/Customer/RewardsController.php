<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Resources\RewardsResource;
use App\Models\Loyality\Reward;
use App\Models\Payment\Promo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;

class RewardsController extends Controller
{
    
    public function getRewards()
    {
        return $this->jsonResponse("Success", RewardsResource::collection(Reward::where("active", 1)->get()));
    }

    public function redeemReward(Request $request)
    {
        $this->validate($request, [
            "reward_id" => "required|exists:rewards,id"
        ]);

        // get reward
        $reward = Reward::findOrFail($request->reward_id);
        if (!$reward->active) {
            return $this->errorResponse(Lang::get("mobile.errorRewardInactive"), "invalid date", [], 422);
        }

        // get user total points
        $user = \Auth::user();
        // validate
        if (!$user->isGold() && $reward->is_gold) {
            return $this->errorResponse(Lang::get("mobile.errorNeedGold"), "invalid date", [], 410);
        }

        if ($user->getCurrentPoints() < $reward->point_cost) {
            return $this->errorResponse(Lang::get("mobile.errorNotEnoughPoints"), "invalid date", [], 411);
        }

        if (!$user->phone) {
            return $this->errorResponse(Lang::get("mobile.errorUpdateApp"), "invalid date", [], 422);   
        }

        if (!$user->phone_verified) {
            if (!$user->verification_code) {
                $user->verification_code = 1234; // rand(1000, 9999);
                $user->save();
            }

            return $this->errorResponse(Lang::get("mobile.errorUpdateApp"), "invalid date", [], 409);   
        }

        // redeem and subtract
        DB::beginTransaction();

        try {

            $points = $user->points()->where("remaining_points", ">", 0)->where('expiration_date', ">=", now())->where("activation_date", "<=", now())->orderBy("created_at", "DESC")->get();

            // check if promo
            if ($reward->type == 1) {
                // generate promo
                $promo = Promo::create([
                    "name" => "GIFT-" . strtoupper(str_random(5)),
                    "type" => $reward->amount_type,
                    "amount" => $reward->amount * 100,
                    "max_amount" => $reward->max_amount * 100,
                    "active" => 1,
                    "recurrence" => 1,
                    "expiration_date" => now()->addMonth()
                ]);

                $promo->targets()->attach(\Auth::id());
            }
            $redeem = $user->redeems()->create([
                "reward_id" => $reward->id,
                "points_used" => $reward->point_cost,
                "status" => 0,
                "promo_id" => ($reward->type == 1 ? $promo->id : null)
            ]);

            $point_cost = $reward->point_cost;

            foreach ($points as $point) {
                if ($point_cost == 0) {
                    continue;
                }
                if ($point->remaining_points >= $point_cost) {
                    $point->remaining_points = $point->remaining_points - $point_cost;
                    $point->used_points = $point->used_points + $point_cost;
                    $point->save();
                    $point->redeems()->attach($redeem->id, ["amount" => $point_cost]);
                    $point_cost = 0;
                    continue;
                } else {
                    $point_cost -= $point->remaining_points;
                    $point->redeems()->attach($redeem->id, ["amount" => $point->remaining_points]);
                    $point->used_points = $point->total_points;
                    $point->remaining_points = 0;
                    $point->save();
                }
            }


            DB::commit();

            return $this->jsonResponse("Success");
        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorResponse("Unexpected Error: " . $e->getMessage(), "invalid data", [], 422);
        }
    }
}
