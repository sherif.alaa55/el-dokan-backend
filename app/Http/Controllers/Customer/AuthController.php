<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Mail\NewRegistration;
use App\Mail\ResetPassword;
use App\Models\Repositories\SocialRepository;
use App\Models\Transformers\CustomerTransformer;
use App\Models\Users\ContactUs;
use App\Models\Users\PasswordReset;
use App\Models\Users\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Laravel\Socialite\Facades\Socialite;
use Spatie\Newsletter\NewsletterFacade;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    private $customerTrans;
    private $socialRepo;

    public function __construct(CustomerTransformer $customerTrans, SocialRepository $socialRepo)
    {
        $this->customerTrans = $customerTrans;
        $this->socialRepo = $socialRepo;
    }


    public function login(Request $request)
    {
        // validate request
        $validator = Validator::make($request->all(), [
            "email" => "required|email",
            "password" => "required"
        ], ["email.email" => "Please provide a valiad email"]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }

        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($request->only(["email", "password"]))) {
                return $this->errorResponse(Lang::get("mobile.errorIncorrectPassword"), "invalid data", [], 403);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return $this->errorResponse(Lang::get("mobile.errorIncorrectPassword"), "invalid data", [], 403);
        }

        $user = \Auth::user();

        if (!$user->active) {
            return $this->errorResponse(Lang::get("mobile.errorAccountDeactivated"), "account deactived", [], 403);
        }

        $user->token = $token;
        $user->makeHidden(["created_at", "updated_at"]);

        // return success
        return $this->jsonResponse("Success", $this->customerTrans->transform($user), 200);
    }

    public function logout(Request $request)
    {
        try {
            // and you can continue to chain methods
            $user = JWTAuth::parseToken()->authenticate();

            // $validator = Validator::make($request->all(), [
            //     "token" => "required"
            // ]);

            // if ($validator->fails()) {
            //     return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
            // }

            if ($user)
                $user->tokens()->where("token", $request->token)->delete();
        } catch (\Exception $e) {
        }

        return $this->jsonResponse("Success");
    }

    public function guest()
    {
        $guest = User::find(999999);

        $token = JWTAuth::fromUser($guest);
        // $guest->token = $token;
        $guest->token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL21vYmlsYXR5YXBpLmVsLWRva2FuLmNvbS9hcGkvY3VzdG9tZXIvYXV0aC9ndWVzdCIsImlhdCI6MTYxMjgxNDExNiwiZXhwIjozNzYxMjgxNDExNiwibmJmIjoxNjEyODE0MTE2LCJqdGkiOiJucGRzYVpJdnV5Mm1PM2NQIiwic3ViIjo5OTk5OTksInBydiI6IjRhYzA1YzBmOGFjMDhmMzY0Y2I0ZDAzZmI4ZTFmNjMxZmVjMzIyZTgifQ.BkXuKfrX6TjyA0UJrd0h3Z_CCL0GXRA_M3-cBAyfWgA";


        return $this->jsonResponse("Success", $this->customerTrans->transform($guest));
    }

    public function register(Request $request)
    {
        // validate request
        $validator = Validator::make($request->all(), [
            "name" => "required|min:3",
            "email" => "required|unique:users,email|email",
            "birthdate" => "nullable|before:" . date("Y-m-d"),
            "password" => "required|min:8",
            // "phone" => "required|min:11"
        ], ["email.unique" => Lang::get("mobile.errorEmailUsed"), "name.min" => trans("mobile.errorNameMin")]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "invalid data", $validator->errors(), 422);
        }

        $user = User::where('phone', $request->phone)->get()->first();
        if ($user && $user->phone_verified) {
            return $this->errorResponse(Lang::get("mobile.errorPhoneUsed"), "invalid data", $validator->errors(), 422);
        } else if ($user && !$user->phone_verified) {
            $user->phone = null;
            $user->save();
        }

        // register a user
        $user = User::create($request->all());
        $user->password = bcrypt($request->password);
        $user->active = 1;
        $user->phone_verified = 1;
        $user->type = 1;
        $user->save();

        $settings = $user->settings()->firstOrCreate(['language' => 'en']);

        if (!$user->referal) {
            $user->referal = $user->generateReferal();
            $user->save();
        }

        $token = JWTAuth::attempt($request->only(["email", "password"]));
        $user->token = $token;
        $user->makeHidden(["created_at", "updated_at", "verification_code"]);

        Mail::to($user)->send(new NewRegistration($user));

        // return success
        return $this->jsonResponse("Success", $this->customerTrans->transform($user), 200);
    }

    public function socialRegister(Request $request)
    {
        // validate request
        $validator = Validator::make($request->all(), [
            "name" => "required|min:3",
            "birthdate" => "nullable|before:" . date("Y-m-d"),
            // "phone" => "required|unique:users,phone|min:11",
            "token" => "required"
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "invalid data", $validator->errors(), 422);
        }

        $user = User::where('phone', $request->phone)->get()->first();
        if ($user && $user->verified) {
            return $this->errorResponse(Lang::get("mobile.errorPhoneUsed"), "invalid data", $validator->errors(), 422);
        } else if ($user && !$user->verified) {
            $user->phone = null;
            $user->save();
        }


        // Work around for iOS ... take 2 ... go to hell ya sameh
        JWTAuth::setToken($request->token);

        if (!$user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        if (!$user->active) {
            return $this->errorResponse(Lang::get("mobile.errorAccountDeactivated"), "account deactived", [], 403);
        }

        // register a user
        // $user = \Auth::user();
        $request->merge(["type" => 1]);
        $user->update($request->only(["name", "last_name", "birthdate", "phone", "type"]));


        $user->token = (string)JWTAuth::getToken();
        $user->makeHidden(["created_at", "updated_at"]);
        Mail::to($user)->send(new NewRegistration($user));
        // return success
        return $this->jsonResponse("Success", $this->customerTrans->transform($user), 200);
    }

    public function socialAuth(Request $request)
    {
        // validate request
        $validator = Validator::make($request->all(), [
            "provider" => "required|in:facebook,google,apple"
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }
        $token = $request->access_token;
        $provider = $request->provider;
        if ($provider == "google") {
            try {
                // $request->merge(['code' => $request->access_token]);
                if ($request->code) {
                    $providerUser = Socialite::driver($provider)->stateless()->user();
                } else {
                    $providerUser = Socialite::driver($provider)->stateless()->userFromToken($request->access_token);
                }
            } catch (\Exception $e) {
                return $this->errorResponse("Provider Error", "Invalid token", $e->getMessage(), 400);
            }
        } else if ($provider == "facebook") {
            try {
                $providerUser = Socialite::driver($provider)->stateless()->userFromToken($token);
            } catch (\Exception $e) {
                return $this->errorResponse("Provider Error", "Invalid token", $e->getMessage(), 400);
            }
        } else if ($provider == "apple") {
            //  $providerUser = Socialite::driver($provider)->stateless()->user();
            $providerUser = Socialite::driver($provider)->stateless()->userFromToken($token);
            if ($request->name) {
                $providerUser->name = $request->name;
            } else {
                $providerUser->name = "";
            }
        }
        // dd($providerUser);
        $registered = $this->socialRepo->isFirst($provider, $providerUser->id);
        $user = $this->socialRepo->firstOrCreate($providerUser, $provider);
        if (!$user->referal) {
            $user->referal = $user->generateReferal();
            $user->save();
        }

        $settings = $user->settings()->firstOrCreate(['language' => 'en']);

        $token = \JWTAuth::fromUser($user);
        $user->token = $token;
        $user->makeHidden(["created_at", "updated_at"]);
        if (!$user->active) {
            return $this->errorResponse(Lang::get("mobile.errorAccountDeactivated"), "account deactived", [], 403);
        }
        return $this->jsonResponse("Success", $this->customerTrans->transform($user), 200);
    }

    public function forgetPassword(Request $request)
    {
        // validate request
        $validator = Validator::make($request->all(), [
            "email" => "required|exists:users,email|email",
        ], ["email.exists" => Lang::get("mobile.errorEmailNotFound")]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "invalid data", $validator->errors(), 422);
        }

        $email = $request->email;

        $user = User::where("email", $email)->get()->first();


        // first or create reset token
        $reset = PasswordReset::where("email", $request->email)->get()->first();
        if ($reset) {
            $reset->update(["token" => rand(10000, 99999)]);
            // $reset->update(["token" => 12345]);
        } else {
            $reset = PasswordReset::create([
                "email" => $user->email,
                "token" => rand(10000, 99999)
                // "token" => 12345
            ]);
        }

        // send email with the token
        if ($user) {
            Mail::to($user)->send(new ResetPassword($user, $reset->token));
        }

        return $this->jsonResponse(Lang::get("mobile.linkSent"));
    }

    public function validateCode(Request $request)
    {
        // validate request
        $validator = Validator::make($request->all(), [
            "email" => "required|exists:users,email",
            "code" => "required|exists:password_resets,token",
        ], ["code.exists" => Lang::get("mobile.errorIncorrectCode")]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "invalid data", $validator->errors(), 422);
        }

        if (!PasswordReset::where("email", $request->email)->where("token", $request->code)->get()->count())
            return $this->errorResponse(Lang::get("mobile.errorIncorrectCode"), "Incorrect Code", [], 403);

        return $this->jsonResponse("Success");
    }

    public function resetPassword(Request $request)
    {
        // validate request
        $validator = Validator::make($request->all(), [
            "email" => "required|exists:users,email",
            "code" => "required|exists:password_resets,token",
        ], ["code.exists" => Lang::get("mobile.errorIncorrectCode")]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "invalid data", $validator->errors(), 422);
        }

        $reset = PasswordReset::where("token", $request->code)->get()->first();

        if (!$reset) {
            return $this->errorResponse(Lang::get("mobile.errorIncorrectCode"), "invalid data", $validator->errors(), 422);
        }

        $user = User::where("email", $reset->email)->get()->first();
        $user->password = bcrypt($request->password);
        $user->save();

        $token = JWTAuth::fromUser($user);
        $user->token = $token;

        return $this->jsonResponse("Success", $this->customerTrans->transform($user));
    }

    public function subscribeNewsletter(Request $request)
    {
        $this->validate($request, [
            "email" => "required|email"
        ]);

        NewsletterFacade::subscribe($request->email);

        return $this->jsonResponse("Success");
    }

    public function contactUs(Request $request)
    {
        $this->validate($request, [
            "name" => "required",
            "email" => "required|email",
            "phone" => "required",
            "message" => "required",
        ]);

        $contact = ContactUs::create($request->all());

        return $this->jsonResponse("Success", $contact);
    }
}
