<?php

namespace App\Http\Controllers\Customer;

use App\Http\Resources\Customer\OptionsResource;
use App\Http\Resources\Customer\ProductsOption;
use App\Models\Products\Option;
use App\Models\Products\OptionValue;
use Illuminate\Http\Request;
use App\Models\Products\Brand;
use App\Models\Products\Lists;
use App\Models\Products\Product;
use App\Models\Products\Category;
use App\Models\Products\ListItems;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use App\Models\Products\Sort\SorterBuilder;
use App\Models\Repositories\ProductRepository;
use App\Http\Resources\Customer\BrandsResource;
use App\Http\Resources\Customer\GroupsResource;
use App\Models\Products\Group;
use App\Models\Transformers\ProductTransformer;
use App\Models\Transformers\ProductDetailTransformer;
use App\Models\Transformers\Customer\CategoryTransformer;

class ProductsController extends Controller
{
    private $productTrans;
    private $productRepo;
    private $productDetailTrans;
    private $categoryTrans;

    public function __construct(ProductTransformer $productTrans, ProductRepository $productRepo, ProductDetailTransformer $productDetailTrans, CategoryTransformer $categoryTrans)
    {
        $this->productTrans = $productTrans;
        $this->productRepo = $productRepo;
        $this->productDetailTrans = $productDetailTrans;
        $this->categoryTrans = $categoryTrans;
    }

    public function index()
    {
        $products = $this->productRepo->getAllProductsPaginated();

        return $this->jsonResponse("Success", $this->productTrans->transformCollection($products));
    }

    public function home(Request $request)
    {
        $bought = $this->productRepo->getMostBought();
        $recent = $this->productRepo->getMostRecent();
        $discounted = $this->productRepo->getDiscounted();

        $locale = $request->header("lang") == 2 ? "ar" : "en";

        return $this->jsonResponse("Success", [
            [
                "name" => Lang::get("mobile.topPromotions", [], $locale),
                // "description" => "22 items in up to 50% sale.",
                "image" => URL::to('images/top-promotions.jpeg'),
                "sort" => "offers",
                "products" => $this->productTrans->transformCollection($discounted)
            ],
            [
                "name" => Lang::get("mobile.mostBought", [], $locale),
                "image" => URL::to('images/popular.jpeg'),
                "sort" => "popular",
                "products" => $this->productTrans->transformCollection($bought)
            ],
            [
                "name" => Lang::get("mobile.mostRecent", [], $locale),
                "image" => URL::to('images/top-promotions.jpeg'),
                "sort" => "recent",
                "products" => $this->productTrans->transformCollection($recent)
            ]
        ]);
    }

    public function topPromotions()
    {
        $products = Product::MainProduct()->available()->with("category.parent")->active()->where("discount_price", ">", 0)->orderBy("discount_price", "DESC")->paginate(10);

        return response()->json([
            "code" => 200,
            "message" => "Success",
            "data" => $this->productTrans->transformCollection($products->items()),
            "total" => $products->lastPage()
        ], 200);
    }

    public function mostBought()
    {
        $products = Product::MainProduct()->available()->with("category.parent")->active()->orderBy("orders_count", "DESC")->paginate(10);

        return response()->json([
            "code" => 200,
            "message" => "Success",
            "data" => $this->productTrans->transformCollection($products->items()),
            "total" => $products->lastPage()
        ], 200);
    }

    public function show($id)
    {
        $product = $this->productRepo->getProductById($id);

        $relatedSkus = $product->relatedSkus->pluck('sku')->toarray();
        $relatedProducts = Product::whereIn('sku', $relatedSkus)->get();

        $product->is_favourite = $product->isUserFavourite(\Auth::user());

        $similar_products = $this->productRepo->getSimilarProducts($product);
        $product = $this->productDetailTrans->transform($product);
        $similar_products = $this->productTrans->transformCollection($similar_products);
        $relatedProducts = $this->productTrans->transformCollection($relatedProducts);

        $product = array_merge($product, ['related_skus' => $relatedProducts]);
        return $this->jsonResponse("Success", [
            "product" => $product,
            "similar_products" => $similar_products
        ]);
    }

    public function search(Request $request)
    {
        // validate request
        $validator = Validator::make($request->all(), ["q" => "required"], ["query.required" => "Please enter a search term"]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }

        $products = $this->productRepo->searchProducts($request->q);

        // $products = array_values($products->filter(function ($item)
        // {
        //     return $item->active;
        // }));


        return $this->jsonResponse("Success", $this->productTrans->transformCollection($products));
    }

    public function search_products(Request $request)
    {
        $request->options_values_ids = $request->options_values_ids ?? [];

        $products = $orProducts = $this->productRepo->fullSearchVariants($request);
        if(count($request->options_values_ids)) {
            $requestValues = OptionValue::find($request->options_values_ids);
            $products = $orProducts->filter(function($product) use ($request, $requestValues){
                $productValuesIds = $this->productRepo->getProductsOptionsValues(collect([$product])->merge($product->availableProductVariants))->pluck('values')->flatten(1)->pluck('id');
                $found = 0;
                foreach ($requestValues as $value) {
                    $valuesOptions = $requestValues->where('option_id', $value->option_id);
                    if($valuesOptions->count() === 1) {
                        !in_array($value->id, $productValuesIds->toArray()) ?: $found++;
                    } else {
                        $multipleValueIds = $requestValues->whereIn('option_id', $valuesOptions->pluck('option_id')->toArray())->pluck('id');
                        foreach ($multipleValueIds as $valueId) {
                            if(in_array($valueId, $productValuesIds->toArray())){
                                $found++;
                            }
                        }
                    }
                }
                return $found >= $requestValues->count();
            });
        }
        $productsOptions = $this->productRepo->getProductsOptionsValues($products)->sortBy('id');
        $orProductsOptions = $this->productRepo->getProductsOptionsValues($orProducts)->sortBy('id');

        foreach ($productsOptions as $option) {
            $orProductsOption = $orProductsOptions->where('id', $option['id'])->first();
            foreach ($option['values'] as $value) {
                if(in_array($value['id'], $request->options_values_ids)) {
                    $option['values'] = $orProductsOption['values'];
                }
            }
        }

        $pageSize = $request->page_size ?: 10;
        // $options =  Option::where("type",'!=', '5')->with('values')->get();
        $productsOptions = $productsOptions->sortBy('order')->values();
        $category_ids = $products->pluck("category_id")->toArray();
        $groups =   Group::whereHas('active_sub_categories', function ($query) use($category_ids) {
            return $query->whereIn('categories.id',$category_ids)->where('parent_id','!=',null);
        })->with(['active_sub_categories' => function ($q) use ($category_ids) {
            return $q->whereIn('categories.id',$category_ids)->where('parent_id','!=',null);
        }])->get();
        $prices = $products->pluck('productVariants')->flatten(1)->map(function ($v) {
            return [
                $v->discount_price ?? $v->price
            ];
        })->flatten(1);
        $brand_ids = $products->pluck("brand_id")->toArray();
        $brands = Brand::whereIn("id", $brand_ids)->orderBy("name", "ASC")->get();
        $max_price = $products->max("price");
        $min_price = $products->min("price");
        $paginated = $products->forPage($request->page ?: 1, $pageSize)->values();
        return $this->jsonResponse("Success", [
            "products" => $this->productTrans->transformCollection($paginated),
            "products_options" => ProductsOption::collection($productsOptions),
            "total" => ceil($products->count() / $pageSize),
            "total_products" => $products->count(),
            "sub_categories" => GroupsResource::collection($groups)->sortBy('name')->values(),
            // "options" => OptionsResource::collection($options),
            "brands" => BrandsResource::collection($brands)->sortBy('name')->values(),
            "max_price" => (int) $max_price,
            "min_price" => (int) $min_price,
        ]);
    }

    public function myFavourites()
    {
        $user = \Auth::user();

        $favourites = $user->favourites;

        return $this->jsonResponse("Success", $this->productTrans->transformCollection($favourites));
    }

    public function favourite($id)
    {
        $user = \Auth::user();

        $product = Product::findOrFail($id);

        if ($user->favourites()->find($id)) {

        } else {
            $user->favourites()->attach($id);
        }


        return $this->jsonResponse("Success", $this->productTrans->transformCollection($user->favourites));
    }

    public function addProductsToFavourite(Request $request)
    {
        $user = \Auth::user();

        $user->favourites()->syncWithoutDetaching($request->product_ids);

        return $this->jsonResponse("Success", $this->productTrans->transformCollection($user->favourites));
    }

    public function removeAllFavourites()
    {
        $user = \Auth::user();

        $user->favourites()->sync([]);

        return $this->jsonResponse("Success", $this->productTrans->transformCollection($user->favourites));
    }

    public function myComparisons()
    {
        $user = Auth::user();
        $comparisons = $user->comparisons;
        return $this->jsonResponse("Success", $this->productDetailTrans->transformCollection($comparisons));
    }

    public function setCompare($id)
    {
        $user = Auth::user();

        $product = Product::findOrFail($id);
        if($product) {
            $user->comparisons()->syncWithoutDetaching([$id]);
        }
        return $this->jsonResponse("Success");
    }

    public function removeCompare($id)
    {
        $user = Auth::user();

        $product = Product::findOrFail($id);
        if($product) {
            $user->comparisons()->detach([$id]);
        }
        return $this->jsonResponse("Success");
    }

    public function setProductsCompare(Request $request)
    {
        $user = Auth::user();

        $user->comparisons()->syncWithoutDetaching($request->product_ids);

        return $this->jsonResponse("Success");
    }

    public function removeProductsCompare()
    {
        $user = Auth::user();

        $user->comparisons()->sync([]);
        
        return $this->jsonResponse("Success");
    }

    public function unfavourite($id)
    {
        $user = \Auth::user();

        $product = Product::findOrFail($id);

        if ($user->favourites()->find($id)) {
            $user->favourites()->detach($id);
        }

        return $this->jsonResponse("Success");
    }

    public function notifyAvailability($id)
    {
        $user = \Auth::user();
        if (!$user->stock_notifications->contains($id))
            $user->stock_notifications()->attach($id);
        return $this->jsonResponse("Success");
    }
}
