<?php

namespace App\Http\Controllers\Customer;

use App\Http\Resources\Customer\PaymentMethodsResource;
use App\Models\Payment\PaymentMethod;
use App\Models\Services\AramexService;
use App\Models\Services\OrdersService;
use Facades\App\Models\Repositories\CartRepository;
use Facades\App\Models\Services\SettingsService;
use App\Models\Services\ValidateOrder;
use App\Models\Services\WeAcceptPayment;
use Exception;
use Carbon\Carbon;
use App\Mail\OrderCreated;
use App\Models\Users\User;
use App\Models\Orders\Order;
use Illuminate\Http\Request;
use App\Models\Payment\Promo;
use App\Models\Users\Address;
use App\Models\Products\Product;
use App\Models\Orders\OrderState;
use App\Models\Payment\Promotion;
use App\Models\Settings\Settings;
use App\Models\Payment\PromoTypes;
use App\Models\Payment\Qnb\Parser;
use App\Notifications\OrderPlaced;
use Illuminate\Support\Facades\DB;
use App\Models\Orders\OrderManager;
use App\Models\Orders\OrderProduct;
use App\Models\Payment\Qnb\api_lib;
use App\Models\Services\SmsService;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Models\Payment\Qnb\Merchant;
use App\Models\Services\PushService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use App\Models\Payment\PaymentMethods;
use App\Models\Payment\PromoValidator;
use App\Models\Products\ProductReview;
use App\Models\Payment\PromoCalculator;
use Illuminate\Support\Facades\Session;
use App\Models\Services\LocationService;
use App\Models\Services\LoyalityService;
use Illuminate\Support\Facades\Validator;
use App\Models\Services\PromotionsService;
use App\Models\Services\ValUPaymentService;
use App\Models\Repositories\OrderRepository;
use Illuminate\Support\Facades\Notification;
use App\Models\Transformers\ProductTransformer;
use App\Models\Transformers\CustomerOrderTransformer;
use Illuminate\Validation\ValidationException;

class OrdersController extends Controller
{
    private $orderRepo;
    private $orderTrans;
    private $settings;
    private $pushService;
    private $smsService;
    private $loyality;
    private $valuService;
    private $productTrans;
    private $locationService;
    private $promotionsService;
    private $aramexService;

    public function __construct(AramexService $aramexService, WeAcceptPayment $acceptPayment, OrderRepository $orderRepo, CustomerOrderTransformer $orderTrans, OrdersService $ordersService, ValidateOrder $validateOrder, PushService $pushService, SmsService $smsService, LoyalityService $loyality, ValUPaymentService $valuService, ProductTransformer $productTrans, LocationService $locationService, PromotionsService $promotionsService)
    {
        $this->settings = SettingsService::getSystemSettings();
        $this->orderRepo = $orderRepo;
        $this->orderTrans = $orderTrans;
        $this->pushService = $pushService;
        $this->smsService = $smsService;
        $this->loyality = $loyality;
        $this->valuService = $valuService;
        $this->productTrans = $productTrans;
        $this->locationService = $locationService;
        $this->promotionsService = $promotionsService;
        $this->validateOrder = $validateOrder;
        $this->ordersService = $ordersService;
        $this->acceptPayment = $acceptPayment;
        $this->aramexService = $aramexService;
    }

    public function index()
    {
        $orders = Auth::user()->orders()->orderBy("created_at", "DESC")->get();
        return $this->jsonResponse("Success", $this->orderTrans->transformCollection($orders));
    }

    public function show($id)
    {
        $order = Auth::user()->orders()->findOrFail($id);

        return $this->jsonResponse("Success", $this->orderTrans->transform($order));
    }

    public function checkPromo(Request $request)
    {
        // validate request
        $validator = Validator::make($request->all(), [
            "promo" => "required",
            "amount" => "required"
        ], ["promo.exists" => "Promo code does not exist"]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }


        $promo = Promo::where("name", $request->promo)->first();

        $referer = User::where('referal', $request->promo)->first();

        if (!$promo && !$referer) {
            return $this->errorResponse("Promo code does not exist", "Invalid data", $validator->errors(), 422);
        }

        if ($referer && Auth::user()->first_order) {
            return $this->errorResponse("You already ordered before", "Invalid data", $validator->errors(), 422);
        }

        $settings = Settings::getSystemSettings();
        $amount = $request->amount;
        if ($referer && $amount < $settings->refer_minimum) {
            return $this->errorResponse("Refer doesn't meet minimum order amount", "Invalid data", $validator->errors(), 422);
        }

        if ($promo) {
            try {
                PromoValidator::validate($promo, Auth::user(), $request->amount, $request->items);
            } catch (\Exception $e) {
                return $this->errorResponse($e->getMessage(), "error", [], 409);
            }

            $discount = PromoCalculator::calculate($promo, $amount);

            $amount = $request->amount - $discount;
        }

        return $this->jsonResponse("Success", (int)$amount);
    }

    public function checkPromoV2(Request $request)
    {
        // validate request
        $this->validate($request, [
            "promo" => "required",
            "amount" => "required",
            "items" => "sometimes|array|filled"
        ], ["promo.exists" => "Promo code does not exist"]);

        $user = Auth::user();
        if (!$user->phone) {

            return $this->errorResponse(Lang::get('mobile.errorUpdateApp'), "Invalid data", [], 422);
        }

        if (!$user->phone_verified) {

            if ($request->verification_code && $request->verification_code != $user->verification_code) {
                return $this->errorResponse(Lang::get('mobile.errorIncorrectVerification'), "Invalid data", [], 404);
            } elseif ($request->verification_code && $request->verification_code == $user->verification_code) {
                $user->update(["phone_verified" => 1, "verification_code" => null]);
            } else {
                if (!$user->verification_code) {
                    $user->verification_code = rand(1000, 9999);
                    $user->save();
                }
                try {
                    $app_name = env("APP_NAME");
                    $this->smsService->sendSms($user->phone, $user->verification_code . " is your {$app_name} code.");
                } catch (\Exception $e) {
                    Log::error($e->getMessage());
                }
                return $this->errorResponse(Lang::get('mobile.errorUpdateApp'), "Invalid data", [], 409);
            }

        }


        $promo = Promo::where("name", $request->promo)->with(['paymentMethods' => function($q) {
            return $q->notClosedMethods();
        }])->first();

        $referer = User::where('referal', $request->promo)->first();

        if (!$promo && !$referer) {
            return $this->errorResponse(trans("mobile.errorPromoNotExist"), "Invalid data", [], 415);
        }

        if ($referer && Auth::user()->first_order) {
            return $this->errorResponse(trans("mobile.errorPromoFirstOrderOnly"), "Invalid data", [], 415);
        }

        $settings = Settings::getSystemSettings();
        $amount = $request->amount;
        if ($referer && $amount < $settings->refer_minimum) {
            return $this->errorResponse(trans("mobile.errorPromoMinimumAmount"), "Invalid data", [], 415);
        }

        if ($promo) {
            $preOrder = false;
            $free_delivery = false;
            $products = collect([]);
            $items = CartRepository::getUserCartItems();
            foreach ($items as $key => $item) {
                $product = Product::find($item["id"]);
                $product->amount = $item["amount"];
                if (!is_null($product->discount_price)) {
                }
                $products->push($product);
                if ($product->preorder === 1) {
                    $preOrder = true;
                }
            }

            $items = $this->promotionsService->checkForDiscounts($products);

            if ($promo->list_id) {
                $amount = 0;
                foreach ($items as $item) {
                    if ($promo->list->products()->where("products.id", $item->id)->orWhere("products.id", $item->parent_id)->exists()) {
                        $price = $item->active_discount ? $item->discount_price : $item->price;
                        $amount += $price;
                    }
                }
            }

            try {
                PromoValidator::validate($promo, Auth::user(), $amount, $items);
            } catch (\Exception $e) {
                return $this->errorResponse($e->getMessage(), "error", [], 415);
            }

            if ($promo->type == PromoTypes::FREE_DELIVERY) {
                $free_delivery = true;
            } else {
                $discount = PromoCalculator::calculate($promo, $amount);

                $amount = $request->amount - $discount;
            }

            $promoPaymentMethods = $promo->paymentMethods()->notClosedMethods()->get();
            $paymentMethods = $promoPaymentMethods->count() ? $promoPaymentMethods : PaymentMethod::notClosedMethods()->get();

            if (!is_null($this->settings->except_cod_amount) && $amount > $settings->except_cod_amount) {
                $paymentMethods = $paymentMethods->where('is_online', 1);
            }
            // if($preOrder){
            //     $paymentMethods->whereIn('id', [2,1]);
            // }
            return $this->jsonResponse("Success", [
                "amount" => $amount,
                "free_delivery" => $free_delivery,
                "referal" => false,
                "payment_methods" => PaymentMethodsResource::collection($paymentMethods)
            ]);
        } else {
            return $this->jsonResponse("Success", [
                "points" => $settings->refer_points,
                "free_delivery" => false,
                "referal" => true
            ]);
        }
    }

    public function getDeliveryFees(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "address_id" => "required|integer|exists:addresses,id",
            "items" => "sometimes|array|filled",
            "items.*id" => "required|integer|exists:products,id",
            "items.*.amount" => "required|integer",
        ]);
        $weight = 0;
        $numberOfPiece = 0;
        $preOrder = false;

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }
        $cartItems = CartRepository::getUserCartItems();
        foreach ($cartItems as $item) {
            $product = Product::findOrFail($item['id']);
            $weight += $product->weight * $item['amount'];
            $numberOfPiece += $item['amount'];

            if ($product->preorder === 1) {
                $preOrder = true;
            }
        }
        $address = Address::findOrFail($request->address_id);
        $settings = Settings::getSystemSettings();

        if (!isset($address->area->aramex_area_name) || $address->area->aramex_area_name == null) {
            return $this->errorResponse(trans("mobile.errorAramexLocation"), trans("mobile.errorAramexLocation"), $validator->errors(), 422);
        }
        if ($weight <= 0 || $numberOfPiece <= 0) {
            return $this->errorResponse("weight or Number of Pieces cannot be  zero ", "error", [], 422);
        }
        $totalAmount = $this->ordersService->getTotalAmount($request->all());
        if (!is_null($settings->min_order_amount) && $settings->min_order_amount <= $totalAmount / 100) {
            $fees = 0;
        } else {
            $aramexValidateAddress = $this->aramexService->validateAddress($address);
            if (!$aramexValidateAddress) {
                return $this->errorResponse(trans("mobile.errorAramexLocation"), "Invalid data", null, 422);
            }
            $fees = $this->aramexService->calculateFees($address, $weight, $numberOfPiece);
        }
        $paymentMethods = PaymentMethod::notClosedMethods();
        if (!is_null($this->settings->except_cod_amount) && ($totalAmount / 100) > $settings->except_cod_amount) {
            $paymentMethods->where('is_online', 1);
        }
        // if($preOrder){
        //     $paymentMethods->whereIn('id', [2,1]);
        // }
        $paymentMethods = $paymentMethods->get();
        $user = Auth::user();
        // $promos = $user->targetPromos()->whereDoesntHave('uses', function ($q) use ($user) {
        //     $q->where('id', $user->id);
        // })->where('expiration_date', '>', now())->where("active", 1)->orderBy('recurrence')->get();

        $promos = Promo::where(function ($qu) use ($user) {
            $qu->whereHas("target_lists", function ($q) use ($user) {
                $q->where("phone", $user->phone)->orWhere("user_id", $user->id);
            });
        })->whereDoesntHave("uses", function ($q) use ($user) {
            $q->where("id", $user->id);
        })->whereDate('expiration_date', '>=', Carbon::today()->toDateString())->where("active", 1)->orderBy('recurrence')->get();

        return $this->jsonResponse("Success", [
            "payment_methods" => PaymentMethodsResource::collection($paymentMethods),
            "total_amount" => (double)$totalAmount / 100,
            "delivery_fees" => (double)$fees,
            "promos" => $promos,
            "checkout_notes" => trans("mobile.checkoutNotes")
        ]);
    }

    public function validateCart(Request $request)
    {
        $products = collect([]);
        $items = $request->items;
        foreach ($items as $key => $item) {
            $product = Product::find($item["id"]);
            $product->amount = $item["amount"];
            $products->push($product);
        }
        $products = $this->promotionsService->checkForDiscounts($products);
        return $this->jsonResponse("Success", $this->productTrans->transformCollection($products));
    }

    public function createOrder(Request $request)
    {
        $settings = Settings::getSystemSettings();
        $user = auth()->user();
        $validator = $this->ordersService->validateOrder($request->all(), auth()->user(), $request->header("lang"));
        // validate request
        if (!$validator->valid()) {
            $error = $validator->errors()->first();

            if ($error->getCode() == 409) {
                if (!$user->verification_code) {
                    $user->verification_code = rand(1000, 9999);
                    $user->save();
                }
                try {
                    $this->smsService->sendSms($user->phone, $user->verification_code . " is your Trolley code.");
                } catch (\Exception $e) {
                    Log::error($e->getMessage());
                }
            }

            return $this->errorResponse($error->getMessage(), "Invalid data", $validator->errors(), $error->getCode());
        }
        $totalAmount = $this->ordersService->getTotalAmount($request->all());
        if (in_array($request->payment_method, [2, 3, 5])) {
            if (in_array($request->payment_method, [3, 5])) {
                $totalAndFees = $this->ordersService->getTotalAmount($request->all(), true);
                $totalAmount = $totalAndFees['total'];
            }
            $pay = $this->acceptPayment->Pay($totalAmount, $request->all());
            return $this->jsonResponse("Success", ['pay_url' => $pay], 202);
        }
        $order = $this->ordersService->createOrder($request->all(), null, auth()->User());
        $code = 200;
        $msg = "Success";
        $open_time = date("Y-m-d") . " " . $settings->open_time;
        $off_time = date("Y-m-d") . " " . $settings->off_time;
        if (strtotime(date("H:i")) < strtotime($open_time) || strtotime(date("H:i")) > strtotime($off_time)) {
            $msg = trans("mobile.errorStoreClosed");
            $code = 201;
        }

        return $this->jsonResponse($msg, $this->orderTrans->transform($order), $code);
    }

    public function cancelOrder(Request $request, $id)
    {
        $order = Auth::user()->orders()->findOrFail($id);
        $om = new OrderManager($order);
        try {
            $om->cancelOrder(Auth::user() , true);
        } catch (\Exception $e) {
            return $this->errorResponse("Can't cancel order because it is " . $order->state->name, "Invalid data", [], 422);
        }

        Auth::user()->settings ? app()->setLocale(Auth::user()->settings->language) : app()->setLocale('en');
        Mail::to(Auth::user())->send(new OrderCreated($order));
        $this->pushService->notifyAdmins("Order Cancelled", "Customer {$order->user_id} Has just cancelled order {$order->id}");
        return $this->jsonResponse("Success", $this->orderTrans->transform($order));
    }

    public function editSchedule(Request $request, $order_id)
    {
        $order = Auth::user()->orders()->findOrFail($order_id);
        if ($order->scheduled_at) {
            $order->update(["scheduled_at" => $request->schedule_date]);
        } else {
            $order->schedule()->delete();
            $order->createSchdule($request);
        }

        return $this->jsonResponse("Success", $this->orderTrans->transform($order));
    }

    public function cancelSchedule($order_id)
    {
        $order = Auth::user()->orders()->findOrFail($order_id);
        if ($order->scheduled_at) {
            $order->delete();
        } else {
            $order->schedule()->delete();
        }

        return $this->jsonResponse("Success");
    }

    public function editLateSchedule(Request $request, $order_id)
    {
        $order = Auth::user()->orders()->findOrFail($order_id);
        $order->update(["scheduled_at" => $request->schedule_date]);

        return $this->jsonResponse("Success", $this->orderTrans->transform($order));
    }

    public function cancelLateSchedule($order_id)
    {
        $order = Auth::user()->orders()->findOrFail($order_id);
        $order->delete();

        return $this->jsonResponse("Success");
    }

    public function rateOrder(Request $request, $id)
    {
        $order = Auth::user()->orders()->findOrFail($id);

        // validate request
        $validator = Validator::make($request->all(), [
            "rate" => "required|integer|in:1,2,3,4,5",
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }

        $order->rate = $request->rate;
        $order->feedback = $request->feedback;
        $order->save();

        if ($request->rate <= 2) {
            $this->pushService->notifyAdmins("Order Rating", "Order ID {$order->id} is rated {$request->rate}");
        }

        try {
            $order->deliverer->calculateRate();
        } catch (\Throwable $e) {
            // No deliverer found for this order
            Log::warning("Order has no Deliverer");
        }

        if ($request->items) {
            foreach ($request->items as $item) {
                $orderItem = $order->items()->where('product_id', $item["id"])->first();
                if ($orderItem) {
                    $data['type'] = 2;
                    $data['user_id'] = $order->user_id;
                    $data['product_id'] = $item['id'];
                    $data['order_id'] = $order->id;
                    $data['rate'] = $item['rate'];
                    $data['comment'] = isset($item['comment']) ? $item['comment'] : '';
                    $productReview = ProductReview::where('order_id', $order->id)->where('product_id', $item['id'])->where('user_id', $data['user_id'])->first();
                    if ($productReview) {
                        $productReview->update($data);
                    } else {
                        ProductReview::create($data);
                    }
                }
            }
        }

        return $this->jsonResponse("Success", $this->orderTrans->transform($order));
    }

    public function payOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "order_id" => "required|exists:orders,id",
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }

        $order = Order::findOrFail($request->order_id);

        //Creates the Merchant Object from config. If you are using multiple merchant ID's,
        // you can pass in another configArray each time, instead of using the one from configuration.php
        $configArray = array();
        $configArray["certificateVerifyPeer"] = FALSE;
        $configArray["certificateVerifyHost"] = 0;
        // $configArray["gatewayUrl"] = "https://qnbalahli.test.gateway.mastercard.com/api/nvp";
        $configArray["gatewayUrl"] = "https://qnbalahli.test.gateway.mastercard.com/api/nvp";
        $configArray["merchantId"] = "TESTQNBAATEST001";
        $configArray["apiUsername"] = "merchant.TESTQNBAATEST001";
        $configArray["password"] = "9c6a123857f1ea50830fa023ad8c8d1b";
        $configArray["debug"] = TRUE;
        $configArray["version"] = "49";
        $merchantObj = new Merchant($configArray);


        // The Parser object is used to process the response from the gateway and handle the connections
        // and uses connection.php
        $parserObj = new Parser($merchantObj);

        //The Gateway URL can be set by using the following function, or the
        //value can be set in configuration.php
        //$merchantObj->SetGatewayUrl("https://secure.uat.tnspayments.com/api/nvp");
        $requestUrl = $parserObj->FormRequestUrl($merchantObj);

        //This is a library if useful functions
        $new_api_lib = new api_lib;

        //Use a method to create a unique Order ID. Store this for later use in the receipt page or receipt function.
        $order_id = $new_api_lib->getRandomString(10);
        // dd($order->invoice->discount ?: $order->invoice->total_amount);
        //Form the array to obtain the checkout session ID.
        $request_assoc_array = array("apiOperation" => "CREATE_CHECKOUT_SESSION",
            "order.id" => $order->id,
            "order.amount" => (!is_null($order->invoice->discount) ? $order->invoice->discount : $order->invoice->total_amount) + $order->invoice->delivery_fees,
            "order.currency" => "EGP"
        );

        //This builds the request adding in the merchant name, api user and password.
        $rqst = $parserObj->ParseRequest($merchantObj, $request_assoc_array);

        //Submit the transaction request to the payment server
        $response = $parserObj->SendTransaction($merchantObj, $rqst);

        //Parse the response
        $parsed_array = $new_api_lib->parse_from_nvp($response);

        //Store the successIndicator for later use in the receipt page or receipt function.
        $successIndicator = $parsed_array['successIndicator'];

        //The session ID is passed to the Checkout.configure() function below.
        $session_id = $parsed_array['session.id'];

        //Store the variables in the session, or a database could be used for example
        Session::put('successIndicator', $successIndicator);
        Session::put('orderID', $order->id);


        // $merchantID = "'" . $merchantObj->GetMerchantId() . "'";
        $merchantID = $merchantObj->GetMerchantId();

        // echo $successIndicator . " " . $session_id;

        // dd($session_id, $successIndicator, $merchantID);
        return view("payment.checkout", ["merchantId" => $merchantID, "order_amount" => ($order->invoice->discount ?: $order->invoice->total_amount) + $order->invoice->delivery_fees, "customerId" => $order->user_id, "order_id" => $order->id, "order_currency" => "EGP", "session_id" => $session_id, "token" => $request->token]);
    }

    public function orderReceipt(Request $request)
    {
        $resultIndicator = $request->resultIndicator;
        // dd(Session::get("orderID"), Session::all());
        $order = Order::find(Session::get("orderID"));

        $om = new OrderManager($order);
        // dd($request->all(), Session::all(), $order);
        if ($resultIndicator == Session::get("successIndicator")) {
            $invoice = $order->invoice;
            if (!is_null($invoice->discount)) {
                $invoice->update(["paid_amount" => $invoice->discount + $invoice->delivery_fees]);
            } else {
                $invoice->update(["paid_amount" => $invoice->total_amount + $invoice->delivery_fees]);
            }
            $this->pushService->notify($order->customer, __('notifications.placingOrderTitle', ['orderId' => $order->id]), __('notifications.placingOrderBody'));
            $om->changeState(OrderState::CREATED, Auth::user());
            Auth::user()->settings ? app()->setLocale(Auth::user()->settings->language) : app()->setLocale('en');
            Mail::to(Auth::user())->send(new OrderCreated($order));
            return redirect(env("WEBSITE_URL") . "/checkout/redirect?order_id={$order->id}&success=1");
        } else {
            $om->cancelOrder(Auth::user(),true);
            return redirect(env("WEBSITE_URL") . "/account/orders/{$order->id}?failed=1");
        }
    }

    public function responseCallBack(Request $request, $provider)
    {
        $orderData = $this->acceptPayment->getOrderData($request->id);
        switch ($provider) {
            case 'weAccept':
                $transaction = $this->acceptPayment->payCallBack($request);
                if ($transaction->transaction_status === "true") {
                    $user = User::findOrFail($transaction->customer_id);
                    $request->request->add([
                        'transaction_user_agent' => $transaction->user_agent,
                        'valu_down_payment' => optional($orderData->source_data)->down_payment,
                        'valu_purchase_fees' => (float) optional($orderData->data)->purchase_fees,
                        "items" => isset($transaction->order_details['items']) && count($transaction->order_details['items']) ? $transaction->order_details['items'] : CartRepository::getUserCartItems($user)
                    ]);
                    $order = $this->ordersService->createOrder($transaction->order_details, $transaction->id, $user);
                    $transaction->update([
                        'order_id' => $order->id
                    ]);
                    return redirect(env("APP_URL") . '/checkout/final-receipt?is_success=true&order_status=' . $order->state->name . '&order_status_id=' . $order->state->id . '&order_id=' . $order->id);
                } else {
                    $message = isset($transaction->transaction_response['data_message']) ? $transaction->transaction_response['data_message'] : "Transaction did not completed";
                    return redirect(env("APP_URL") . '/checkout/final-receipt?is_success=false&message=' . $message . '');
                }
        }
        return false;
    }

    public function processesCallBack(Request $request, $provider)
    {
        switch ($provider) {
            case 'weAccept':
                Log::info($request->all());
                $callback = $this->acceptPayment->payCallBack($request);
                return $callback;
        }

        return false;
    }
}
