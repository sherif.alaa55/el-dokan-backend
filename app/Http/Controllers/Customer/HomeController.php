<?php

namespace App\Http\Controllers\Customer;

use App\Models\Services\PushService;
use App\Models\Users\User;
use App\Notifications\OrderStateChanged;
use App\Notifications\ProductAvailable;
use Illuminate\Support\Facades\Notification;
use Octw\Aramex\Aramex;
use App\Models\Products\Ad;
use App\Models\Home\Section;
use App\Models\Orders\Order;
use Illuminate\Http\Request;
use App\Models\Products\Brand;
use App\Models\Products\Product;
use App\Models\Products\CustomAd;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use App\Models\Shipping\OrderPickup;
use Illuminate\Support\Facades\Lang;
use App\Jobs\UpdateOrderPickupStatus;
use Illuminate\Support\Facades\Cache;
use App\Models\Services\PromotionsService;
use App\Models\Repositories\CartRepository;
use App\Http\Resources\Customer\AdsResource;
use App\Models\Repositories\ProductRepository;
use App\Http\Resources\Customer\BrandsResource;
use App\Models\Transformers\ProductTransformer;
use App\Models\Transformers\CustomerTransformer;
use App\Models\Repositories\CategoriesRepository;
use App\Models\Transformers\Customer\AdsTransformer;
use App\Models\Transformers\Customer\SectionTransformer;
use App\Models\Transformers\Customer\CategoryTransformer;
use App\Models\Transformers\Customer\CustomAdsTransformer;
use App\Http\Resources\Customer\CategoryWithGroupsResource;

class HomeController extends Controller
{
    private $categoryTrans;
    private $categoriesRepo;
    private $productRepo;
    private $productTrans;
    private $adsTrans;
    private $customerTrans;
    private $customAds;
    private $sectionTrans;
    private $cartRepo;
    private $pushService;
    protected $promotionsService;

    public function __construct(PushService $pushService,CustomAdsTransformer $customAds, CategoryTransformer $categoryTrans, SectionTransformer $sectionTrans, CategoriesRepository $categoriesRepo, ProductRepository $productRepo, ProductTransformer $productTrans, AdsTransformer $adsTrans, CustomerTransformer $customerTrans, CartRepository $cartRepo, PromotionsService $promotionsService)
    {
        $this->categoryTrans = $categoryTrans;
        $this->categoriesRepo = $categoriesRepo;
        $this->productRepo = $productRepo;
        $this->productTrans = $productTrans;
        $this->adsTrans = $adsTrans;
        $this->customerTrans = $customerTrans;
        $this->sectionTrans = $sectionTrans;
        $this->customAds = $customAds;
        $this->cartRepo = $cartRepo;
        $this->pushService = $pushService;
        $this->promotionsService = $promotionsService;
    }

    public function test(Request $request)
    {

        dispatch(new \App\Jobs\StockNotifications(Product::find(85217)));
dd('hi');
//        $order = Order::find(3408);
//        $order->customer->settings ? app()->setLocale($order->customer->settings->language) : app()->setLocale('en');
//
//        return view('emails.order_edited', ['order' => $order]);

//      UpdateOrderPickupStatus::dispatch();
//        dd('hiiiiii');

        //getCountries
//        $data = Aramex::fetchCountries();
//        $countryCode = "EG";
//        $data = Aramex::fetchCountries($countryCode);

//        $shipments = ["43829007913"];
//        $data = Aramex::trackShipments($shipments);

//         dd($data);
        //getCountry Cites
//        $data = Aramex::fetchCities($countryCode);
//        return $data->Cities->string;
        // dd($data);

        //Validate Address
//        $data = Aramex::validateAddress([
//            'line1' => 'Test', // optional (Passing it is recommended)
//            'line2' => 'Test', // optional
//            'line3' => 'Test', // optional
//            'country_code' => 'EG',
//            'postal_code' => '', // optional
//            'city' => 'Abasya',
//        ]);


        //Validate Address
//        $data = Aramex::validateAddress([
//            'line1' => 'Test', // optional (Passing it is recommended)
//            'line2' => 'Test', // optional
//            'line3' => 'Test', // optional
//            'country_code' => 'EG',
//            'postal_code' => '', // optional
//            'city' => 'Abasya',
//        ]);


        // Calculate Rate

//        $originAddress = [
//            'line_1' => 'Test string',
//            'city' => 'Amman',
//            'country_code' => 'JO'
//        ];
//
//        $destinationAddress = [
//            'line_1' => 'Test String',
//            'city' => 'Dubai',
//            'country_code' => 'AE'
//        ];
//
//        $shipmentDetails = [
//            'weight' => 5, // KG
//            'number_of_pieces' => 2,
//            'payment_type' => 'P', // if u don't pass it, it will take the config default value
//            'product_group' => 'EXP', // if u don't pass it, it will take the config default value
//            'product_type' => 'PPX', // if u don't pass it, it will take the config default value
//            'height' => 5.5, // CM
//            'width' => 3,  // CM
//            'length' => 2.3  // CM
//        ];
//
//        $shipmentDetails = [
//            'weight' => 5, // KG
//            'number_of_pieces' => 2,
//        ];
//
//        $currency = 'USD';
//        $data = Aramex::calculateRate($originAddress, $destinationAddress , $shipmentDetails , 'USD');
//
//        if(!$data->error){
//            dd($data);
//        }
//        else{
//            // handle $data->errors
//        }


        //cancel picup
//        $data = Aramex::cancelPickup('7480f4b4-fd36-4679-bf23-fae7d89148f3', 'ana asef ya basha ana 3yl so5yr');
//        dd($data);
    }

    public function index(Request $request)
    {
        $categories = $this->categoryTrans->transformCollection($this->categoriesRepo->getActiveCategories());

        $brands = Brand::all();

        $ads = Ad::active()->where("popup", 0)->where("banner_ad", 0)->orderBy("order", "ASC")->get();
        $ads->each->setAppends(["name"]);

        $banner_ads = Ad::active()->where("popup", 0)->where("banner_ad", 1)->get();
        $banner_ads->each->setAppends(["name"]);

        $popup_ads = Ad::active()->where("popup", 1)->where("banner_ad", 0)->orderBy("order", "ASC")->get();
        $popup_ads->each->setAppends(["name"]);

//        $discounted = $this->productRepo->getDiscounted();
//        $bought = $this->productRepo->getMostBought();
//        $recent = $this->productRepo->getMostRecent();

        $locale = $request->header("lang") == 2 ? "ar" : "en";

//        $sections = [
//            [
//                "name" => Lang::get("mobile.topPromotions", [], $locale),
//                "image" => URL::to('images/top-promotions.jpeg'),
//                "sort" => "offers",
//                "products" => $this->productTrans->transformCollection($discounted)
//            ],
//            [
//                "name" => Lang::get("mobile.mostBought", [], $locale),
//                "image" => URL::to('images/popular.jpeg'),
//                "sort" => "popular",
//                "products" => $this->productTrans->transformCollection($bought)
//            ],
//            [
//                "name" => Lang::get("mobile.mostRecent", [], $locale),
//                "image" => URL::to('images/popular.jpeg'),
//                "sort" => "recent",
//                "products" => $this->productTrans->transformCollection($recent)
//            ]
//        ];

        $sections = Section::where('active', 1)->orderBy('order')->get();
        $customAd = CustomAd::where('active', 1)->get();

        $user = \Auth::user();
        $user->token = (string)JWTAuth::getToken();
        $user_profile = $this->customerTrans->transform($user);

        return $this->jsonResponse("Success", [
            "sections" => $this->sectionTrans->transformCollection($sections),
            "categories" => $categories,
            "ads" => AdsResource::collection($ads),
            "custom_ads" => $this->customAds->transformCollection($customAd),
            "popup_ads" => AdsResource::collection($popup_ads),
            "banner_ads" => AdsResource::collection($banner_ads),
            "brands" => BrandsResource::collection($brands),
            "profile" => $user_profile
        ]);
    }

    public function newIndex(Request $request)
    {
        // $categoriesWithGroups = CategoryWithGroupsResource::collection($this->categoriesRepo->getActiveCategories());

        // $brands = Brand::all();
        // $brands = Cache::remember('brands', 60 * 30, function () {
        //     return Brand::all();
        // });

        // $ads = Ad::active()->where("popup", 0)->where("banner_ad", 0)->orderBy("order", "ASC")->get();
        $ads = Cache::remember('ads', 60 * 30, function () {
            return Ad::active()->where("popup", 0)->where("banner_ad", 0)->orderBy("order", "ASC")->get();
        });
        $ads->each->setAppends(["name"]);

        // $banner_ads = Ad::active()->where("popup", 0)->where("banner_ad", 1)->get();
        $banner_ads = Cache::remember('banner_ads', 60 * 30, function () {
            return Ad::active()->where("popup", 0)->where("banner_ad", 1)->get();
        });
        $banner_ads->each->setAppends(["name"]);

        // $popup_ads = Ad::active()->where("popup", 1)->where("banner_ad", 0)->orderBy("order", "ASC")->get();
        $popup_ads = Cache::remember('popup_ads', 60 * 30, function () {
            return Ad::active()->where("popup", 1)->where("banner_ad", 0)->orderBy("order", "ASC")->get();
        });
        $popup_ads->each->setAppends(["name"]);

        $locale = $request->header("lang") == 2 ? "ar" : "en";


        // $sections = Section::where('active', 1)->orderBy('order')->get();
        $sections = Cache::remember('sections', 60 * 30, function () {
            return Section::where('active', 1)->orderBy('order')->get();
        });

        // $customAd = CustomAd::where('active', 1)->get();
        $customAd = Cache::remember('customAd', 60 * 30, function () {
            return CustomAd::all();
        });

        // $user = \Auth::user();
        // $user->token = (string)JWTAuth::getToken();
        // $user_profile = $this->customerTrans->transform($user);

        return $this->jsonResponse("Success", [
            "sections" => $this->sectionTrans->transformCollection($sections),
            // "categories" => $categoriesWithGroups,
            "ads" => AdsResource::collection($ads),
            "custom_ads" => $this->customAds->transformCollection($customAd),
            "popup_ads" => AdsResource::collection($popup_ads),
            "banner_ads" => AdsResource::collection($banner_ads),
            // "brands" => BrandsResource::collection($brands),
            // "profile" => $user_profile,
            "app_version" => [
                "ios" => "1",
                "android" => "1.1",
            ],
        ]);
    }

    public function initialize()
    {
        $categoriesWithGroups = CategoryWithGroupsResource::collection($this->categoriesRepo->getActiveCategories());

        $user = \Auth::user();
        $user->token = (string)JWTAuth::getToken();
        $user_profile = $this->customerTrans->transform($user);

        $products = collect([]);
        $favourites = [];
        $compare = [];

        if ($user->id != 999999) {
            $cart = $user->cart;
            if ($cart) {
                foreach ($cart->cartItems as $item) {
                    $product = Product::find($item->product_id);
                    $product->amount = $item->amount;
                    $products->push($product);
                }
            }

            $favourites = $user->favourites;
            $compare = $user->comparisons;
        }

        $products = $this->promotionsService->checkForDiscounts($products);

        return $this->jsonResponse("Success", [
            "categories" => $categoriesWithGroups,
            "profile" => $user_profile,
            "cart" => $this->productTrans->transformCollection($products),
            "favourites" => $this->productTrans->transformCollection($favourites),
            "compare" => $this->productTrans->transformCollection($compare)
        ]);
    }
}
