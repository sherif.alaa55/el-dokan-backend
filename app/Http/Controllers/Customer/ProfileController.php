<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Resources\Client\EarnedPointsResource;
use App\Http\Resources\Client\ExpiredPointsResource;
use App\Http\Resources\Client\UsedPointsResource;
use App\Models\Services\LocationService;
use App\Models\Services\PushService;
use App\Models\Services\SmsService;
use App\Models\Transformers\CustomerTransformer;
use App\Models\Users\Address;
use App\Models\Users\DeviceToken;
use App\Models\Users\User;
use App\Notifications\WelcomeNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Tymon\JWTAuth\Facades\JWTAuth;

class ProfileController extends Controller
{
    private $customerTrans;
    private $pushService;
    private $locationService;
    private $smsService;

    public function __construct(CustomerTransformer $customerTrans, PushService $pushService, LocationService $locationService, SmsService $smsService)
    {
        $this->customerTrans = $customerTrans;
        $this->pushService = $pushService;
        $this->locationService = $locationService;
        $this->smsService = $smsService;
    }

    public function getProfile()
    {
        $user = \Auth::user();
        $user->token = (string)JWTAuth::getToken();
        return $this->jsonResponse("Success", $this->customerTrans->transform($user));
    }

    public function editProfile(Request $request)
    {
        // validate request
        $validator = Validator::make($request->all(), [
            // "phone" => ["required", Rule::unique("users")->ignore($user->id)],
            // "birthdate" => "required|before:" . date("Y-m-d"),
            "name" => "required",
            "closed_payment_methods" => "sometimes|array|exists:payment_methods,id",
        ], ["phone.unique" => "This phone number is already registered"]);

        $user = User::where("id", "!=", \Auth::id())->where('phone', $request->phone)->get()->first();
        if ($user && $user->phone_verified) {
            return $this->errorResponse(Lang::get("mobile.errorPhoneUsed"), "invalid data", [], 422);
        } else if ($user && !$user->phone_verified) {
            $user->phone = null;
            $user->save();
        }


        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }

        $user = \Auth::user();
        if ($user->phone != $request->phone) {
            $user->phone_verified = 0;
            $user->phone = $request->phone;
            $user->save();
        }
        if($request->closed_payment_methods) {
            foreach ($request->closed_payment_methods as $method) {
                $user->closedPaymentMethods()->create([
                    'payment_method_id' => $method
                ]);
            }
        }

        $user->update($request->only(["phone", "birthdate", "name", "last_name", "image"]));
        $user->token = (string)JWTAuth::getToken();

        return $this->jsonResponse("Success", $this->customerTrans->transform($user));
    }

    public function editPhone(Request $request)
    {
        // validate request
        $this->validate($request, [
            "phone" => "required"
        ]);

        $user = User::where("id", "!=", \Auth::id())->where('phone', $request->phone)->get()->first();
        if ($user && $user->phone_verified) {
            return $this->errorResponse(Lang::get("mobile.errorPhoneUsed"), "invalid data", [], 422);
        } else if ($user && !$user->phone_verified) {
            $user->phone = null;
            $user->save();
        }

        $user = \Auth::user();
        $user->phone_verified = 0;
        $user->verification_code = rand(1000, 9999);
        $user->phone = $request->phone;
        $user->save();

        try {
            $app_name = env("APP_NAME");
            $this->smsService->sendSms($user->phone, $user->verification_code . " is your {$app_name} code.");
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }

        $user->token = (string)JWTAuth::getToken();

        return $this->jsonResponse("Success", $this->customerTrans->transform($user));
    }

    public function resendVerification()
    {
        $user = \Auth::user();
        if ($user->phone_verified) {
            return $this->errorResponse(Lang::get("mobile.errorPhoneVerified"), "invalid data", [], 422);
        }

        $user->verification_code = rand(1000, 9999);
        $user->save();

        try {
            $app_name = env("APP_NAME");
            $this->smsService->sendSms($user->phone, $user->verification_code . " is your {$app_name} code.");
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }

        $user->token = (string)JWTAuth::getToken();

        return $this->jsonResponse("Success", $this->customerTrans->transform($user));
    }

    public function updateNotificationSettings(Request $request)
    {
        $this->validate($request, [
            "notify_general" => "required"
        ]);

        $user = auth()->user();
        $settings = $user->settings()->firstOrCreate([]);

        $settings->update($request->all());
        $user->token = (string)JWTAuth::getToken();

        return $this->jsonResponse("Success", $this->customerTrans->transform($user));
    }

    public function updateLanguageSettings(Request $request)
    {
        $this->validate($request, [
            "language" => "required"
        ]);

        $user = auth()->user();
        $settings = $user->settings()->firstOrCreate([]);

        $settings->update($request->only('language'));
        $user->token = (string)JWTAuth::getToken();

        return $this->jsonResponse("Success", $this->customerTrans->transform($user));
    }

    public function changePassword(Request $request)
    {
        // validate request
        $validator = Validator::make($request->all(), [
            "old_password" => "required",
            "password" => "required|min:8"
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }

        $user = \Auth::user();

        if(!\Auth::attempt(["email" => $user->email, "password" => $request->old_password])) {
            return $this->errorResponse("Incorrect Password", "invalid data", [], 403);
        }

        $user->password = bcrypt($request->password);
        $user->save();

        return $this->jsonResponse("Success");
    }

    public function verifyPhone(Request $request)
    {
        $this->validate($request, [
            "code" => "required"
        ]);

        $user = \Auth::user();
        if ($user->verification_code != $request->code) {
            return $this->errorResponse(trans('mobile.errorIncorrectVerification'), "Invalid data", [], 422);
        }

        $user->phone_verified = 1;
        $user->verification_code = null;
        $user->save();

        return $this->jsonResponse(trans("mobile.success"));
    }

    public function createAddress(Request $request)
    {
        // validate request
        $this->validate($request, [
            "city_id" => "required|exists:cities,id",
            "lat" => "required",
            "lng" => "required"
        ]);

        if (!$this->locationService->isLastNode($request->city_id, $request->area_id, $request->district_id)) {
            return $this->errorResponse(trans('mobile.errorInvalidAddress'), "Invalid data", [], 422);
        }

        $user = \Auth::user();

        if ($user->id == 999999) {
            return $this->jsonResponse("Success", $this->customerTrans->transform($user->load("addresses")));
        }

        $address = $user->addresses()->create($request->only(["name", "city_id", "area_id", "district_id", "address", "apartment", "floor", "landmark", "lat", "lng"]));

        if ($request->primary) {
            $user->addresses()->update(["primary" => 0]);
            $address->update(["primary" => 1]);
        }

        $user->token = (string)JWTAuth::getToken();

        return $this->jsonResponse("Success", $this->customerTrans->transform($user->load("addresses")));
    }

    public function updateAddress(Request $request, $id)
    {
        // validate request
        $this->validate($request, [
            "name" => "required",
            "address" => "required",
            "apartment" => "required",
            "floor" => "required",
            "lat" => "required",
            "lng" => "required"
        ]);

        if (!$this->locationService->isLastNode($request->city_id, $request->area_id, $request->district_id)) {
            return $this->errorResponse(trans('mobile.errorInvalidAddress'), "Invalid data", [], 422);
        }

        $user = \Auth::user();
        $address = $user->addresses()->find($id);

        $new_address = $address->replicate();
        $new_address->created_at = $address->created_at;
        $new_address->save();

        if (!$request->area_id) {
            $request->merge(["area_id" => null]);
        }
        if (!$request->district_id) {
            $request->merge(["district_id" => null]);
        }

        $new_address->update($request->only(["name", "address", "apartment", "floor", "landmark", "lat", "lng", "city_id", "area_id", "district_id","primary"]));

        if ($request->primary === true) {
            $user->addresses()->where('id', '!=', $new_address->id)->update(["primary" => 0]);
            $new_address->primary = 1;
            $new_address->save();
        }

        $address->delete();

        $user->refresh();
        $user->token = (string)JWTAuth::getToken();

        return $this->jsonResponse("Success", $this->customerTrans->transform($user->load("addresses")));
    }

    public function removeAddress(Request $request, $id)
    {
        $user = \Auth::user();
        $address = $user->addresses()->findOrFail($id);

        $user->addresses()->update(["primary" => 0]);
        $address->delete();

        $new_address = $user->addresses()->first();

        if ($new_address) {
            $new_address->update(["primary" => 1]);
        }

        $user->token = (string)JWTAuth::getToken();

        return $this->jsonResponse("Success", $this->customerTrans->transform($user->load("addresses")));
    }

    public function setPrimaryAddress($address_id)
    {
        $user = \Auth::user();

        $address = $user->addresses()->findOrFail($address_id);

        $user->addresses()->update(["primary" => 0]);
        $address->update(["primary" => 1]);

        $user->token = (string)JWTAuth::getToken();

        return $this->jsonResponse("Success", $this->customerTrans->transform($user->load("addresses")));
    }

    public function getPointHistory(Request $request)
    {
        $user = \Auth::user();

        $earned = $user->points;

        $used = $user->redeems;

        // group by expiration date and sum expired points
        $expired = $user->points()
            ->select("*", DB::raw("SUM(expired_points) as expirations"))
            ->groupBy(DB::raw("DATE(expiration_date)"))
            ->where("expiration_date", "<=", now())->get();

        // dd($expired);

        return $this->jsonResponse("Success", [
            "earned" => EarnedPointsResource::collection($earned),
            "used" => UsedPointsResource::collection($used),
            "expired" => ExpiredPointsResource::collection($expired)
        ]);
    }

    public function addToken(Request $request)
    {
        // validate request
        $validator = Validator::make($request->all(), [
            "token" => "required",
            "device_type" => "in:1,2"
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }

        $user = \Auth::user();

        $token_count = $user->tokens->count();

        // check if token exist
        Log::info("TOKEN API " . $request->token);
        if (DeviceToken::where("token", $request->token)->exists()) {
            Log::info("TOKEN EXIST");
            DeviceToken::where("token", $request->token)->delete();
        }

        $token = $user->tokens()->create(["token" => $request->token, "device" => $request->device_type]);

        if (!$token_count)
            Notification::send($user, new WelcomeNotification());

        return $this->jsonResponse("Success", $token_count);
    }
}
