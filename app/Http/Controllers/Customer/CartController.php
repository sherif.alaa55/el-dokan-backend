<?php

namespace App\Http\Controllers\Customer;

use App\Models\Orders\Cart;
use Illuminate\Http\Request;
use App\Models\Orders\CartItem;
use App\Models\Products\Product;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Services\PromotionsService;
use App\Http\Resources\Customer\CartResource;
use App\Models\Transformers\ProductTransformer;

class CartController extends Controller
{
    private $productTrans;

    protected $promotionsService;

    public function __construct(ProductTransformer $productTrans, PromotionsService $promotionsService)
    {
        $this->productTrans = $productTrans;
        $this->promotionsService = $promotionsService;
    }

    public function index()
    {
        $user = Auth::user();
        $cart = $user->cart;

        $products = collect([]);
        if ($cart) {
            foreach ($cart->cartItems as $item) {
                $product = Product::find($item->product_id);
                $product->amount = $item->amount;
                $products->push($product);
            }
        }

        $products = $this->promotionsService->checkForDiscounts($products);

        return $this->jsonResponse("Success", $this->productTrans->transformCollection($products));
    }

    public function store(Request $request)
    {
        $request->validate([
           'reset' => 'required|boolean',
           'items.*.id' => 'required|exists:products,id',
           'items.*.amount' => 'required'
        ]);
        $user = Auth::user();
        DB::beginTransaction();

        !($request->reset && $user->cart) ?: $user->cart()->delete();

        $cart = Cart::firstOrCreate(['user_id' => $user->id]);
        foreach ($request->items as $item) {
            $cartItem = CartItem::updateOrCreate([
                'cart_id' => $cart->id,
                'product_id' => $item['id']
            ],[
                'amount' => $item['amount']
            ]);
            $cartItem->amount != 0 ?: $cartItem->delete();
        }
        DB::commit();
        
        $products = collect([]);
        foreach ($cart->cartItems as $item) {
            $product = Product::find($item->product_id);
            $product->amount = $item->amount;
            $products->push($product);
        }

        $products = $this->promotionsService->checkForDiscounts($products);

        return $this->jsonResponse("Success", $this->productTrans->transformCollection($products));
    }
}
