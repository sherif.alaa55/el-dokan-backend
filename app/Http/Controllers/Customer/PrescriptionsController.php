<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Medical\Prescription;
use App\Models\Services\PushService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PrescriptionsController extends Controller
{
    private $pushService;

    public function __construct(PushService $pushService)
    {
        $this->pushService = $pushService;
    }


    public function index()
    {
    	$user = \Auth::user();

    	return $this->jsonResponse("Success", $user->prescriptions);
    }

    public function storeBase64(Request $request)
    {
    	// validate request
    	$validator = Validator::make($request->all(), Prescription::$validation);

    	if ($validator->fails()) {
    	    return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
    	}

    	$user = \Auth::user();

    	$image = base64_decode($request->image);

        $f = finfo_open();
        $result = finfo_buffer($f, $image, FILEINFO_MIME_TYPE);

        if(!preg_match("/image\//", $result)) {
            return $this->errorResponse("Invalid data type", "Invalid file format", [], 422);
        }
        $extension = str_replace("image/", "", $result);

        $rnd = str_random(6);
        $path = "prescriptions/prescriptions-{$rnd}.{$extension}";
        Storage::disk("public")->put($path, $image);
        $image_path = "storage/".$path;

    	$prescription = $user->prescriptions()->create([
            "name" => $request->name,
            "note" => $request->note,
            "image" => $image_path,
        ]);

        $this->pushService->notifyAdmins(z, "Customer ID {$user->id} has requested a prescription ID {$prescription->id}");

    	return $this->jsonResponse("Success", $prescription);
    }

    public function store(Request $request)
    {
        // validate request
        $validator = Validator::make($request->all(), Prescription::$validation);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }

        $user = \Auth::user();

        $prescription = $user->prescriptions()->create([
            "name" => $request->name,
            "note" => $request->note,
            "image" => $request->image,
        ]);

        $this->pushService->notifyAdmins("Medical Prescription", "Customer ID {$user->id} has requested a prescription ID {$prescription->id}");

        return $this->jsonResponse("Success", $prescription);
    }
}
