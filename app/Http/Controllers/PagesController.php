<?php

namespace App\Http\Controllers;



use App\Models\Pages\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;



class PagesController extends Controller
{

    public function index()
    {
        $page = Page::get();
        return $this->jsonResponse("Success", $page);
    }

    public function store(Request $request)
    {
        // validate request
        $validator = Validator::make($request->all(), Page::$validation + ["slug" => "sometimes|nullable|string|unique:pages,slug"]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }
        $data = $validator->valid();
        if (empty($data['slug'])){
            $data['slug'] = str_replace(' ','-',$data['title_en']);
        }
        $page = Page::create($data);
        return $this->jsonResponse("Success", $page);
    }

    public function update(Request $request, $id)
    {
        $page = Page::find($id);
        if (!$page){
            $message = 'Page Not Found';
            return $this->errorResponse($message, "Invalid data", 422);
        }
        // validate request
        $validator = Validator::make($request->all(), Page::$validation + ["slug" => "sometimes|nullable|string|unique:pages,slug,".$page->id]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }
        $data = $validator->valid();
        $page->update($data);
        return $this->jsonResponse("Success", $page);
    }

    public function activate($id)
    {
        $page = Page::find($id);
        if (!$page) {
            $message = 'This page Not Found';
            return $this->jsonResponse($message, $page);
        }
        $data = ['active' => '1'];
        $page->update($data);
        return $this->jsonResponse("Success");
    }

    public function deactivate(Request $request, $id)
    {
        $page = Page::find($id);
        if (!$page) {
            $message = 'This page Not Found';
            return $this->jsonResponse($message, $page);
        }
        $data = ['active' => '0'];
        $page->update($data);
        return $this->jsonResponse("Success");
    }
}
