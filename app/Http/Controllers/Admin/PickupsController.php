<?php

namespace App\Http\Controllers\Admin;

use App\Models\Orders\OrderState;
use App\Models\Shipping\OrderPickup;
use Octw\Aramex\Aramex;
use Illuminate\Http\Request;
use App\Models\Shipping\Pickup;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Models\Services\AramexService;
use App\Http\Resources\Admin\PickupResource;

class PickupsController extends Controller
{
    protected $aramexService;

    public function __construct(AramexService $aramexService)
    {
        $this->aramexService = $aramexService;
    }

    public function index(Request $request)
    {
        $pickups = Pickup::with("order_pickups")->orderBy("created_at", "DESC")->get();

        return $this->jsonResponse("Success", $pickups);
    }

    public function show($id)
    {

        $pickup = Pickup::with("order_pickups", "orders")->find($id);

//        $orderPicUpsShippingIDS = $pickup->order_pickups->pluck('shipping_id')->toArray();

        $orderPickups = $pickup->order_pickups;
        foreach ($orderPickups as $orderPickup) {
            $state = $this->aramexService->getShipmentState($orderPickup->shipping_id);
            $orderPickup->update(["update_description" => $state["UpdateDescription"], "status" => $state["UpdateCode"]]);
            if (OrderPickup::PICKUP_CANCELLED == $state['UpdateCode']) {
//                $orderPickup->order()->update(['state_id' => OrderState::CANCELLED]);
            } elseif (OrderPickup::PICKUP_RETURNED == $state['UpdateCode']) {
                $orderPickup->order()->update(['state_id' => OrderState::DELIVERY_FAILED]);
            } elseif (OrderPickup::PICKUP_DELIVERED == $state['UpdateCode']) {
                $orderPickup->order()->update(['state_id' => OrderState::DELIVERED]);
            }elseif (OrderPickup::PICKUP_RECEIVED_AT_ORIGIN_FACILITY == $state['UpdateCode']) {
                $orderPickup->order()->update(['state_id' => OrderState::ONDELIVERY]);
            }

            // try {
            //     $TrackingData = Aramex::trackShipments(["$orderPickup->shipping_id"]);


            //     if (!$TrackingData->HasErrors) {
            //         $trackingResult = $TrackingData->TrackingResults->KeyValueOfstringArrayOfTrackingResultmFAkxlpY->Value->TrackingResult;
            //         if (is_array($trackingResult)) {
            //             $lastUpdateCode = $trackingResult[0]->UpdateCode;
            //             $lastTrackingUpdate = $trackingResult[0]->UpdateDescription;
            //         }else {
            //             $lastUpdateCode = $trackingResult->UpdateCode;
            //             $lastTrackingUpdate = $trackingResult->UpdateDescription;
            //             $trackingResult = [$trackingResult];
            //         }
            //         $orderPickup->update(['update_description'=>$lastTrackingUpdate,'tracking_result'=>$trackingResult,'status'=>$lastUpdateCode]);
            //     }
            // }catch (\Exception $e) {

            //     Log::error($e->getMessage());
            // }

        }
//        $orderPicUpsShippingIDS = array_map(function($value) {return (string)$value;}, $orderPicUpsShippingIDS);
//        dd($data);
        return $this->jsonResponse("Success", new PickupResource($pickup));
    }
}
