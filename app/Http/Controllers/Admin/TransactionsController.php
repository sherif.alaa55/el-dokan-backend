<?php

namespace App\Http\Controllers\Admin;

use App\Http\Resources\Admin\TransactionResource;
use App\Models\Orders\Transaction;
use App\Models\Products\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class TransactionsController extends Controller
{

    public function index()
    {
        $transactions = Transaction::with('customer')->paginate(20);
        return $this->jsonResponse("Success", ["transactions" => TransactionResource::collection($transactions),"total" => $transactions->total()]);
    }

    public function show($id)
    {
        $transaction = Transaction::findOrFail($id);
        return $this->jsonResponse("Success", new TransactionResource($transaction));
    }

}
