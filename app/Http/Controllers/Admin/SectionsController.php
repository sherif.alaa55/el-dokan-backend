<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Models\Home\Section;
use App\Models\Products\ListItems;
use App\Models\Products\Lists;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;


class SectionsController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = Section::with('list')->get();
        return $this->jsonResponse("Success", $list);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate request
        $validator = Validator::make($request->all(), Section::$validation);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }
        $data = $validator->valid();
        $section = Section::create($data);
        $section->list;
        Cache::forget('sections');
        return $this->jsonResponse("Success", $section);
    }


    public function update(Request $request, $id)
    {
        $section = Section::find($id);
        if (!$section){
            $message = 'section Not Found';
            return $this->errorResponse($message, "Invalid data", 422);
        }
        // validate request
        $validator = Validator::make($request->all(), Section::$validation);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }
        $data = $validator->valid();
        $section->update($data);
        $section->list;
        Cache::forget('sections');
        return $this->jsonResponse("Success", $section);
    }

    public function activate($id)
    {
        $section = Section::find($id);
        if (!$section) {
            $message = 'This section Not Found';
            return $this->jsonResponse($message, $section);
        }
        $data = ['active' => '1'];
        $section->update($data);
        Cache::forget('sections');
        return $this->jsonResponse("Success");
    }

    public function deactivate(Request $request, $id)
    {
        $section = Section::find($id);
        if (!$section) {
            $message = 'This section Not Found';
            return $this->jsonResponse($message, $section);
        }
        $data = ['active' => '0'];
        $section->update($data);
        Cache::forget('sections');
        return $this->jsonResponse("Success");
    }
}
