<?php

namespace App\Http\Controllers\Admin;

use App\Models\Orders\Order;
use App\Models\Products\Product;
use App\Models\Repositories\CustomerRepository;
use App\Models\Services\SmsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
class DashboardController extends Controller
{
    private $customerRepo;
    private $smsService;

    public function __construct(CustomerRepository $customerRepo, SmsService $smsService)
    {
        $this->customerRepo = $customerRepo;
        $this->smsService = $smsService;
    }

    
    public function index()
    {
        $products = Product::MainProduct()->count();
        $orders = Order::count();
        $customers = $this->customerRepo->customerCount();

        try {
            $smsCredit = $this->smsService->checkCredit();
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            $smsCredit = null;
        }

        return $this->jsonResponse("Success", [
            "products" => $products,
            "orders" => $orders,
            "customers" => $customers,
            "smsCredit" => $smsCredit
        ]);
    }
    public function clearCache()
    {
        Artisan::call('cache:clear');
        return $this->jsonResponse("cache Cleared Successfully", null);
    }
}
