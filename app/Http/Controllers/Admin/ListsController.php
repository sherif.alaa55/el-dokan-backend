<?php

namespace App\Http\Controllers\Admin;

use App\Models\Products\Ad;
use App\Models\Products\Brand;
use App\Models\Products\Category;
use App\Models\Products\ListItems;
use App\Models\Products\ListRules;
use App\Models\Products\Lists;
use App\Models\Products\Product;
use App\Models\Products\Tag;
use App\Models\Transformers\ProductFullTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class ListsController extends Controller
{
    private $productTrans;
    private $lists;

    public function __construct(ProductFullTransformer $productTrans, Lists $lists)
    {
        $this->productTrans = $productTrans;
        $this->lists = $lists;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = Lists::with('products')->get();
        return $this->jsonResponse("Success", $list);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate request
        $validator = Validator::make($request->all(), Lists::$validation);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }
        $data = $validator->valid();
        $list = Lists::create($data);


        if ($request->list_method == 0) {
            if (isset($request->items) && is_array($request->items))
                $list->manualProductLists($request->items);
        } elseif ($request->list_method == 1) {
            if (isset($request->rules) && is_array($request->rules))
                $list->conditionProductLists($request->rules, $request->condition_type, $list->id);
        }
        if ($request->list_method == 0) {
            $list->products = $this->productTrans->transformCollection($list->products);
        }
        $list->listRules;
        return $this->jsonResponse("Success", $list);
    }

    public function update(Request $request, $id)
    {
        $list = Lists::find($id);
        if (!$list) {
            $message = 'List Not Found';
            return $this->errorResponse($message, "Invalid data", 422);
        }
        // validate request
        $validator = Validator::make($request->all(), Lists::$validation);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }
        $data = $validator->valid();
        $list->update($data);
        ListItems::where('list_id', $list->id)->delete();
        ListRules::where('list_id', $list->id)->delete();
        if ($request->list_method == 0) {
            if (isset($request->items) && is_array($request->items))
                $list->manualProductLists($request->items);
        } elseif ($request->list_method == 1) {
            if (isset($request->roles) && is_array($request->roles))
                $list->conditionProductLists($request->roles, $request->condition_type, $list->id);
        }
        if ($request->list_method == 0) {
            $list->products = $this->productTrans->transformCollection($list->products);
        }
        $list->listRules;
        return $this->jsonResponse("Success", $list);
    }

    public function activate($id)
    {
        $list = Lists::find($id);
        if (!$list) {
            $message = 'This list Not Found';
            return $this->jsonResponse($message, $list);
        }
        $data = ['active' => '1'];
        $list->update($data);
        return $this->jsonResponse("Success");
    }

    public function deactivate(Request $request, $id)
    {
        $list = Lists::find($id);
        if (!$list) {
            $message = 'This list Not Found';
            return $this->jsonResponse($message, $list);
        }
        $data = ['active' => '0'];
        $list->update($data);
        return $this->jsonResponse("Success");
    }

    public function sync()
    {
        $this->lists->sync();
        return $this->jsonResponse("Success", '');
    }

    public function exportListItems($id)
    {
        $list = Lists::find($id);
        if (!$list) {
            $message = 'This list Not Found';
            return $this->jsonResponse($message, $list);
        }

        return Excel::create($list->name_en.'_' . date("Ymd"), function ($excel) use ($list) {
            $excel->sheet('report', function ($sheet) use ($list) {
                $sheet->row(1, ["sku"]);
                if (count($list->products) > 0) {
                    foreach ($list->products as $key => $product) {
                        $sheet->row($key + 2, [
                            $product->sku,
                        ]);
                    }
                }

            });
        })->download('xlsx');

    }

    public function importListItems(Request $request,$id)
    {
        $totalCount = 0;
        $totalAddedCount = 0;
        $totalMissedCount = 0;
        $missedDetails = [];
        $this->validate($request, ["file" => "required"]);
        $list = Lists::findOrFail($id);
        try {
            Excel::load($request->file, function ($reader) use ($list,&$totalCount, &$totalAddedCount, &$totalMissedCount,&$missedDetails ){
                // Loop through all sheets
                $results = $reader->all();
                //$list->listItems()->delete();
                foreach ($results as $key => $record) {
                    $totalCount++;
                    $product = Product::where('sku',$record)->first();
                    if ($product){
                        $totalAddedCount++;
                            $data = [
                                'item_id'=>$product->id,
                                'list_id'=>$list->id,
                            ];
                        ListItems::updateOrCreate($data);
                    }else{
                        $totalMissedCount++;
                        $missedDetails[] = 'product with sku ' . $record . '( Sku Not Found )';
                    }
                }
            });
        } catch (\Exception $e) {
            Log::error("HandleFileError: " . $e->getMessage());
        }
        $data = [
            'total_count' => $totalCount,
            'total_added_count' => $totalAddedCount,
            'missed_count' => $totalMissedCount,
            'missed_details' => $missedDetails,
        ];
        return $this->jsonResponse("Success", $data);
        // return response
    }

//    public function destroy($id)
//    {
//        $options = Option::find($id);
//        if (!$options) {
//            $message = 'This option Not Found';
//            $options =[];
//            return $this->jsonResponse($message, $options);
//        }
//
//        $options->delete();
//        return $this->jsonResponse("Success");
//    }
}
