<?php

namespace App\Http\Controllers\Admin;

use App\Models\ACL\Role;
use App\Models\Users\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;
use App\Models\Repositories\CustomerRepository;
use App\Models\Transformers\CustomerFullTransformer;
use App\Models\Transformers\CustomerSimpleTransformer;

class CustomersController extends Controller
{
	private $customerRepo;
	private $customerTrans;
    private $fullTrans;

	public function __construct(CustomerRepository $customerRepo, CustomerSimpleTransformer $customerTrans, CustomerFullTransformer $fullTrans)
	{
		$this->customerRepo = $customerRepo;
		$this->customerTrans = $customerTrans;
        $this->fullTrans = $fullTrans;
	}


    public function index(Request $request)
    {
    	$query = User::with("addresses.area")->where("type", 1);

        $customer = $this->customerRepo->searchCustomers($request->all(),$query);

        $customers = $customer->paginate(20);

    	return $this->jsonResponse("Success", ["customers" => $this->customerTrans->transformCollection($customers->items()), "total" => $customers->total()]);
    }

    public function simpleList()
    {
        $customers = User::with("addresses")->where("type", 1)->get(["id", "name"]);

        return $this->jsonResponse("Success", $customers);
    }

    public function search(Request $request)
    {
        $query = User::with("addresses.area")->where("type", 1);

        $customer = $this->customerRepo->searchCustomers($request->all(),$query);

        $customers = $customer->get();

    	return $this->jsonResponse("Success", ["customers" => $this->customerTrans->transformCollection($customers), "total" => $customers->count()]);
    }

    public function searchCustomers(Request $request)
    {
        $query = User::with("addresses.area")->where("type", 1);

        $customer = $this->customerRepo->searchCustomers($request->all(),$query);

        $customers = $customer->paginate(20);

    	return $this->jsonResponse("Success", ["customers" => $this->customerTrans->transformCollection($customers->items()), "total" => $customers->total()]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required",
            "last_name" => "sometimes|nullable",
            "phone" => "required",
            "email" => "required|email|unique:users,email",
            "birthdate" => "sometimes|nullable",
            "password" => "required|min:8"
        ]);

        if (User::where("phone", $request->phone)->where("phone_verified", 1)->exists()) {
            return $this->errorResponse("Phone number already registered", "Invalid data", [], 400);
        }

        $request->merge(["type" => 1, "password" => bcrypt($request->password), "active" => 1, "phone_verified" => 1]);
        $user = User::create($request->only(["name", "last_name", "phone", "email", "password", "type", "active", "phone_verified", "birthdate"]));
        $user->refresh();

        $settings = $user->settings()->firstOrCreate(['language' => 'en']);

        return $this->jsonResponse("Success", $this->customerTrans->transform($user));
    }

    public function update(Request $request, $id)
    {
        $user = User::where("id", $id)->where("type", 1)->first();
        $this->validate($request, [
            "name" => "required",
            "last_name" => "sometimes|nullable",
            "phone" => "required",
            "email" => "required|email|unique:users,email,{$id}",
            "birthdate" => "sometimes|nullable",
            "password" => "sometimes|nullable|min:8",
            "closed_payment_methods" => "sometimes|array"
        ]);

        if (User::where("phone", $request->phone)->where("phone_verified", 1)->where("id", "!=", $id)->exists()) {
            return $this->errorResponse("Phone number already registered", "Invalid data", [], 400);
        }

        $request->merge(["phone_verified" => 1]);
        $user->update($request->only(["name", "last_name", "phone", "email", "birthdate", "phone_verified"]));
        if ($request->password) {
            $user->update(["password" => bcrypt($request->password)]);
        }

        if($request->closed_payment_methods) {
            $user->closedPaymentMethods()->sync($request->closed_payment_methods);
        }
        return $this->jsonResponse("Success", $this->customerTrans->transform($user));
    }


    public function show($id)
    {
    	$customer = $this->customerRepo->getCustomerById($id);

    	return $this->jsonResponse("Success", $this->fullTrans->transform($customer));
    }

    public function activate($id)
    {
    	$customer = $this->customerRepo->getCustomerById($id);

    	$customer->active = 1;
        $customer->deactivation_notes = null;
    	$customer->save();

    	return $this->jsonResponse("Success");
    }

    public function deactivate(Request $request, $id)
    {
    	// validate request
    	$validator = Validator::make($request->all(), ["deactivation_notes" => "required"]);

    	if ($validator->fails()) {
    	    return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
    	}

    	$customer = $this->customerRepo->getCustomerById($id);

    	$customer->active = 0;
    	$customer->deactivation_notes = $request->deactivation_notes;
    	$customer->save();

    	return $this->jsonResponse("Success");
    }

    public function export()
    {
        $customers = $this->customerRepo->getAllCustomers();

    	return Excel::create('customers_' . date("Ymd"), function ($excel) use ($customers){
                $excel->sheet('report', function ($sheet) use ($customers){
                    // $sheet->fromArray($players);
                    $sheet->row(1, ["id", "name", "email", "birthdate", "address", "contact", "total_orders","language", "active"]);

                    foreach ($customers as $key => $customer) {
                        // dd(array_values($player));
                        $sheet->row($key + 2, [
                            $customer->id,
                            $customer->name,
                            $customer->email,
                            $customer->birthdate ? date("Y-m-d", strtotime($customer->birthdate)) : "",
                            $customer->addresses->first() ? $customer->addresses->first()->address : "",
                            $customer->phone,
                            $customer->orders->count(),
                            $customer->settings->language ?? null,
                            (int)$customer->active
                        ]);
                    }
                });
            })->download('xlsx');
    }

    public function exportCartItems()
    {
        $customers = $this->customerRepo->getAllCustomers()->filter(function ($customer) {
            return (bool) optional($customer->cart)->count();
        });
    	return Excel::create('customers_' . date("Ymd"), function ($excel) use ($customers){
                $excel->sheet('report', function ($sheet) use ($customers){
                    // $sheet->fromArray($players);
                    $sheet->row(1, ["id", "name", "email", "contact", "items","last_update"]);
                    $index = 0;
                    foreach ($customers as $key => $customer) {
                        $skus = "";
                        $lastUpdate = "";
                        if($customer->cart) {
                            $cartItems = $customer->cart->cartItems;
                            $skus = implode(",", $cartItems->pluck('product')->pluck('sku')->toArray());
                            $lastUpdate = $cartItems->sortByDesc('updated_at')->first()->updated_at;
                        }
                        $sheet->row($index + 2, [
                            $customer->id,
                            $customer->name,
                            $customer->email,
                            $customer->phone,
                            $skus,
                            $lastUpdate
                        ]);
                        $index++;
                    }
                });
            })->download('xlsx');
    }

    public function verifyPhone($id)
    {
        $user = User::findOrFail($id);

        $user->phone_verified = 1;
        $user->save();

        return $this->jsonResponse("Success", $this->fullTrans->transform($user));
    }

    public function getCustomerToken($id)
    {
        $customer = $this->customerRepo->getCustomerById($id);

        $token = JWTAuth::fromUser($customer);

        return $this->jsonResponse("Success", $token);
    }
}
