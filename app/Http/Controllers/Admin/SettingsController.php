<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Settings\Settings;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    
    public function getSettings()
    {
    	return $this->jsonResponse("Success", Settings::getSystemSettings());
    }

    public function updateLoyalitySettings(Request $request)
    {
    	$this->validate($request, [
    		"ex_rate_pts" => "required",
    		"ex_rate_egp" => "required",
    		"ex_rate_gold" => "required",
    		"egp_gold" => "required",
    		"pending_days" => "required",
    		"refer_points" => "required",
    		"refer_minimum" => "required"
    	]);

    	$settings = Settings::getSystemSettings();
    	$settings->ex_rate_pts = $request->ex_rate_pts;
    	$settings->ex_rate_egp = $request->ex_rate_egp;
    	$settings->ex_rate_gold = $request->ex_rate_gold;
    	$settings->egp_gold = $request->egp_gold;
    	$settings->pending_days = $request->pending_days;
    	$settings->refer_points = $request->refer_points;
    	$settings->refer_minimum = $request->refer_minimum;
    	$settings->save();

    	return $this->jsonResponse("Success", $settings);
    }

    public function updateSystemSettings(Request $request)
    {
    	$settings = Settings::getSystemSettings();
    	$settings->min_order_amount	= $request->min_order_amount;
    	$settings->except_cod_amount	= $request->except_cod_amount;
    	$settings->off_time	= $request->off_time;
        $settings->open_time = $request->open_time;
    	$settings->save();

    	return $this->jsonResponse("Success", $settings);
    }
}
