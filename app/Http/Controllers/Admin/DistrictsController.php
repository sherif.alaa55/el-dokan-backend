<?php

namespace App\Http\Controllers\Admin;

use App\Models\Locations\Area;
use App\Models\Locations\District;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
class DistrictsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($area_id)
    {
        $districts = Area::findOrFail($area_id)->districts()->with("area")->get();

        return $this->jsonResponse("Success", $districts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $area_id)
    {
        // validate request
        $this->validate($request, [
            "name" => "required",
            "name_ar" => "required",
            "delivery_fees" => "required"
        ]);

        $area = Area::findOrFail($area_id);
        $request->merge(["active" => 1]);
        $district = $area->districts()->create($request->only(["name", "area_id", "delivery_fees", "active"]));
        $district->refresh();

        return $this->jsonResponse("Success", $district->load("area"));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($area_id, $id)
    {
        $district = District::findOrFail($id);

        return $this->jsonResponse("Success", $district->load("city"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $area_id, $id)
    {
        $district = District::findOrFail($id);

        // validate request
        $this->validate($request, [
            "name" => "required",
            "delivery_fees" =>  "required"
        ]);

        $district->update($request->only(["name", "name_ar", "delivery_fees"]));

        return $this->jsonResponse("Success", $district->load("area"));
    }

    public function updateDeliveryFees(Request $request)
    {
        $this->validate($request, [
            "district_ids" => "required|array",
            "delivery_fees" => "required"
        ]);

        District::whereIn("id", $request->district_ids)->update(["delivery_fees" => $request->delivery_fees]);

        return $this->jsonResponse("Success");
    }

    public function activate($area_id, $id)
    {
        $district = District::findOrFail($id);

        $district->active = 1;
        $district->deactivation_notes = null;
        $district->save();

        return $this->jsonResponse("Success");
    }

    public function deactivate(Request $request, $area_id, $id)
    {
        // validate request
        $this->validate($request, ["deactivation_notes" => "required"]);

        $district = District::findOrFail($id);

        $district->active = 0;
        $district->deactivation_notes = $request->deactivation_notes;
        $district->save();

        return $this->jsonResponse("Success");
    }
}
