<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Payment\PaymentMethod;
use Illuminate\Http\Request;

class PaymentMethodController extends Controller
{
    public function index()
    {
        return $this->jsonResponse('success', PaymentMethod::all());
    }

    public function activateMethod($methodID)
    {
        $method = PaymentMethod::find($methodID);
        $method->update(['active' => 1]);
        return $this->jsonResponse('success', $method);
    }

    public function deactivateMethod($methodID)
    {
        request()->validate([
            'deactivation_notes' => 'required',
        ]);
        $method = PaymentMethod::find($methodID);
        $method->update([
            'active' => 0,
            'deactivation_notes' => request('deactivation_notes')
        ]);
        return $this->jsonResponse('success', $method);
    }
}
