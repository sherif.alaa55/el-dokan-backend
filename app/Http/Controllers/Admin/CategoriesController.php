<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Products\Category;
use App\Models\Products\CategoryOption;
use App\Models\Products\Option;
use App\Models\Products\Product;
use App\Models\Repositories\CategoriesRepository;
use App\Models\Repositories\ProductRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;

class CategoriesController extends Controller
{
    private $categoriesRepo;
    private $productsRepo;

    public function __construct(CategoriesRepository $categoriesRepo, ProductRepository $productsRepo)
    {
        $this->categoriesRepo = $categoriesRepo;
        $this->productsRepo = $productsRepo;
    }

    public function index()
    {
        $categories = $this->categoriesRepo->getMainCategories();

        //$categories = $categories->transform(function ($item) {
        //    $item->product_count = $item->getProductCount();
        //    $item->products = [];
        //    return $item;
        //});
        return $this->jsonResponse("Success", $categories);
    }

    public function store(Request $request)
    {
        // validate request
        $validator = Validator::make($request->all(), Category::$validation + ["name" => "required"]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }

        $request->merge(["created_by" => \Auth::id()]);
        $category = Category::create($request->only(["name", "name_ar", "description", "description_ar", "created_by", "order", "image"]));
        $category->active = 1;
        $category->save();

        foreach ($request->sub_categories as $sub) {
            $subCategory = $category->subCategories()->create(["name" => $sub["name"], "name_ar" => $sub["name_ar"], "image" => $sub["image"], "active" => 1]);
            if (isset($sub['options']) && is_array($sub['options'])) {
                foreach ($sub['options'] as $optionID) {
                    $option = Option::find($optionID);
                    if ($option) {
                        $categoryOpthionData = [
                            'sub_category_id' => $subCategory->id,
                            'option_id' => $optionID,
                            'created_by' => \Auth::id(),
                        ];
                        CategoryOption::create($categoryOpthionData);
                    }
                }
            }
        }

        return $this->jsonResponse("Success", $category->load("subCategories", "creator"));
    }

    public function show($id)
    {
        $category = $this->categoriesRepo->getCategoryById($id);

        return $this->jsonResponse("Success", $category->load("subCategories"));
    }

    public function update(Request $request, $id)
    {
        $category = $this->categoriesRepo->getCategoryById($id);

        // validate request
        $validator = Validator::make($request->all(), Category::$validation + ["name" => "required"]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }
        $category->update($request->only(["name", "name_ar", "description", "description_ar", "order", "image","active"]));

        if ($request->deleted_ids) {
            $deleted_ids = $request->deleted_ids;
            Category::whereIn("id", $deleted_ids)->delete();
        }


        foreach ($request->sub_categories as $sub) {
            $subCatOrder= isset($sub["order"]) ? $sub["order"] : 0 ;
            if (isset($sub["id"])) {
                $subCategory = Category::find($sub["id"]);
                $subCategory->update(["name" => $sub["name"], "name_ar" => $sub["name_ar"], "image" => $sub["image"] , "order"=>$subCatOrder]);
                CategoryOption::whereNotIn('option_id',$sub['options'])->where('sub_category_id',$sub["id"])->delete();
                if (is_array($sub['options'])) {
                    foreach ($sub['options'] as $optionID) {
                        $option = Option::find($optionID);
                        $catOption = CategoryOption::where('option_id',$optionID)->where('sub_category_id',$sub["id"])->first();
                        if (!isset($catOption) && isset($option)){
                            $categoryOpthionData = [
                                'sub_category_id' => $subCategory->id,
                                'option_id' => $optionID,
                                'created_by' => \Auth::id(),
                            ];
                             CategoryOption::create($categoryOpthionData);
                        }
                    }
                }
            } else {
                $subCategory = $category->subCategories()->create(["name" => $sub["name"], "name_ar" => $sub["name_ar"], "image" => $sub["image"], "active" => 1,"order"=>$subCatOrder]);
                if (isset($sub['options']) && is_array($sub['options'])) {
                    foreach ($sub['options'] as $optionID) {
                        $option = Option::find($optionID);
                        if ($option) {
                            $categoryOpthionData = [
                                'sub_category_id' => $subCategory->id,
                                'option_id' => $optionID,
                                'created_by' => \Auth::id(),
                            ];
                            CategoryOption::create($categoryOpthionData);
                        }
                    }
                }
            }
        }

        return $this->jsonResponse("Success", $category->load("subCategories"));
    }

    public function getProducts($id)
    {
        $category = $this->categoriesRepo->getCategoryById($id);

        $products = Product::MainProduct()->active()->where("category_id", $category->id)->get(["id", "name", "image", "price", "sku"]);

        return $this->jsonResponse("Success", $products);
    }

    public function activate($id)
    {
        $category = $this->categoriesRepo->getCategoryById($id);

        $category->active = 1;
        $category->deactivation_notes = null;
        $category->save();

        return $this->jsonResponse("Success");
    }

    public function deactivate(Request $request, $id)
    {
        // validate request
        $validator = Validator::make($request->all(), ["deactivation_notes" => "required"]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }

        $category = $this->categoriesRepo->getCategoryById($id);

        $category->active = 0;
        $category->deactivation_notes = $request->deactivation_notes;
        $category->save();

        return $this->jsonResponse("Success");
    }

    public function destroy($id)
    {
//        $category = $this->categoriesRepo->getCategoryById($id);
//
//        $category->delete();
//
//        return $this->jsonResponse("Success");
    }

    public function export()
    {
        $items = Category::where('parent_id',null)->with('subCategories')->get();

        return Excel::create('Categories_' . date("Ymd"), function ($excel) use ($items) {
            $excel->sheet('report', function ($sheet) use ($items) {

                $sheet->row(1, [
                    "category",
                    "category_ar",
                    "category_image",
                    "subcategory",
                    "subcategory_ar",
                    "subcategory_slug",
                    "subcategory_image",
                    "subcategory_options",
                ]);

                foreach ($items as $key => $item) {
                    if (count($item['subCategories']) > 0){
                        $i = 0;
                        $subCategoryData =[];
                        foreach ($item['subCategories'] as $subCategory){
                            $options  = $subCategory->options()->pluck('name_en')->implode(',');
                            $array = [
                                $i == 0 ? $item->name : '',
                                $i == 0 ? $item->name_ar : '',
                                $i == 0 ? $item->image : '',
                                $subCategory->name,
                                $subCategory->name_ar,
                                $subCategory->slug,
                                $subCategory->image,
                                $options
                            ];
                            $subCategoryData[] = $array;
                            $i++;
                        }
                        $data = $subCategoryData;
                    }else{
                        $data = [
                            $item->name,
                            $item->name_ar,
                            $item->image,
                            '',
                            '',
                            '',
                        ];
                    }
                    $sheet->rows($data);
                }
            });
        })->download('xlsx');
    }
    public function import(Request $request)
    {
        $this->validate($request, ["file" => "required"]);
        try {
            Excel::load($request->file, function ($reader) {
                // Loop through all sheets
                $results = $reader->all();
                $parentCategory = null;
                foreach ($results as $key => $row) {
                    $categoryImage = isset($row['category_image']) ?  $row['category_image'] : null;
                    $subcategory_slug = isset($row['subcategory_slug']) && !empty($row['subcategory_slug']) ? $row['subcategory_slug'] : null;
                    $subCategoryOptions = isset($row['subcategory_options']) && !empty($row['subcategory_options']) ? explode(',', $row['subcategory_options']) : '';
                    if (filter_var($categoryImage, FILTER_VALIDATE_URL) === FALSE) {
                        $categoryImage =  str_replace(' ','-',$categoryImage);
                        $categoryImage = url("storage/uploads/".$categoryImage);
                    }
                    $subCategoryImage = isset($row['subcategory_image']) ?  $row['subcategory_image'] : '';
                    if (filter_var($subCategoryImage, FILTER_VALIDATE_URL) === FALSE) {
                        $subCategoryImage =  str_replace(' ','-',$subCategoryImage);
                        $subCategoryImage = url("storage/uploads/".$subCategoryImage);
                    }
                    if ($row['category'] != null || $row['category_ar'] != null) {
                        $item = Category::where("name", $row['category'])->first();
                        if ($item) {
                            $item->update(["name" => $row['category'], "name_ar" => $row['category_ar'], "image" => $categoryImage]);
                            if ($row['subcategory'] != null || $row['subcategory_ar'] != null) {
                                if ($subcategory_slug){
                                    $subCat = Category::where('slug',$subcategory_slug)->where('parent_id',$item->id)->first();
                                }else{
                                    $subCat = Category::where('name',$row['subcategory'])->where('parent_id',$item->id)->first();
                                }
                                if ($subCat){
                                    $subCat->update(["name" => $row['subcategory'], "name_ar" => $row['subcategory_ar'], "image" => $subCategoryImage, "parent_id" => $item->id,"active"=>'1','slug'=>$subcategory_slug]);
                                }else{
                                    $subCat =Category::create(["name" => $row['subcategory'], "name_ar" => $row['subcategory_ar'], "image" => $subCategoryImage, "parent_id" => $item->id,"active"=>'1','slug'=>$subcategory_slug]);
                                }
                                if (is_array($subCategoryOptions) && ! empty($subCategoryOptions)) {
                                    CategoryOption::where('sub_category_id',$subCat->id)->delete();
                                    foreach ($subCategoryOptions as $subCategoryOption) {
                                        if (!empty($subCategoryOption) && $subCategoryOption != null){
                                            $option = Option::where('name_en', $subCategoryOption)->first();
                                            if ($option) {
                                                $categoryOptionData = [
                                                    "option_id" => $option->id,
                                                    "sub_category_id" => $subCat->id
                                                ];
                                                CategoryOption::create($categoryOptionData);

                                            }

                                        }


                                    }


                                }

                            }
                        } else {
                            if ($row['subcategory'] != null || $row['subcategory_ar'] != null) {
                                $item = Category::create(["name" => $row['category'], "name_ar" => $row['category_ar'], "image" => $categoryImage,"active"=>'1']);
                                $subCat =Category::create(["name" => $row['subcategory'], "name_ar" => $row['subcategory_ar'], "image" => $subCategoryImage, "parent_id" => $item->id,"active"=>'1','slug'=>$subcategory_slug]);
                                if (is_array($subCategoryOptions) ) {
                                    foreach ($subCategoryOptions as $subCategoryOption) {
                                        $option = Option::where('name_en', $subCategoryOption)->first();
                                        if ($option) {
                                            $categoryOptionData = [
                                                "option_id" => $option->id,
                                                "sub_category_id" => $subCat->id
                                            ];
                                            CategoryOption::updateOrCreate($categoryOptionData);
                                        }
                                    }
                                }
                            } else {
                                $item = Category::create(["name" => $row['category'], "name_ar" => $row['category_ar'], "image" => $categoryImage,"active"=>'1']);
                            }
                        }
                        $parentCategory = $item;
                    } else {
                        if ($parentCategory) {
                            if ($subcategory_slug){
                                $subCat = Category::where('slug',$subcategory_slug)->where('parent_id',$parentCategory->id)->first();
                            }else{
                                $subCat = Category::where("name", $row['subcategory'])->where('parent_id',$parentCategory->id)->first();
                            }
                            if ($subCat) {
                                $subCat->update(["name" => $row['subcategory'], "name_ar" => $row['subcategory_ar'], "image" => $subCategoryImage,"slug"=>$subcategory_slug]);
                            } else {
                                $subCat = Category::create(["name" => $row['subcategory'], "name_ar" => $row['subcategory_ar'], "image" => $subCategoryImage, "parent_id" => $parentCategory->id,"active"=>'1',"slug"=>$subcategory_slug]);
                            }
                            if (is_array($subCategoryOptions)) {
                                foreach ($subCategoryOptions as $subCategoryOption) {
                                    $option = Option::where('name_en', $subCategoryOption)->first();
                                    if ($option) {
                                        $categoryOptionData = [
                                            "option_id" => $option->id,
                                            "sub_category_id" => $subCat->id
                                        ];
                                        CategoryOption::updateOrCreate($categoryOptionData);
                                    }
                                }
                            }
                        }
                    }
                }
            });
        } catch (\Exception $e) {
            Log::error("HandleFileError: " . $e->getMessage());
        }
        return $this->jsonResponse("Success", null);
        // return response
    }
}
