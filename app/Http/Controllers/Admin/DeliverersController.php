<?php

namespace App\Http\Controllers\Admin;

use App\Models\ACL\Role;
use App\Models\Repositories\DelivererRepository;
use App\Models\Transformers\DelivererFullTransformer;
use App\Models\Transformers\OrderDetailsTransformer;
use App\Models\Users\User;
use App\Models\Locations\District;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class DeliverersController extends Controller
{
    private $delivererRepo;
    private $deliverTrans;
    private $orderTrans;

    public function __construct(DelivererRepository $delivererRepo, DelivererFullTransformer $deliverTrans, OrderDetailsTransformer $orderTrans)
    {
        $this->delivererRepo = $delivererRepo;
        $this->deliverTrans = $deliverTrans;
        $this->orderTrans = $orderTrans;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $deliverers = $this->delivererRepo->getAllDeliverersPaginated($request->all());

        return $this->jsonResponse("Success", ["deliverers" => $this->deliverTrans->transformCollection($deliverers->items()), "total" => $deliverers->total()]);
    }

    public function allDeliverers()
    {
        $deliverers = $this->delivererRepo->getAllDeliverers();

        return $this->jsonResponse("Success", $this->deliverTrans->transformCollection($deliverers));
    }

    public function orders($id)
    {
        $deliverer = $this->delivererRepo->getDelivererById($id);
        
        $orders = $deliverer->deliveries;

        return $this->jsonResponse("Success", $this->orderTrans->transformCollection($orders));
    }

    public function exportOrders($id)
    {
        $deliverer = $this->delivererRepo->getDelivererById($id);
        
        $orders = $deliverer->deliveries;

        return Excel::create('orders_' . date("Ymd"), function ($excel) use ($orders){
                $excel->sheet('report', function ($sheet) use ($orders){
                    // $sheet->fromArray($players);
                    $sheet->row(1, ["id", "customer", "time", "paid amount", "total amount", "remaining", "state"]);
                    
                    foreach ($orders as $key => $order) {
                        $sheet->row($key + 2, [
                            $order->id,
                            $order->customer->name,
                            $order->created_at,
                            $order->invoice->cost_amount,
                            $order->invoice->total_amount,
                            $order->invoice->remaining,
                            $order->state->name,
                        ]);
                    }
                });
            })->download('xlsx');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate request
        $validator = Validator::make($request->all(), [
            "name" => "required",
            // "password" => "required",
            // "phone" => "required",
            // "birthdate" => "required|before:" . date("Y-m-d"),
            "address" => "required",
            // "unique_id" => "required|unique:deliverer_profiles,unique_id",
            // "area_id" => "required|exists:areas,id"
        ], ["phone.unique" => "This phone number is already registered", "email.unique" => "This email is already registered", "unique_id.unique" => "This unique id is already registered"]);
        
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }

        if ($request->email) {
            $user = User::whereHas('delivererProfile')->where('email', $request->email)->first();
            if ($user) {
                return $this->errorResponse("This email is already registered", "Invalid data", [], 422);
            }
        }

        if ($request->phone) {
            $user = User::whereHas('delivererProfile')->where('phone', $request->phone)->first();
            if ($user) {
                return $this->errorResponse("This phone is already registered", "Invalid data", [], 422);
            }
        }

        DB::beginTransaction();
        try {
            $request->merge(["birthdate" => date("Y-m-d", strtotime($request->birthdate)), "type" => 3]);

            $user = User::create($request->only(["name", "email", "phone", "birthdate", "type"]));
            $user->password = bcrypt($request->password);
            $user->active = 1;
            $user->save();

            $user->addresses()->create($request->only(["address"]));

            // store deliverer profile
            $deliverer_profile = $user->delivererProfile()->create($request->only(["area_id", "unique_id", "image", "city_id"]));
            $deliverer_profile->status = 3;
            $deliverer_profile->save();

            if ($request->districts_id) {
                $deliverer_profile->districts()->attach($request->districts_id);
            }

            DB::commit();
            return $this->jsonResponse("Success", $this->deliverTrans->transform($user));
        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorResponse($e->getMessage(), "Invalid data", [], 422);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $deliverer = $this->delivererRepo->getDelivererById($id);

        return $this->jsonResponse("Success", $this->deliverTrans->transform($deliverer));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        // validate request
        $validator = Validator::make($request->all(), [
            "name" => "required",
            // "phone" => ["required", Rule::unique("users")->ignore($user->id)],
            // "email" => [Rule::unique("users")->ignore($user->id)],
            // "birthdate" => "required|before:" . date("Y-m-d"),
            "address" => "required",
            // "unique_id" => ["required", Rule::unique("deliverer_profiles")->ignore($user->delivererProfile->id)],
            // "area_id" => "required|exists:areas,id"
        ], ["phone.unique" => "This phone number is already registered", "email.unique" => "This email is already registered", "unique_id.unique" => "This unique id is already registered"]);
        
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }

        DB::beginTransaction();
        try {
            // store user
            $request->merge(["birthdate" => date("Y-m-d", strtotime($request->birthdate))]);
            $user->update($request->only(["name", "email", "phone", "birthdate"]));
            if ($request->password) {
                $user->password = bcrypt($request->password);
                $user->save();
            }

            $user->addresses->first()->update($request->only(["address"]));

            // store deliverer profile
            $deliverer_profile = $user->delivererProfile;
            $deliverer_profile->update($request->only(["area_id", "unique_id", "image", "city_id"]));

            if ($request->districts_id) {
                $deliverer_profile->districts()->sync($request->districts_id);
            }

            DB::commit();
            return $this->jsonResponse("Success", $this->deliverTrans->transform($user));
        }catch(\Exception $e) {
            DB::rollback();
            return $this->errorResponse($e->getMessage(), "Invalid data", [], 422);
        }
    }

    public function activate($id)
    {
        $deliverer = $this->delivererRepo->getDelivererById($id);

        $deliverer->active = 1;
        $deliverer->deactivation_notes = null;
        $deliverer->save();

        return $this->jsonResponse("Success");
    }

    public function deactivate(Request $request, $id)
    {
        // validate request
        $validator = Validator::make($request->all(), ["deactivation_notes" => "required"]);
        
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }

        $deliverer = $this->delivererRepo->getDelivererById($id);

        $deliverer->active = 0;
        $deliverer->deactivation_notes = $request->deactivation_notes;
        $deliverer->save();

        return $this->jsonResponse("Success");
    }

    public function export()
    {
        $deliverers = $this->delivererRepo->getAllDeliverers();

        return Excel::create('deliverers_' . date("Ymd"), function ($excel) use ($deliverers){
                $excel->sheet('report', function ($sheet) use ($deliverers){
                    // $sheet->fromArray($players);
                    $sheet->row(1, ["id", "unique_id", "name", "address", "phone", "total_orders", "area", "active"]);
                    
                    foreach ($deliverers as $key => $user) {
                        // dd(array_values($player));
                        $sheet->row($key + 2, [
                            $user->id,
                            $user->delivererProfile->unique_id,
                            $user->name,
                            $user->addresses->first() ? $user->addresses->first()->address : "",
                            $user->phone,
                            $user->orders->count(),
                            $user->delivererProfile->area->name,
                            (int)$user->active
                        ]);
                    }
                });
            })->download('xlsx');
    }

    public function import(Request $request)
    {
        try {
            Excel::load($request->file, function ($reader) {

                $results = $reader->all();

                foreach ($results as $key => $record) {
                    $district = District::where("name", "LIKE", "%{$record->district}%")->first();
                    if (!$district) {
                        continue;
                    }

                    $user = User::where("name", $record->branch)->where("type", 3)->first();

                    if (!$user) {
                        $user = User::create(["name" => $record->branch, "type" => 3]);
                        $user->active = 1;
                        $user->save();

                        $user->addresses()->create(["address" => $district->name]);

                        // store deliverer profile
                        $deliverer_profile = $user->delivererProfile()->create(["city_id" => $district->area->city_id, "area_id" => $district->area_id]);
                        $deliverer_profile->status = 3;
                        $deliverer_profile->save();

                        $deliverer_profile->districts()->attach($district->id);
                    } else {
                        $user->delivererProfile->districts()->attach($district->id);
                    }
                }
            });
        } catch (\Exception $e) {
            Log::error("Import File Error: " . $e->getMessage());
            return $this->errorResponse($e->getMessage(), "Invalid data", [], 422);
        }

        // return response
        return $this->jsonResponse("Success");
    }
}
