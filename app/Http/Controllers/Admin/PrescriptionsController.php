<?php

namespace App\Http\Controllers\Admin;

use App\Models\Medical\Prescription;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
class PrescriptionsController extends Controller
{
    
    public function index()
    {
    	return $this->jsonResponse("Success", Prescription::with("user.addresses")->get());
    }

    public function export()
    {
        $prescriptions = Prescription::with("user.addresses")->get();

        return Excel::create('products_' . date("Ymd"), function ($excel) use ($prescriptions){
                $excel->sheet('report', function ($sheet) use ($prescriptions){
                    // $sheet->fromArray($players);
                    $sheet->row(1, ["id", "name", "phone", "address", "time"]);
                    
                    foreach ($prescriptions as $key => $prescription) {
                        // dd(array_values($player));
                        $addresses = $user->addresses;
                        $address = "";
                        if($addresses->count()) {
                        	$address = $addresses->first()->address;
                        }
                        $sheet->row($key + 2, [
                            $prescription->id,
                            $prescription->user->name,
                            $prescription->user->phone,
                            $addresses,
                            $prescription->created_at,
                        ]);
                    }
                });
            })->download('xlsx');
    }
}
