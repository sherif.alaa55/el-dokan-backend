<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Payment\Promotion;

class PromotionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promotions = Promotion::with("brand", "list")->orderBy("created_at", "DESC")->get();

        return $this->jsonResponse("Success", $promotions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required",
            "name_ar" => "required",
            "qty" => "required",
            // "brand_id" => "required|unique:promotions,brand_id",
            "expiration_date" => "required|after:yesterday",
            "discount" => "required|min:0"
        ]);

        $promotion = Promotion::create($request->only(["name", "name_ar", "brand_id", "expiration_date", "discount", "qty", "discount_qty", "list_id", "target_list_id", "qty2"]));
        $promotion->refresh();

        return $this->jsonResponse("Success", $promotion->load("brand", "list"));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $promotion = Promotion::with("brand")->findOrFail($id);

        return $this->jsonResponse("Success", $promotion);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $promotion = Promotion::with("brand")->findOrFail($id);

        $this->validate($request, [
            "name" => "required",
            "name_ar" => "required",
            "qty" => "required",
            // "brand_id" => "required|unique:promotions,brand_id,{$id}",
            "expiration_date" => "required|after:yesterday",
            "discount" => "required|min:0"
        ]);

        $promotion->update($request->only(["name", "name_ar", "brand_id", "expiration_date", "discount", "qty", "discount_qty", "list_id", "target_list_id", "qty2"]));

        return $this->jsonResponse("Success", $promotion->load("list"));
    }

    public function activate($id)
    {
        $promotion = Promotion::findOrFail($id);

        $promotion->active = 1;
        $promotion->deactivation_notes = null;
        $promotion->save();

        return $this->jsonResponse("Success");
    }

    public function deactivate(Request $request, $id)
    {
        // validate request
        $this->validate($request, ["deactivation_notes" => "required"]);

        $promotion = Promotion::findOrFail($id);

        $promotion->active = 0;
        $promotion->deactivation_notes = $request->deactivation_notes;
        $promotion->save();

        return $this->jsonResponse("Success");
    }
}
