<?php


namespace App\Http\Controllers\Admin;

use App\Jobs\StockNotifications;
use Carbon\Carbon;
use App\Models\Users\User;
use App\Models\Products\Tag;
use Illuminate\Http\Request;
use App\Models\Products\Brand;
use App\Models\Products\Lists;
use App\Models\Products\Option;
use App\Utils\ExcelValueBinder;
use Illuminate\Validation\Rule;
use App\Models\Products\Product;
use App\Models\Orders\OrderState;
use App\Models\Products\Category;
use Illuminate\Support\Facades\DB;
use App\Models\Orders\OrderProduct;
use App\Models\Products\ProductTag;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Models\Products\OptionValue;
use App\Models\Services\PushService;
use Maatwebsite\Excel\Facades\Excel;
use Rap2hpoutre\FastExcel\FastExcel;
use App\Models\Products\ProductImage;
use App\Models\Products\ProductPrice;
use Illuminate\Support\Facades\Cache;
use App\Models\Products\CategoryOption;
use App\Models\Products\ProductVariant;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use App\Models\Services\ProductsService;
use phpDocumentor\Reflection\Types\This;
use Illuminate\Support\Facades\Validator;
use App\Models\Products\StockNotification;
use App\Models\Products\ProductOptionValues;
use App\Models\Products\ProductVariantImage;
use App\Models\Products\ProductVariantValue;
use App\Models\Transformers\ProductFullTransformer;
use App\Models\Transformers\ProductVariantFullTransformer;

class ProductsController extends Controller
{
    private $productTrans;
    private $pushService;
    private $lists;
    private $attributesCount;
    private $optionsCount;
    private $productVariantTrans;

    public function __construct(ProductFullTransformer $productTrans, PushService $pushService, Lists $lists, ProductVariantFullTransformer $productVariantTrans, ProductsService $productsService)
    {
        $this->productsService = $productsService;
        $this->productTrans = $productTrans;
        $this->pushService = $pushService;
        $this->lists = $lists;
        $this->productVariantTrans = $productVariantTrans;
    }

    public function index(Request $request)
    {
        $products = Product::query()->with('images', 'ProductVariants', 'ProductVariants.images', 'ProductVariants.productOptions');

        if ($request->q) {
            $products->where(function ($q) use ($request) {
                $q->orWhere("name", "LIKE", "%{$request->q}%");
                $q->orWhere("name_ar", "LIKE", "%{$request->q}%");
                $q->orWhere("description", "LIKE", "%{$request->q}%");
                $q->orWhere("description_ar", "LIKE", "%{$request->q}%");
                $q->orWhere("id", "LIKE", "%{$request->q}%");
                $q->orWhere("sku", "LIKE", "%{$request->q}%");
            });
            // $products = $products->where("name", "LIKE", "%{$request->q}%")->orWhere("description", "LIKE", "%{$request->q}%")->orWhere("id", $request->q)->orWhere("sku", $request->q);
        }
        $products->when($request->sub_category_id, function ($q) use ($request) {
            $q->where(function ($query) use ($request) {
                $query->where('category_id', $request->sub_category_id);
            });
        });
        $products->when($request->variant, function ($q) {
            $q->where('parent_id', '!=', null);
        });
        if ($request->parent_id) {
            $products->where("parent_id", $request->parent_id);
            $products = $products->orderBy("created_at", "DESC")->paginate(20);
            return $this->jsonResponse("Success", ["products" => $this->productVariantTrans->transformCollection($products->items()), "total" => $products->total()]);
        } else {
            if (!$request->variant) {
                $products->MainProduct();
            }
            $products = $products->orderBy("created_at", "DESC")->paginate(20);
            return $this->jsonResponse("Success", ["products" => $this->productTrans->transformCollection($products->items()), "total" => $products->total()]);
        }
    }

    public function searchVariants(Request $request)
    {
        $products = Product::query()->with('images', 'ProductVariants', 'ProductVariants.images', 'ProductVariants.productOptions')->where("stock", ">", 0)->whereNotNull("parent_id");

        if ($request->q) {
            $products->where(function ($q) use ($request) {
                $q->search($request->q)->orWhere("id", $request->q)->orWhere("sku", "LIKE", "%{$request->q}%");
            });
            // $products = $products->where("name", "LIKE", "%{$request->q}%")->orWhere("description", "LIKE", "%{$request->q}%")->orWhere("id", $request->q)->orWhere("sku", $request->q);
        }
        $products = $products->paginate(20);

        return $this->jsonResponse("Success", ["products" => $this->productTrans->transformCollection($products->items()), "total" => $products->total()]);
    }

    public function search(Request $request)
    {
        $products = Product::query()->with('images', 'ProductVariants', 'ProductVariants.images', 'ProductVariants.productOptions');
        $products->when($request->q, function ($q) use ($request) {
            $q->where(function ($q) use ($request) {
                $q->orWhere("name", "LIKE", "%{$request->q}%");
                $q->orWhere("name_ar", "LIKE", "%{$request->q}%");
                $q->orWhere("description", "LIKE", "%{$request->q}%");
                $q->orWhere("description_ar", "LIKE", "%{$request->q}%");
                $q->orWhere("id", "LIKE", "%{$request->q}%");
                $q->orWhere("sku", "LIKE", "%{$request->q}%");
            });
        });

        $products->when($request->variant, function ($q) {
            $q->where('parent_id', '!=', null);
        });
        $products->when($request->sub_category_id, function ($q) use ($request) {
            $q->where(function ($query) use ($request) {
                $query->where('category_id', $request->sub_category_id);
            });
        });

        if ($request->parent_id) {
            $products->where("parent_id", $request->parent_id);
            $products = $products->orderBy("created_at", "DESC")->paginate(20);
            return $this->jsonResponse("Success", ["products" => $this->productVariantTrans->transformCollection($products->items()), "total" => $products->total()]);
        } else {
            if (!$request->variant) {
                $products->MainProduct();
            }
            $products = $products->orderBy("created_at", "DESC")->paginate(20);
            return $this->jsonResponse("Success", ["products" => $this->productTrans->transformCollection($products->items()), "total" => $products->total()]);
        }
    }

    public function store(Request $request)
    {
        //validate request
        $validator = Validator::make($request->all(), Product::$mainProductValidation + ["sku" => "required|unique:products,sku"]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }

        $category = Category::find($request->category_id);
        if ($category->parent_id == null) {
            $message = 'The Category must be subcategory not Main Category';
            return $this->errorResponse($message, "Invalid data", $message, 422);
        }
        if ($request->brand_id && !is_numeric($request->brand_id)) {
            $brand = Brand::firstOrCreate(["name" => $request->brand_id]);
            $request->merge(["brand_id" => $brand->id]);
        }

        $data = $validator->validated();
        $data["bundle_checkout"] = (bool)$data["bundle_checkout"];
        $data['last_editor'] = auth()->User()->id;
        $data['creator_id'] = auth()->User()->id;
        $product = Product::create($data);
//        $product = Product::create($request->only([
//            "sku",
//            "category_id",
//            "optional_sub_category_id",
//            "brand_id",
//            "stock_alert",
//            "order",
//            "active",
//            "preorder",
//            "max_per_order",
//            "min_days",
//            "name",
//            "name_ar",
//            "type",
//        ]));

        $product->active = 1;
        $product->save();
        $product->createPrice();

        // store related_skus
        if ($request->related_ids) {
            foreach ($request->related_ids as $related_id) {
                $relatedProduct = Product::find($related_id);
                if ($relatedProduct)
                    $product->relatedSkus()->create(["sku" => $relatedProduct->sku]);
            }
        }

        // store images
        if ($request->images) {
            foreach ($request->images as $image) {
                if (isset($image["url"]) && !empty($image["url"])) {
                    $product->images()->create(["url" => $image["url"]]);
                }
            }
        }

        // Store Product Option Values

        if ($request->tags && is_array($request->tags)) {
            foreach ($request->tags as $tag) {
                $tagQuery = Tag::find($tag);
                if ($tagQuery) {
                    $data = [
                        'tag_id' => $tagQuery->id,
                        'product_id' => $product->id,
                    ];
                    ProductTag::updateOrCreate($data);
                }
            }
        }


        if ($request->option_values && is_array($request->option_values)) {
            foreach ($request->option_values as $option_value) {
                $option = Option::find($option_value['option_id']);
                if ($option) {
                    $categoryOptions = CategoryOption::where('sub_category_id', $category->id)->where('option_id', $option->id)->first();
                    $value = OptionValue::where('id', $option_value['option_value_id'])->where('option_id', $option->id)->first();
                    if ($categoryOptions) {
                        $data = [
                            'product_id' => $product->id,
                            'option_id' => $option->id,
                            'type' => '0',
                            'created_by' => \Auth::id(),
                        ];
                        if ($option->type == 5 && isset($option_value['input_en']) && isset($option_value['input_ar'])) {
                            $data['input_en'] = $option_value['input_en'];
                            $data['input_ar'] = $option_value['input_ar'];
                        } elseif ($option->type == 4 && isset($option_value['image'])) {
                            $data['image'] = $option_value['image'];
                        } elseif ($value) {
                            $data['value_id'] = $value->id;
                        }
                        $productOptionValue = ProductOptionValues::updateOrCreate($data);
                    }
                }
            }
        }

        if ($request->product_variant_options && is_array($request->product_variant_options)) {
            foreach ($request->product_variant_options as $product_variant_option) {
                $option = Option::find($product_variant_option);
                if ($option) {
                    $data = [
                        'product_id' => $product->id,
                        'option_id' => $option->id,
                        'type' => '1',
                        'created_by' => \Auth::id()
                    ];
                    ProductOptionValues::updateOrCreate($data);
                }
            }
        }
        return $this->jsonResponse("Success", $this->productTrans->transform($product));
    }

    public function show($id)
    {
        $product = Product::findOrFail($id);

        return $this->jsonResponse("Success", $this->productTrans->transform($product));
//        return $this->jsonResponse("Success", $product);
    }

    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        // validate request
        $validator = Validator::make($request->all(), Product::$mainProductValidation + ["sku" => ["required", Rule::unique("products")->ignore($id)]]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }
        $category = Category::find($request->category_id);
        if ($category->parent_id == null) {
            $message = 'The Category must be subcategory not Main Category';
            return $this->errorResponse($message, "Invalid data", $message, 422);
        }
        if ($request->brand_id && !is_numeric($request->brand_id)) {
            $brand = Brand::firstOrCreate(["name" => $request->brand_id]);
            $request->merge(["brand_id" => $brand->id]);
        }

        $price = $product->price;
        $discount_price = $product->discount_price;

        $parentData = [
            'brand_id' => $request->brand_id,
            'category_id' => $request->category_id
        ];

        $data = $validator->validated();
        $data['last_editor'] = auth()->User()->id;
        $product->update($data);
//        $product->update($request->only([
//            "sku",
//            "category_id",
//            "optional_sub_category_id",
//            "brand_id",
//            "order",
//            "stock_alert",
//            "active",
//            "preorder",
//            "max_per_order",
//            "min_days",
//            "name",
//            "name_ar",
//            "type",
//        ]));

        if ($product->stock > 0) {
            $users = $product->stock_notifiers;
            $product->stock_notifiers()->sync([]);

            $this->pushService->notifyUsers($users, "{$product->name} is now available", "Product in Stock");
        }

        if ($price !== $request->price || $discount_price !== $request->discount_price) {
            $product->createPrice();
        }

        if ($request->deleted_images) {
            ProductImage::whereIn("id", $request->deleted_images)->delete();
        }
//        $optionIDS = array_column($request->option_values, 'option_id');
//        $optionValueIDS = array_column($request->option_values, 'option_value_id');

        // store images
        if ($request->images) {
            foreach ($request->images as $image) {
                if (isset($image["url"]) && !empty($image["url"])) {
                    if (isset($image["id"])) {
                        $product->images()->find($image["id"])->update(["url" => $image["url"]]);
                    } else {
                        $product->images()->create(["url" => $image["url"]]);
                    }
                }
            }
        }

        // store related_skus
        $product->relatedSkus()->delete();
        if ($request->related_ids) {
            foreach ($request->related_ids as $related_id) {
                $relatedProduct = Product::find($related_id);
                if ($relatedProduct)
                    $product->relatedSkus()->create(["sku" => $relatedProduct->sku]);
            }
        }


//     $removedProductOptionsValues =   ProductOptionValues::where('product_id',$id)->whereNotIn('option_id',$optionIDS)->whereNotIn('value_id',$optionValueIDS)->delete();
//        $removedProductOptionsValues = ProductOptionValues::where('product_id', $id)->delete();
        if ($request->option_values && is_array($request->option_values)) {
            ProductOptionValues::where('product_id', $product->id)->where('type', '0')->delete();
            foreach ($request->option_values as $option_value) {
                $option = Option::find($option_value['option_id']);
                if ($option) {
                    $categoryOptions = CategoryOption::where('sub_category_id', $category->id)->where('option_id', $option->id)->first();
                    $value = OptionValue::where('id', $option_value['option_value_id'])->where('option_id', $option->id)->first();
                    if ($categoryOptions) {
                        $data = [
                            'product_id' => $product->id,
                            'option_id' => $option->id,
                            'type' => '0',
                            'created_by' => \Auth::id(),
                        ];
                        if ($option->type == 5 && isset($option_value['input_en']) && isset($option_value['input_ar'])) {
                            $data['input_en'] = $option_value['input_en'];
                            $data['input_ar'] = $option_value['input_ar'];
                        } elseif ($option->type == 4 && isset($option_value['image'])) {
                            $data['image'] = $option_value['image'];
                        } elseif ($value) {
                            $data['value_id'] = $value->id;
                        }
                        $productOptionValue = ProductOptionValues::updateOrCreate($data);
                    }
                }
            }
        }


        if ($request->product_variant_options && is_array($request->product_variant_options)) {
            $productOldOptionValues = $product->productVariantOptions->pluck('id')->toArray();
            foreach ($request->product_variant_options as $product_variant_option) {
                $option = Option::find($product_variant_option);
                if ($option) {
                    if (($key = array_search($option->id, $productOldOptionValues)) !== false) {
                        unset($productOldOptionValues[$key]);
                    }
                    $productOptionValue = ProductOptionValues::where('type', '1')->where('product_id', $product->id)->where('option_id', $option->id)->first();
                    if (!$productOptionValue) {
                        $data = [
                            'product_id' => $product->id,
                            'option_id' => $option->id,
                            'type' => '1',
                            'created_by' => \Auth::id()
                        ];
                        ProductOptionValues::create($data);
                    }
                }
            }

            if (count($productOldOptionValues) > 0) {
                ProductOptionValues::where('type', '1')->where('product_id', $product->id)->whereIn('option_id', $productOldOptionValues)->delete();
            }
            foreach ($product->productVariants as $variant) {
                ProductOptionValues::where('type', '1')->where('product_id', $variant->id)->whereIn('option_id', $productOldOptionValues)->delete();
            }
        } else {
            ProductOptionValues::where('type', '1')->where('product_id', $product->id)->delete();
            $variant_ids = $product->productVariants->pluck('id')->toArray();
            ProductOptionValues::where('type', '1')->whereIn('product_id', $variant_ids)->delete();
        }

        $variants = $product->productVariants;
        foreach ($variants as $variant ) {
            $variant->update($parentData);
        }

        Cache::forget("product_images_{$product->id}");
        Cache::forget("product_" . $product->id . "_1_options");
        Cache::forget("product_" . $product->id . "_2_options");

        return $this->jsonResponse("Success", $this->productTrans->transform($product));
    }

    public function resetVariantList()
    {
        $variantOptions = [];
        $mainProductOptions = [];
        $mainProducts = Product::MainProduct()->get();
        $i = 0;
        foreach ($mainProducts as $mainProduct) {
            $variantOptions = [];
            $productVariants = $mainProduct->productVariants()->pluck('id');
            if (count($productVariants) > 0) {
                foreach ($productVariants as $productVariant) {
                    $optionValues = ProductOptionValues::where('product_id', $productVariant)->where('type', '1')->get();
                    if (count($optionValues) > 0) {
                        foreach ($optionValues as $optionValue) {
                            $variantOptions[] = [
                                'option_id' => $optionValue->option_id,
                                'value_id' => $optionValue->value_id,
                                'image' => $optionValue->image,
                                'input_en' => $optionValue->input_en,
                                'input_ar' => $optionValue->input_ar,
                                'type' => '1',
                            ];
                        }
                    }
                }
            }
            // $mainProductVariants = ProductOptionValues::where('product_id', $mainProduct->id)->where('type', '1')->get();
            // if (count($mainProductVariants) > 0) {
            //     foreach ($mainProductVariants as $mainProductVariant) {
            //         $mainProductOptions[] = [
            //             'product_id' => $mainProduct->id,
            //             'option_id' => $mainProductVariant->option_id,
            //             'value_id' => $mainProductVariant->value_id,
            //             'image' => $mainProductVariant->image,
            //             'input_en' => $mainProductVariant->input_en,
            //             'input_ar' => $mainProductVariant->input_ar,
            //             'type' => '1',
            //         ];
            //     }
            // }
            ProductOptionValues::where('product_id', $mainProduct->id)->where('type', '1')->delete();
            foreach ($variantOptions as $variantOption) {
                $variantOption['product_id'] = $mainProduct->id;
                ProductOptionValues::updateOrCreate($variantOption);
            }
            // foreach ($mainProductOptions as $mainProductOption) {
            //     $productOption = ProductOptionValues::where('option_id', $mainProductOption['option_id'])->where('value_id', $mainProductOption['value_id'])->where('product_id', $mainProduct->id)->where('type', '1')->first();
            //     if ($productOption) {
            //         $productOption->update($mainProductOption);
            //     }
            // }
            $i++;
        }
        $this->pushService->notifyAdmins("Products Reseated", "All Products Reseated Successfully", '', 10, '', \Auth::id());
        Log::info("PRODUCT RESET SUCCESSFULLY");
        Artisan::call('cache:clear');
        return $this->jsonResponse("Success", $i);
    }

    public function resetProductVariantList($id)
    {
        $variantOptions = [];
        $mainProductOptions = [];
        $mainProducts = Product::MainProduct()->where("id", $id)->get();
        $i = 0;
        foreach ($mainProducts as $mainProduct) {
            $productVariants = $mainProduct->productVariants()->pluck('id');
            if (count($productVariants) > 0) {
                foreach ($productVariants as $productVariant) {
                    $optionValues = ProductOptionValues::where('product_id', $productVariant)->where('type', '1')->get();
                    if (count($optionValues) > 0) {
                        foreach ($optionValues as $optionValue) {
                            $variantOptions[] = [
                                'option_id' => $optionValue->option_id,
                                'value_id' => $optionValue->value_id,
                                'image' => $optionValue->image,
                                'input_en' => $optionValue->input_en,
                                'input_ar' => $optionValue->input_ar,
                                'type' => '1',
                            ];
                        }
                    }
                }
            }
            // $mainProductVariants = ProductOptionValues::where('product_id', $mainProduct->id)->where('type', '1')->get();
            // if (count($mainProductVariants) > 0) {
            //     foreach ($mainProductVariants as $mainProductVariant) {
            //         $mainProductOptions[] = [
            //             'product_id' => $mainProduct->id,
            //             'option_id' => $mainProductVariant->option_id,
            //             'value_id' => $mainProductVariant->value_id,
            //             'image' => $mainProductVariant->image,
            //             'input_en' => $mainProductVariant->input_en,
            //             'input_ar' => $mainProductVariant->input_ar,
            //             'type' => '1',
            //         ];
            //     }
            // }
            ProductOptionValues::where('product_id', $mainProduct->id)->where('type', '1')->delete();

            foreach ($variantOptions as $variantOption) {
                $variantOption['product_id'] = $mainProduct->id;
                ProductOptionValues::create($variantOption);
            }
            // foreach ($mainProductOptions as $mainProductOption) {
            //     $productOption = ProductOptionValues::where('option_id', $mainProductOption['option_id'])->where('value_id', $mainProductOption['value_id'])->where('product_id', $mainProduct->id)->where('type', '1')->first();
            //     if ($productOption) {
            //         $productOption->update($mainProductOption);
            //     }
            // }
            $i++;
        }
        $this->pushService->notifyAdmins("Products Reseated", "All Products Reseated Successfully", '', 10, '', \Auth::id());
        return $this->jsonResponse("Success", $i);
    }

    public function getStats(Request $request, $product_id)
    {
        // validate request
        $validator = Validator::make($request->all(), ["begin" => "required", "end" => "required"]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }


        $begin = date("Y-m-d", strtotime($request->begin));
        $end = date("Y-m-d", strtotime($request->end));

        $history = OrderProduct::query()
            ->select(DB::raw('count(amount) as `productAmount`'), DB::raw("DATE_FORMAT(`created_at`,'%b %Y') as month"))
            ->where("created_at", ">=", $begin)
            ->where("created_at", "<=", $end)
            ->where("product_id", $product_id)
            ->groupby('product_id', 'month')
            ->get()->toArray();

        return $this->jsonResponse("Success", $history);
    }

    public function destroy($id)
    {
        //
    }

    public function stockAlert()
    {
        $stockNotification = StockNotification::with('user', 'product')->paginate(20);
        return $this->jsonResponse("Success", ["items" => $stockNotification->items(), "total" => $stockNotification->total()]);
    }

    public function activate($id)
    {
        $product = Product::findOrFail($id);

        $product->active = 1;
        $product->deactivation_notes = null;
        $product->save();

        return $this->jsonResponse("Success");
    }

    public function deactivate(Request $request, $id)
    {
        // validate request
        $validator = Validator::make($request->all(), ["deactivation_notes" => "required"]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }

        $product = Product::findOrFail($id);

        $product->active = 0;
        $product->deactivation_notes = $request->deactivation_notes;
        $product->save();

        return $this->jsonResponse("Success");
    }

    public function importStocks(Request $request)
    {
        $totalCount = 0;
        $totalUpdatedCount = 0;
        $totalMissedCount = 0;
        $missedDetails = [];
        $this->validate($request, ["file" => "required"]);
        try {
            $binder = new ExcelValueBinder;
            $rows = Excel::setValueBinder($binder)->load($request->file)->get()->toArray();
            Log::info("Count: " . count($rows));
            if (!empty($rows)) {
                foreach ($rows as $row) {
                    Log::info($row);
                    // $MainSku = isset($row['sku']) && !empty($row['sku']) ? $row['sku'] : '';
                    $productStock = isset($row['stock']) && !empty($row['stock']) && ($row['stock']) > 0 ? $row['stock'] : 0;
                    $productPrice = isset($row['price']) && !empty($row['price']) && ($row['price']) > 0 ? $row['price'] : 0;
                    $productActive = isset($row['active']) ? $row['active'] : null;
                    $productDiscountPrice = isset($row['discount_price']) && !empty($row['discount_price']) && ($row['discount_price']) > 0 ? $row['discount_price'] : null;
                    if (is_float($row['sku'])) {
                        $MainSku = (int)$row["sku"];
                    } else {
                        $MainSku = $row["sku"];
                    }

                    if (isset($row['sku']) && $row['sku'] != null) {
                        $product = Product::where('sku', $MainSku)->first();
                        $totalCount++;
                        if ($product) {
                            $data = [];
                            if (isset($row['stock']) && $row['stock'] !== null) {
                                $data['stock'] = $productStock;
                            }
                            if (isset($row['price']) && $row['price'] != null) {
                                $data['price'] = $productPrice;
                                if (isset($row['discount_price']) && $row['discount_price'] != null) {
                                    $data['discount_price'] = $productDiscountPrice;
                                } else {
                                    $data["discount_price"] = null;
                                }
                            }

                            if (isset($row['active'])) {
                                $data['active'] = (int)$productActive;
                            }


                            $totalUpdatedCount++;
                            $product->update($data);
                            if ($product->stock > 0) {
                                dispatch(new StockNotifications($product));
                            }
                        } else {
                            $totalMissedCount++;
                            $missedDetails[] = 'Product with sku ' . $MainSku . '( Not Found )';
                        }
                    }
                }
            }
            $data = [
                'total_count' => $totalCount,
                'total_missed_count' => $totalMissedCount,
                'total_updated_count' => $totalUpdatedCount,
                'missed_details' => $missedDetails,
            ];
            Log::info($data);
            $this->pushService->notifyAdmins("File Imported", "Your import has completed! You can check the result from", '', 10, json_encode($data));
            return $this->jsonResponse("Success", $data);
        } catch (\Exception $e) {
            // dd($e);
            Log::error("Import File Error: " . $e->getMessage());
            return $this->errorResponse($e->getMessage(), "Invalid data", [], 422);
        }
    }

    public function export()
    {
        $products = Product::with("category.parent", "brand")->get();

        return Excel::create('products_' . date("Ymd"), function ($excel) use ($products) {
            $excel->sheet('report', function ($sheet) use ($products) {
                // $sheet->fromArray($players);
                $sheet->row(1, ["id", "name", "name_ar", "description", "meta_title", "meta_description", "keywords", "preorder", "preorder_price", "description_ar", "category", "subcategory", "brand", "sku", "total_orders", "price", "discount_price", "image", "active", "weight"]);

                foreach ($products as $key => $product) {
                    // dd(array_values($player));
                    $brand_name = $product->brand ? $product->brand->name : "";
                    $sheet->row($key + 2, [
                        $product->id,
                        $product->name,
                        $product->name_ar,
                        $product->description,
                        $product->meta_title,
                        $product->meta_description,
                        $product->keywords,
                        $product->preorder,
                        $product->preorder_price,
                        $product->description_ar,
                        $product->category->parent->name,
                        $product->category->name,
                        $brand_name,
                        $product->sku,
                        $product->orders_count,
                        $product->price,
                        $product->discount_price,
                        $product->image,
                        (int)$product->active,
                        $product->weight,
                    ]);
                }
            });
        })->download('csv');
        $this->pushService->notifyAdmins("File Imported", "the File Has Been Imported Successfully", '', 10, null);
    }

    public function exportStocks()
    {
        $products = Product::MainProduct()->get();
        $fileName = 'products_' . date("Y-m-d-H-i-s");
        Excel::create($fileName, function ($excel) use ($products) {
            $excel->sheet('report', function ($sheet) use ($products) {
                $sheet->row(1, ["sku", "price", "discount_price", "stock", "active"]);
                foreach ($products as $item) {
                    if (count($item->productVariants) > 0) {
                        $variantsStocks = [];
                        foreach ($item->productVariants as $key => $product) {
                            $variantsStocks[] = [
                                $product->sku,
                                $product->price,
                                $product->discount_price,
                                $product->stock,
                                $product->active,
                            ];
                        }

                        $sheet->rows($variantsStocks);
                    }
                }
            });

        })->store('xlsx', storage_path("app/public/exports"));
        $filePath = url("storage/exports") . '/' . $fileName . '.xlsx';
        $this->pushService->notifyAdmins("File Exported ", "Your export has completed! You can download the file from", '', 11, $filePath, \Auth::id());

        return $this->jsonResponse("Success");
    }

    public function import(Request $request)
    {

        // $user_id = \Auth::id();
        try {
            Excel::load($request->file, function ($reader) {
                // Loop through all sheets

                $results = $reader->all();
                $missed_categories = [];
                $missed_count = 0;
                $missed_images = [];
                $missed_keys = [];
                foreach ($results as $key => $record) {

                    if (is_float($record->sku)) {
                        $sku = (int)$record->sku;
                    } else {
                        $sku = $record->sku;
                    }
                    $category_name = $record->subcategory;

                    if ($category = Category::whereNotNull("parent_id")->where("name", trim($category_name))->first()) {
                        $category_id = $category->id;
                    } else {
                        $missed_categories[] = $category_name;
                        $missed_keys[] = $key;
                        $missed_count++;
                        continue;
                    }

                    $brand_id = null;
                    if ($record->brand) {
                        $brand = Brand::where("name", $record->brand)->first();
                        if ($brand) {
                            $brand_id = $brand->id;
                        }
                    }

                    $images = explode(',', $record->image);
                    $main_image = null;
                    if (count($images)) {
                        $main_image = trim($images[0]);
                        $main_image = preg_replace("/\s/", "-", $main_image);
                        unset($images[0]);
                        if (filter_var($main_image, FILTER_VALIDATE_URL) === FALSE) {
                            $main_image = "storage/uploads/" . $main_image;
                        }
                        // if (!Storage::disk('public')->has('uploads/' . $main_image)) {
                        //     $missed_images[] = $main_image;
                        // }
                    }

                    // if product exists
                    $product_data = [
                        "name" => $record->name,
                        "name_ar" => $record->name_ar,
                        "price" => $record->price,
                        "weight" => $record->weight,
                        "discount_price" => $record->discount_price,
                        "description" => $record->description,
                        "meta_title" => $record->meta_title,
                        "meta_description" => $record->meta_description,
                        "keywords" => $record->keywords,
                        "preorder" => $record->preorder,
                        "preorder_price" => $record->preorder_price,
                        "description_ar" => $record->description_ar,
                        "sku" => $record->sku,
                        "brand_id" => $brand_id,
                        // "image" => preg_replace("/\s/", "-", $record->image),
                        "image" => $main_image,
                        "category_id" => $category_id,
                        "active" => $record->active
                    ];

                    if ($product = Product::where("sku", $sku)->first()) {
                        $product->update($product_data);
                    } else {
                        $product = Product::create($product_data + ["creator_id" => \Auth::id()]);
                    }

                    if (count($images)) {
                        $product->images()->delete();
                        foreach ($images as $image) {
                            $image = preg_replace("/\s/", "-", trim($image));
                            if (filter_var($main_image, FILTER_VALIDATE_URL) === FALSE) {
                                $image = "storage/uploads/" . $image;
                            }
                            $product->images()->create([
                                "url" => $image
                            ]);
                        }
                    }
                }
                Log::info($missed_images);
                Log::info($missed_categories);
                Log::info($missed_keys);
                Log::info("Missed Products: {$missed_count}");
            });
        } catch (\Exception $e) {
            // dd($e->getMessage());
            Log::error("HandleFileError: " . $e->getMessage());
            Log::error($e->getTrace());
        }

//        $this->lists->ReseatConditionProductLists();
        // return response

    }

    public function importBrands(Request $request)
    {
        try {
            Excel::load($request->file, function ($reader) {
                // Loop through all sheets

                $results = $reader->all();

                foreach ($results as $key => $record) {

                    if (is_float($record->sku)) {
                        $sku = (int)$record->sku;
                    } else {
                        $sku = $record->sku;
                    }

                    if (!$record->brand) {
                        continue;
                    }
                    $brand = Brand::firstOrCreate(["name" => $record->brand]);


                    if ($product = Product::where("sku", $sku)->first()) {
                        $product->update(["brand_id" => $brand->id]);
                    }
                }
            });
        } catch (\Exception $e) {

            Log::error("HandleFileError: " . $e->getMessage());
        }

        // return response
    }

    public function importPrices(Request $request)
    {
        // get file
        ini_set('max_execution_time', 900);
        ini_set('memory_limit', '1024M');
        Log::error("IMPORT START");

        try {
            Excel::load($request->file, function ($reader) {
                // Loop through all sheets

                $results = $reader->all();
                foreach ($results as $key => $record) {

                    if (is_float($record->sku)) {
                        $sku = (int)$record->sku;
                    } else {
                        $sku = $record->sku;
                    }

                    if ($product = Product::where("sku", $sku)->first()) {
                        $product->update(["stock" => $record->stock, "price" => $record->price, "discount_price" => $record->discount_price]);
                    }

                }
            });
        } catch (\Exception $e) {
            Log::error("Import File Error: " . $e->getMessage());
            return $this->errorResponse($e->getMessage(), "Invalid data", [], 422);
        }

        Log::info("IMPORT COMPLETE");
        $this->pushService->notifyAdmins("File Imported", "the File Has Been Imported Successfully", '', 10, null, \Auth::id());
        // return response
        return $this->jsonResponse("Success");
    }

    public function exportProductSales(Request $request)
    {
        $items = OrderProduct::with("order", "product")->whereHas("order", function ($q) {
            $q->where("state_id", OrderState::DELIVERED);
        });
        if ($request->date_from) {
            $items = $items->whereDate("created_at", ">=", date("Y-m-d", strtotime($request->date_from)));
        }
        if ($request->date_to) {
            $items = $items->whereDate("created_at", "<=", date("Y-m-d", strtotime($request->date_to)));
        }
        $items = $items->select("*", DB::raw("SUM(amount) as sale_count"))->groupby("product_id")->get();
        return Excel::create('products_' . date("Ymd"), function ($excel) use ($items) {
            $excel->sheet('report', function ($sheet) use ($items) {

                $sheet->row(1, ["name", "main_sku", "sku", "count"]);

                foreach ($items as $key => $item) {
                    $sheet->row($key + 2, [
                        $item->product->name,
                        $item->product->parent->sku,
                        $item->product->sku,
                        $item->sale_count,
                    ]);
                }
            });
        })->download('xlsx');
    }

    public function fullExport(Request $request)
    {
        // DB::setFetchMode(PDO::FETCH_ASSOC);
        $attributesCount = 0;
        $optionsCount = 0;


        $products = Product::query();
        $products->when(isset($request->sub_category_id) && !empty(isset($request->sub_category_id)), function ($q) use ($request) {
            $q->where('category_id', $request->sub_category_id);
        });
        $fileName = 'products_' . date("Y-m-d-H-i-s");
        $products = $products->MainProduct()->with('ProductOptionValues', 'productVariantOptions', 'brand', 'category', 'category.parent', 'category.group', 'tags', 'images', 'productVariants', 'productVariants.images')->get();

        $attributes = Option::all();

//        $productAttributesMaxCount = Product::withCount('ProductOptions')->orderBy('product_options_count', 'DESC')->first();
        $productVariantOptionsMaxCount = Product::withCount('productVariantOptions')->where('parent_id', '!=', null)->orderBy('product_variant_options_count', 'DESC')->first();

        $this->optionsCount = $productVariantOptionsMaxCount ? $productVariantOptionsMaxCount->product_variant_options_count : $optionsCount;
//        $this->attributesCount = $productAttributesMaxCount ? $productAttributesMaxCount->product_options_count : $attributesCount;
        $this->attributesCount = $attributes->count();

        $headingArray = [
            "main_id",
            "main_sku",
            "variant_sku",
            "related_skus",
            "name",
            "name_ar",
            "description",
            "description_ar",
            "long_description_en",
            "long_description_ar",
            "meta_title",
            "meta_title_ar",
            "meta_description",
            "meta_description_ar",
            "keywords",
            "preorder",
            "preorder_start_date",
            "preorder_end_date",
            // "preorder_price",
            "weight",
            "category",
            "group",
            "subcategory",
            "subcategory_slug",
            "category_optional",
            "subcategory_optional",
            "brand",
            "tags",
            "Subtract Stock",
            "stock",
            "barcode",
            "price",
            "discount_price",
            "discount_start_date",
            "discount_end_date",
            "main_image",
            "images",
            "order",
            "active",
            "type",
            "has_stock",
            "bundle_checkout",
            "bundle_skus"
        ];

        for ($x = 1; $x <= $this->attributesCount; $x++) {
            array_push($headingArray, "attribute_name_$x");
            array_push($headingArray, "attribute_value_$x");
            array_push($headingArray, "attribute_value_ar_$x");
        }

        for ($x = 1; $x <= $this->optionsCount; $x++) {
            array_push($headingArray, "option_name_$x");
            array_push($headingArray, "option_value_$x");
            array_push($headingArray, "option_image_$x");
        }
        Log::info("Export Count: " . $products->count());
        Excel::create($fileName, function ($excel) use ($products, $headingArray, $attributes) {
            $excel->sheet('products_' . date("Ymd"), function ($sheet) use ($products, $headingArray, $attributes) {
                $sheet->row(1, $headingArray);
                foreach ($products as $key => $item) {
                    Log::info("Exporting Row: {$key}");
                    $category = $item->category ? $item->category->parent : "";
                    $subCategory = $item->category ? $item->category : "";
                    $brandName = $item->brand ? $item->brand->name : "";
                    $group = isset($item->category->group[0]) ? $item->category->group[0]->name_en : "";
                    $tags = $item->tags->pluck('name_en')->implode(',');
                    $images = $item->images->pluck('url')->implode(',');
                    $variants = [];
                    $relatedSkus = implode(',', $item->relatedSkus->pluck('sku')->toArray());
                    $bundleSkus = implode(',', $item->bundleProduct->pluck('sku')->toArray());
                    $i = 0;
                    $attributesData = [];
//                    if (!empty($attributes)) {
//                        $s = 0;
//                        $attributesData = [];

                    foreach ($attributes as $attribute) {
//                            $productAttribute = $attribute->whereHas('attributeProducts', function ($q) use ($item) {
//                                $q->where('products.id',$item->id);
//                            })->first();
                        $productAttributePivot = ProductOptionValues::where('option_id', $attribute->id)->where('product_id', $item->id)->where('type', '0')->first();
                        array_push($attributesData, $attribute->name_en);
                        if ($productAttributePivot) {
                            if ($attribute->type == 5) {
                                array_push($attributesData, $productAttributePivot->input_en);
                                array_push($attributesData, $productAttributePivot->input_ar);
                            } else {
                                $value = OptionValue::find($productAttributePivot->value_id);
                                if ($value) {
                                    array_push($attributesData, $value->name_en);
                                    array_push($attributesData, $value->name_ar);
                                } else {
                                    array_push($attributesData, '');
                                    array_push($attributesData, '');
                                }
                            }
                        } else {
                            array_push($attributesData, '');
                            array_push($attributesData, '');
                        }
                    }

//                        foreach ($productAttributes as $productAttribute) {
//
//                            if ($productAttribute['option']->type == 5) {
//                                //$inputData = ProductOptionValues::where('product_id', $item->id)->where('option_id', $productAttribute->option->id)->where('value_id', $productAttribute->value->id)->where('type', '0')->first();
//                                array_push($attributesData, $productAttribute['option']->name_en);
//                                array_push($attributesData, $productAttribute['value']['input_en']);
//                                array_push($attributesData, $productAttribute['value']['input_ar']);
//                            } else {
//                                array_push($attributesData, $productAttribute['option']->name_en);
//                                array_push($attributesData, $productAttribute['value']->name_en);
//                                array_push($attributesData, $productAttribute['value']->name_ar);
//                            }
//
//
//                            $s++;
//                        }

//                        if ($s < $this->attributesCount) {
//                            for ($x = $s + 1; $x <= $this->attributesCount; $x++) {
//                                array_push($attributesData, "");
//                                array_push($attributesData, "");
//                                array_push($attributesData, "");
//
//                            }
//                        }
//                    } else {
//                        $attributesData = [];
//                        for ($x = 1; $x <= $this->attributesCount; $x++) {
//                            array_push($attributesData, "");
//                            array_push($attributesData, "");
//                            array_push($attributesData, "");
//                        }

//                    }

                    $mainProductsArray = [
                        '',
                        $item->sku,
                        '',
                        $relatedSkus,
                        $item->name,
                        $item->name_ar,
                        $item->description,
                        $item->description_ar,
                        $item->long_description_en,
                        $item->long_description_ar,
                        $item->meta_title,
                        $item->meta_title_ar,
                        $item->meta_description,
                        $item->meta_description_ar,
                        $item->keywords,
                        $item->preorder,
                        $item->preorder_start_date,
                        $item->preorder_end_date,
                        // $item->preorder_price,
                        $item->weight,
                        isset($category->name) ? $category->name : "",
                        $group,
                        $subCategory ? $subCategory->name : "",
                        $subCategory ? $subCategory->slug : "",
                        optional(optional($item->optionalSubCategory)->parent)->name,
                        optional($item->optionalSubCategory)->name,
                        $brandName,
                        $tags,
                        $item->subtract_stock,
                        $item->stock,
                        $item->barcode,
                        $item->price,
                        $item->discount_price,
                        $item->discount_start_date,
                        $item->discount_end_date,
                        $item->image,
                        $images,
                        $item->order,
                        $item->active,
                        $item->type,
                        $item->has_stock,
                        $item->bundle_checkout,
                        $bundleSkus,
                    ];

                    foreach ($attributesData as $attribute) {
                        array_push($mainProductsArray, $attribute);
                    }
//                    dd($mainProductsArray);
                    if (count($item->productVariants) > 0) {

                        foreach ($item->productVariants as $productVariant) {
                            $productOptions = $productVariant->customerVariantOptionsLists();

                            $attributesData = [];
                            foreach ($attributes as $attribute) {
                                $productAttributePivot = ProductOptionValues::where('option_id', $attribute->id)->where('product_id', $productVariant->id)->where('type', '0')->first();
                                array_push($attributesData, $attribute->name_en);
                                if ($productAttributePivot) {
                                    if ($attribute->type == 5) {
                                        array_push($attributesData, $productAttributePivot->input_en);
                                        array_push($attributesData, $productAttributePivot->input_ar);
                                    } else {
                                        $value = OptionValue::find($productAttributePivot->value_id);
                                        if ($value) {
                                            array_push($attributesData, $value->name_en);
                                            array_push($attributesData, $value->name_ar);
                                        } else {
                                            array_push($attributesData, '');
                                            array_push($attributesData, '');
                                        }
                                    }
                                } else {
                                    array_push($attributesData, '');
                                    array_push($attributesData, '');
                                }
                            }

                            $optionsData = [];
                            if (!empty($productOptions)) {
                                $s = 0;
                                foreach ($productOptions as $productOption) {
                                    array_push($optionsData, $productOption['option']['name']);
                                    array_push($optionsData, isset($productOption['values'][0]) ? $productOption['values'][0]['name'] : "");
                                    array_push($optionsData, isset($productOption['values'][0]) ? $productOption['values'][0]['image'] : "");
                                    $s++;
                                }
                                if ($s < $this->optionsCount) {
                                    for ($x = $s; $x <= $this->optionsCount; $x++) {
                                        array_push($optionsData, "");
                                        array_push($optionsData, "");
                                        array_push($optionsData, "");
                                    }
                                }
                            } else {
                                for ($x = 1; $x <= $this->optionsCount; $x++) {
                                    array_push($optionsData, "");
                                    array_push($optionsData, "");
                                    array_push($optionsData, "");
                                }
                            }
                            $bundleSkus = implode(',', $productVariant->bundleProduct->pluck('sku')->toArray());
                            $variantImages = $productVariant->images->pluck('url')->implode(',');
                            $productVariantArray = [
                                $productVariant->parent_id,
                                $item->sku,
                                $productVariant->sku,
                                $relatedSkus,
                                $productVariant->name,
                                $productVariant->name_ar,
                                $productVariant->description,
                                $productVariant->description_ar,
                                $productVariant->long_description_en,
                                $productVariant->long_description_ar,
                                $productVariant->meta_title,
                                $productVariant->meta_title_ar,
                                $productVariant->meta_description,
                                $productVariant->meta_description_ar,
                                $productVariant->keywords,
                                $item->preorder,
                                $item->preorder_start_date,
                                $item->preorder_end_date,
                                // $item->preorder_price,
                                $productVariant->weight,
                                isset($category->name) ? $category->name : "",
                                $group,
                                $subCategory ? $subCategory->name : "",
                                $subCategory ? $subCategory->slug : "",
                                optional(optional($item->optionalSubCategory)->parent)->name,
                                optional($item->optionalSubCategory)->name,
                                $brandName,
                                $tags,
                                $item->subtract_stock,
                                $productVariant->stock,
                                $productVariant->barcode,
                                $productVariant->price,
                                $productVariant->discount_price,
                                $productVariant->discount_start_date,
                                $productVariant->discount_end_date,
                                $productVariant->image,
                                $variantImages,
                                $productVariant->order,
                                $productVariant->active,
                                $item->type,
                                $item->has_stock,
                                $item->bundle_checkout,
                                $bundleSkus,
                            ];

                            foreach ($attributesData as $attribute) {
                                array_push($productVariantArray, $attribute);
                            }

                            foreach ($optionsData as $options) {
                                array_push($productVariantArray, $options);
                            }

                            //$i == 0 ? $variants[] = $mainProductsArray : '';
                            $variants[] = $productVariantArray;
                            $i++;
                        }

                        $sheet->rows($variants);
                    } else {
                        //$sheet->row($key + 2, $mainProductsArray);
                    }
                }
            });

        })->store('xlsx', storage_path("app/public/exports"));

        $filePath = url("storage/exports") . '/' . $fileName . '.xlsx';
        Log::info("Export Finished", ["path" => $filePath]);
        $this->pushService->notifyAdmins("File Exported ", "Your export has completed! You can download the file from", '', 11, $filePath, \Auth::id());
        return $this->jsonResponse("Success");
    }

    public function fullImport(Request $request)
    {
        $this->validate($request, ["file" => "required", "approved" => "sometimes|nullable|boolean"]);
        isset($request->approved) ? $approved = $request->approved : $approved = 1;
        $totalCount = 0;
        $totalMainProductCount = 0;
        $totalVariantProductCount = 0;
        $totalMissedProductsCount = 0;
        $totalMissedVariantsCount = 0;
        $totalCreatedProductsCount = 0;
        $totalCreatedVariantsCount = 0;
        $totalUpdatedVariantsCount = 0;
        $missedDetails = [];
        $mainSkuArray = [];
        try {
            //$file = $request->file('file')->getRealPath();
            Excel::load($request->file, function ($reader) use (&$missedDetails, &$totalMainProductCount, &$totalVariantProductCount, &$totalCount, &$approved, &$totalMissedProductsCount, &$totalMissedVariantsCount, &$totalCreatedProductsCount, &$totalCreatedVariantsCount, &$totalUpdatedProductsCount, &$totalUpdatedVariantsCount, &$mainSkuArray) {
                // Loop through all sheets
                $rows = $reader->all();
                if (!empty($rows)) {
                    foreach ($rows as $rowKey => $row) {
                        Log::info("Row: {$rowKey}");
                        if (isset($row["main_sku"]) && is_float($row['main_sku'])) {
                            $MainSku = (int)$row['main_sku'];
                        } elseif (isset($row["main_sku"])) {
                            $MainSku = $row['main_sku'];
                        }
                        $productNameEn = isset($row['name']) && !empty($row['name']) ? $row['name'] : '';
                        $VariantSku = isset($row['variant_sku']) && !empty($row['variant_sku']) ? $row['variant_sku'] : '';
                        $productNameAr = isset($row['name_ar']) && !empty($row['name_ar']) ? $row['name_ar'] : '';
                        $productDescriptionEn = isset($row['description']) && !empty($row['description']) ? $row['description'] : '';
                        $productDescriptionAr = isset($row['description_ar']) && !empty($row['description_ar']) ? $row['description_ar'] : '';
                        $productLongDescriptionEn = isset($row['long_description_en']) && !empty($row['long_description_en']) ? $row['long_description_en'] : '';
                        $productLongDescriptionAr = isset($row['long_description_ar']) && !empty($row['long_description_ar']) ? $row['long_description_ar'] : '';
                        $productmeta_title = isset($row['meta_title']) && !empty($row['meta_title']) ? $row['meta_title'] : '';
                        $productmeta_title_ar = isset($row['meta_title_ar']) && !empty($row['meta_title_ar']) ? $row['meta_title_ar'] : '';
                        $productmeta_description = isset($row['meta_description']) && !empty($row['meta_description']) ? $row['meta_description'] : '';
                        $productmeta_description_ar = isset($row['meta_description_ar']) && !empty($row['meta_description_ar']) ? $row['meta_description_ar'] : '';
                        $productkeywords = isset($row['keywords']) && !empty($row['keywords']) ? $row['keywords'] : '';
                        $productMainImage = isset($row['main_image']) && !empty($row['main_image']) ? $row['main_image'] : '';
                        $productActive = isset($row['active']) && !empty($row['active']) ? $row['active'] : 1;
                        $productPreorder = isset($row['preorder']) && !empty($row['preorder']) ? $row['preorder'] : 0;
                        $preorderStartDate = isset($row['preorder_start_date']) && !empty($row['preorder_start_date']) && $row['preorder_start_date'] > 0 ? $row['preorder_start_date'] : null;
                        $preorderEndDate = isset($row['preorder_start_date']) && !empty($row['preorder_start_date']) && $row['preorder_start_date'] > 0 ? $row['preorder_start_date'] : null;
                        // $productPreorderPrice = isset($row['preorder_price']) && !empty($row['preorder_price']) ? $row['preorder_price'] : null;
                        $productWeight = isset($row['weight']) && !empty($row['weight']) ? $row['weight'] : null;
                        $productSubtractStock = isset($row['subtract_stock']) && !empty($row['subtract_stock']) ? $row['subtract_stock'] : 1;
                        $productStock = isset($row['stock']) && !empty($row['stock']) && ($row['stock']) > 0 ? $row['stock'] : 0;
                        $productBarcode = isset($row['barcode']) && !empty($row['barcode']) ? $row['barcode'] : '';
                        $productPrice = isset($row['price']) && !empty($row['price']) && $row['price'] > 0 ? $row['price'] : null;
                        $productOrder = isset($row['order']) && !empty($row['order']) ? $row['order'] : 0;
                        $type = isset($row['type']) && !empty($row['type']) ? $row['type'] : 1;
                        $has_stock = isset($row['has_stock']) && !empty($row['has_stock']) ? $row['has_stock'] : 0;
                        $bundle_checkout = isset($row['bundle_checkout']) && !empty($row['bundle_checkout']) ? $row['bundle_checkout'] : 0;
                        $productDiscountPrice = isset($row['discount_price']) && !empty($row['discount_price']) && $row['discount_price'] > 0 ? $row['discount_price'] : null;
                        $discountStartDate = isset($row['discount_start_date']) && !empty($row['discount_start_date']) && $row['discount_start_date'] > 0 ? $row['discount_start_date'] : null;
                        $discountEndDate = isset($row['discount_end_date']) && !empty($row['discount_end_date']) && $row['discount_end_date'] > 0 ? $row['discount_end_date'] : null;
                        $productTags = isset($row['tags']) && !empty($row['tags']) ? explode(',', $row['tags']) : '';
                        $relatedSkus = isset($row['related_skus']) && !empty($row['related_skus']) ? explode(',', $row['related_skus']) : [];
                        $bundleSkus = isset($row['bundle_skus']) && !empty($row['bundle_skus']) ? explode(',', $row['bundle_skus']) : [];
                        $productImages = isset($row['images']) && !empty($row['images']) ? explode(',', $row['images']) : '';
                        $categoryOptional = isset($row['category_optional']) && !empty($row['category_optional']) ? $row['category_optional'] : null;
                        $subcategoryOptional = isset($row['subcategory_optional']) && !empty($row['subcategory_optional']) ? $row['subcategory_optional'] : null;
                        $brandID = null;
                        $subCategoryID = null;
                        $product = null;
                        $variantProduct = null;
                        $mainProduct = null;
                        $totalCount++;

                        if (filter_var($productMainImage, FILTER_VALIDATE_URL) === FALSE) {
                            $productMainImage = str_replace(' ', '-', $productMainImage);
                            $productMainImage = url("storage/uploads/" . $productMainImage);
                        }
                        if (isset($row['main_sku']) && $row['main_sku'] != null) {
                            $productData = [
                                "sku" => $MainSku,
                                "name" => $productNameEn,
                                "name_ar" => $productNameAr,
                                "description" => $productDescriptionEn,
                                "description_ar" => $productDescriptionAr,
                                "long_description_en" => $productLongDescriptionEn,
                                "long_description_ar" => $productLongDescriptionAr,
                                "meta_title" => $productmeta_title,
                                "meta_description" => $productmeta_description,
                                "keywords" => $productkeywords,
                                "preorder" => $productPreorder,
                                "preorder_start_date" => $preorderStartDate,
                                "preorder_end_date" => $preorderEndDate,
                                // "preorder_price" => $productPreorderPrice,
                                "image" => $productMainImage,
                                "weight" => $productWeight,
                                "stock" => $productStock,
                                "barcode" => $productBarcode,
                                "active" => $productActive,
                                "subtract_stock" => $productSubtractStock,
                                "category_optional" => $categoryOptional,
                                "subcategory_optional" => $subcategoryOptional,
                                "price" => null,
                                "order" => $productOrder,
                                "type" => $type,
                                "has_stock" => $has_stock,
                                "bundle_checkout" => $bundle_checkout,
                                "discount_price" => null,
                            ];
                            $productVariantData = [
                                "sku" => $VariantSku,
                                "name" => $productNameEn,
                                "name_ar" => $productNameAr,
                                "description" => $productDescriptionEn,
                                "description_ar" => $productDescriptionAr,
                                "long_description_en" => $productLongDescriptionEn,
                                "long_description_ar" => $productLongDescriptionAr,
                                "meta_title" => $productmeta_title,
                                "meta_title_ar" => $productmeta_title_ar,
                                "meta_description" => $productmeta_description,
                                "meta_description_ar" => $productmeta_description_ar,
                                "keywords" => $productkeywords,
                                "preorder" => $productPreorder,
                                "weight" => $productWeight,
                                "image" => $productMainImage,
                                "stock" => $productStock,
                                "barcode" => $productBarcode,
                                "active" => $productActive,
                                "price" => $productPrice,
                                "order" => $productOrder,
                                "discount_price" => $productDiscountPrice,
                                "discount_start_date" => $discountStartDate,
                                "discount_end_date" => $discountEndDate
                            ];
                            $priceInstance = [
                                'price' => $productPrice,
                                "discount_price" => $productDiscountPrice,
                                "discount_start_date" => $discountStartDate,
                                "discount_end_date" => $discountEndDate
                            ];

                            $mainProduct = Product::where('sku', $MainSku)->first();

                            if (isset($row['subcategory_slug']) && $row['subcategory_slug'] != null) {
                                $subCategory = Category::whereNotNull("parent_id")->where("slug", $row['subcategory_slug'])->first();
                                if ($subCategory) {
                                    $subCategoryID = $subCategory->id;
                                }
                            } else {
                                if (isset($row['subcategory']) && $row['subcategory'] != null && isset($row['category']) && $row['category'] != null) {
                                    $query = Category::query();
                                    $query->where('name', $row['subcategory']);
                                    $query->whereHas('parent', function ($q) use ($row) {
                                        $q->where('name', $row['category']);
                                    });
                                    $query->when(isset($row['group']) && !empty($row['group']), function ($q) use ($row) {
                                        $q->whereHas('group', function ($q) use ($row) {
                                            $q->where('groups.name_en', $row['group']);
                                        });
                                    });
                                    $subCategory = $query->first();
                                    if ($subCategory) {
                                        $subCategoryID = $subCategory->id;
                                    }
                                }
                            }
                            if (isset($row['brand']) && $row['brand'] != null) {
                                $brandData = Brand::where("name", $row['brand'])->first();
                                if ($brandData) {
                                    $brandID = $brandData->id;
                                    $productData['brand_id'] = $brandID;
                                    $productVariantData['brand_id'] = $brandID;
                                } else {
                                    $totalMissedProductsCount++;
                                    $missedDetails[] = 'variant with sku ' . $MainSku . '( Brand Not Found )';
                                }
                            }

                            if (!$subCategoryID) {
                                $totalMissedProductsCount++;
                                $missedDetails[] = 'variant with sku ' . $MainSku . '( Category Not Found )';
                                continue;
                            } else {
                                $productData['category_id'] = $subCategoryID;
                                $productVariantData['category_id'] = $subCategoryID;
                            }
                            if (isset($row['category_optional'], $row['subcategory_optional'])) {
                                $optionalSubCategory = Category::where('name', $subcategoryOptional)
                                    ->whereHas('parent', function ($q) use ($categoryOptional) {
                                        $q->where('name', $categoryOptional);
                                    })->first();
                                $productData['optional_sub_category_id'] = optional($optionalSubCategory)->id;
                                $productVariantData['optional_sub_category_id'] = optional($optionalSubCategory)->id;
                            }

                            if (!in_array($MainSku, $mainSkuArray)) {


                                if ($mainProduct) {
                                    $totalCreatedProductsCount++;
                                    $approved == 1 ? $mainProduct->update($productData) : '';
                                    $priceInstance['product_id'] = $mainProduct->id;
                                    $approved == 1 ? ProductPrice::create($priceInstance) : '';
                                } else {
                                    $totalUpdatedProductsCount++;
                                    $approved == 1 ? $mainProduct = Product::create($productData) : '';
                                    // dd($mainProduct);
                                    $priceInstance['product_id'] = $mainProduct->id;
                                    $approved == 1 ? ProductPrice::create($priceInstance) : '';
                                }
                                $mainSkuArray[] = $MainSku;
                            }

                            if ($row['variant_sku'] != null) {
                                $totalVariantProductCount++;
                                $productVariantData['parent_id'] = $mainProduct->id;
                                $variantProduct = Product::where('sku', $VariantSku)->first();
                                if ($variantProduct) {
                                    $totalUpdatedVariantsCount++;
                                    $approved == 1 ? $variantProduct->update($productVariantData) : '';
                                } else {
                                    $totalCreatedVariantsCount++;
                                    $approved == 1 ? $variantProduct = Product::create($productVariantData) : '';
                                }
                                $priceInstance['product_id'] = $variantProduct->id;
                                $approved == 1 ? ProductPrice::create($priceInstance) : '';

                            }

                            if (isset($variantProduct)) {
                                $x = 1;
                                $approved == 1 ? ProductOptionValues::where('product_id', $variantProduct->id)->where("type", '1')->delete() : '';
                                $approved == 1 ? ProductOptionValues::where('product_id', $mainProduct->id)->where("type", '1')->delete() : '';
                                while ((isset($row['option_name_' . $x]) && !empty($row['option_name_' . $x]))) {
                                    $option = Option::where('name_en', trim($row['option_name_' . $x]))->first();

                                    if ($option) {

                                        if ($x == 1) {
                                            // make first option the default
                                            $mainProduct->update(["option_default_id" => $option->id]);
                                            $variantProduct->update(["option_default_id" => $option->id]);
                                        }

                                        $value = OptionValue::where('name_en', trim($row['option_value_' . $x]))->where('option_id', $option->id)->first();
                                        if (!$value && $option->type != 5) {
                                            $value = OptionValue::create(['name_en' => trim($row['option_value_' . $x]), 'name_ar' => isset($row['option_value_ar_' . $x]) ? trim($row['option_value_ar_' . $x]) : trim($row['option_value_' . $x]), 'option_id' => $option->id]);
                                        }
                                        if ($approved == 1) {
                                            $variantOptionValueData = [
                                                'product_id' => $variantProduct->id,
                                                'option_id' => $option->id,
                                                'type' => '1'
                                            ];

                                            if ($option->type == 4 && $value) {
                                                $variantOptionValueData['value_id'] = $value ? $value->id : null;
                                                $image = isset($row['option_image_' . $x]) ? $row['option_image_' . $x] : '';
                                                if (filter_var($image, FILTER_VALIDATE_URL) === FALSE) {
                                                    $image = str_replace(' ', '-', $image);
                                                    $image = url("storage/uploads/" . $image);
                                                }
                                                $variantOptionValueData['image'] = isset($row['option_image_' . $x]) ? $image : '';
                                                $approved == 1 ? ProductOptionValues::updateOrCreate(['product_id' => $variantProduct->id, 'option_id' => $option->id], $variantOptionValueData) : '';
                                            } elseif ($value) {
                                                $variantOptionValueData['value_id'] = $value->id;
                                                $approved == 1 ? ProductOptionValues::updateOrCreate(['product_id' => $variantProduct->id, 'option_id' => $option->id], $variantOptionValueData) : '';
                                            } else {
                                                $totalMissedVariantsCount++;
                                                $missedDetails[] = 'variant with sku ' . $VariantSku . '( value ' . $row['option_value_' . $x] . ' Not Found )';
                                            }

                                        }

                                    } else {
                                        $totalMissedVariantsCount++;
                                        $missedDetails[] = 'variant with sku ' . $VariantSku . '( option ' . $row['option_name_' . $x] . ' Not Found )';
                                    }
                                    $x++;
                                }

                                $mainOptions = $mainProduct->variantOptions()->where('product_option_values.type', "1")->groupBy("option_id")->get();
                                foreach ($mainOptions as $mainOption) {
                                    $pov = ProductOptionValues::where("product_id", $mainProduct->id)->where("option_id", $mainOption->option_id)->first();
                                    if (!$pov) {
                                        ProductOptionValues::updateOrCreate(['product_id' => $mainProduct->id, 'option_id' => $mainOption->option_id, 'value_id' => $mainOption->value_id, 'type' => $mainOption->type]);
                                    }
                                }
                            }

                            if (isset($variantProduct)) {
                                $s = 1;
                                $approved == 1 ? ProductOptionValues::where('product_id', $variantProduct->id)->where("type", '0')->delete() : '';
                                $subCategoryID = $variantProduct->parent->category->id;
                                while ((isset($row['attribute_name_' . $s]) && !empty($row['attribute_name_' . $s]))) {
                                    $option = Option::where('name_en', $row['attribute_name_' . $s])->first();
                                    if ($option) {
                                        if (isset($row['attribute_value_' . $s]) && $row['attribute_value_ar_' . $s]) {


                                            $subCategoryOption = CategoryOption::where('sub_category_id', $subCategoryID)->where('option_id', $option->id)->first();
                                            if ($subCategoryOption) {
                                                $value = OptionValue::where('name_en', $row['attribute_value_' . $s])->where('option_id', $option->id)->first();
                                                $ProductOptionValueData = [
                                                    'product_id' => $variantProduct->id,
                                                    'option_id' => $option->id,
                                                    'type' => '0'
                                                ];
                                                if (!$value && $option->type != 5) {
                                                    $value = OptionValue::create(['name_en' => trim($row['attribute_value_' . $s]), 'name_ar' => trim($row['attribute_value_ar_' . $s]), 'option_id' => $option->id]);
                                                }
                                                if ($option->type == 5) {
                                                    $ProductOptionValueData['input_en'] = isset($row['attribute_value_' . $s]) ? $row['attribute_value_' . $s] : '';
                                                    $ProductOptionValueData['input_ar'] = isset($row['attribute_value_ar_' . $s]) ? $row['attribute_value_ar_' . $s] : '';
                                                    $approved == 1 ? ProductOptionValues::updateOrCreate($ProductOptionValueData) : '';
                                                } else {
                                                    if ($value) {
                                                        $ProductOptionValueData['value_id'] = $value->id;
                                                        $approved == 1 ? ProductOptionValues::updateOrCreate($ProductOptionValueData) : '';
                                                    } else {
                                                        ProductOptionValues::where('product_id', $variantProduct->id)->where('option_id', $option->id)->where('type', '0')->delete();
                                                        $totalMissedProductsCount++;
                                                        $missedDetails[] = 'Product with sku ' . $MainSku . '( Attribute value ' . $row['attribute_value_' . $s] . ' Not Found )';
                                                    }
                                                }
                                            } else {
                                                $totalMissedProductsCount++;
                                                $missedDetails[] = 'Product with sku ' . $MainSku . '( Attribute ' . $row['attribute_name_' . $s] . ' Not added in Sub Category ' . $mainProduct->category->name . ' with ID ' . $subCategoryID . ' and option ID ' . $option->id . ')';
                                            }
                                        } else {
                                            ProductOptionValues::where('product_id', $variantProduct->id)->where('option_id', $option->id)->where('type', '0')->delete();
                                        }
                                    } else {
                                        $totalMissedProductsCount++;
                                        $missedDetails[] = 'Product with sku ' . $MainSku . '( Attribute ' . $row['attribute_name_' . $s] . ' Not Found )';
                                    }
                                    $s++;
                                }
                            }

                            if (is_array($productTags) && count($productTags)) {
                                if ($mainProduct && $approved == 1) {
                                    $approved == 1 ? ProductTag::where('product_id', $mainProduct->id)->delete() : '';
                                }
                                foreach ($productTags as $tag) {
                                    $tagData = Tag::where("name_en", $tag)->first();
                                    if ($tagData) {
                                        if ($mainProduct && $approved == 1) {
                                            $data = [
                                                'tag_id' => $tagData->id,
                                                'product_id' => $mainProduct->id,
                                            ];
                                            $approved == 1 ? ProductTag::updateOrCreate($data) : '';
                                        }
                                    } else {
                                        $totalMissedProductsCount++;
                                        $missedDetails[] = 'Product with sku ' . $MainSku . '( Tag  ' . $tag . ' Not Found )';
                                    }
                                }
                            }

                            if (count($relatedSkus)) {
                                $mainProduct->relatedSkus()->delete();
                                foreach ($relatedSkus as $sku) {
                                    $mainProduct->relatedSkus()->create(["sku" => $sku]);
                                }
                            }

                            $variantProduct->bundleProduct()->delete();
                            if (count($bundleSkus)) {
                                Log::info($bundleSkus);
                                foreach ($bundleSkus as $bsku) {
                                    $bundleProduct = Product::where("sku", $bsku)->first();
                                    Log::info("PRODUCT: ", ["sku" => $bsku, "id" => $bundleProduct ? $bundleProduct->id : null, "variant" => $variantProduct->sku]);
                                    if ($bundleProduct) {
                                        $variantProduct->bundleProduct()->attach($bundleProduct->id);
                                    }
                                }
                            }

                            isset($variantProduct) ? $product = $variantProduct : $product = $mainProduct;
                            if (isset($product) && $approved == 1) {
                                if (is_array($productImages) && count($productImages)) {
                                    $approved == 1 ? $product->images()->delete() : '';
                                    foreach ($productImages as $image) {
                                        if (filter_var($image, FILTER_VALIDATE_URL) === FALSE) {
                                            $image = str_replace(' ', '-', $image);
                                            $image = url("storage/uploads/" . $image);
                                        }
                                        $approved == 1 ? $product->images()->create(["url" => $image]) : '';
                                    }
                                }
                            }


                        }

                        Log::info("Imported Row: " . $rowKey);
                    }
                }

            });
            $totalApprovedProductsCount = $totalCreatedProductsCount + $totalUpdatedProductsCount;
            $totalApprovedVariantsCount = $totalCreatedVariantsCount + $totalUpdatedVariantsCount;
            $totalApprovedCount = $totalApprovedProductsCount + $totalApprovedVariantsCount;
            $totalMissedCount = $totalMissedProductsCount + $totalMissedVariantsCount;
            $totalCreatedCount = $totalCreatedProductsCount + $totalCreatedVariantsCount;
            $totalUpdatedCount = $totalUpdatedProductsCount + $totalUpdatedVariantsCount;
            $data = [
                'total_count' => $totalCount,
                'total_product_count' => $totalMainProductCount,
                'total_variant_product_count' => $totalVariantProductCount,
                'total_approved_count' => $totalApprovedCount,
                'total_approved_products_count' => $totalApprovedProductsCount,
                'total_approved_variants_count' => $totalApprovedVariantsCount,
                'total_missed_count' => $totalMissedCount,
                'total_missed_products_count' => $totalMissedProductsCount,
                'total_missed_variants_count' => $totalMissedVariantsCount,
                'total_created_count' => $totalCreatedCount,
                'total_created_products_count' => $totalCreatedProductsCount,
                'total_created_variants_count' => $totalCreatedVariantsCount,
                'total_updated_count' => $totalUpdatedCount,
                'total_updated_products_count' => $totalUpdatedProductsCount,
                'total_updated_variants_count' => $totalUpdatedVariantsCount,
                'missed_details' => $missedDetails,
            ];
            Log::info("IMPORT FINISHED");
            Artisan::call('cache:clear');
            $this->pushService->notifyAdmins("File Imported", "Your import has completed! You can check the result from", '', 10, json_encode($data), \Auth::id());
            return $this->jsonResponse("Success", $data);
        } catch (\Exception $e) {
            // dd($e);
            Log::error("Import File Error: " . $e->getMessage());
            return $this->errorResponse($e->getMessage(), "Invalid data", $e->getTrace(), 422);
        }
    }

    public function magentoImport(Request $request)
    {
        $this->validate($request, ["file" => "required", "approved" => "sometimes|nullable|boolean"]);
        isset($request->approved) ? $approved = $request->approved : $approved = 1;
        $totalCount = 0;
        $totalMainProductCount = 0;
        $totalVariantProductCount = 0;
        $totalMissedProductsCount = 0;
        $totalMissedVariantsCount = 0;
        $totalCreatedProductsCount = 0;
        $totalCreatedVariantsCount = 0;
        $totalUpdatedVariantsCount = 0;
        $totalUpdatedProductsCount = 0;
        $missedDetails = [];
        $mainSkuArray = [];
        $mainProductIDS = [];
        $variantProductIDS = [];
        try {
            //$file = $request->file('file')->getRealPath();
            Excel::load($request->file, function ($reader) use (&$variantProductIDS, &$mainProductIDS, &$missedDetails, &$totalMainProductCount, &$totalVariantProductCount, &$totalCount, &$approved, &$totalMissedProductsCount, &$totalMissedVariantsCount, &$totalCreatedProductsCount, &$totalCreatedVariantsCount, &$totalUpdatedProductsCount, &$totalUpdatedVariantsCount, &$mainSkuArray) {
                // Loop through all sheets
                $rows = $reader->all();
                if (!empty($rows)) {
                    foreach ($rows as $row) {
                        if ($row['store_view_code'] == 'en') {
                            continue;
                        }
                        if (isset($row["sku"]) && is_float($row['sku'])) {
                            $MainSku = (int)$row['sku'];
                        } elseif (isset($row["sku"])) {
                            $MainSku = $row['sku'];
                        }
                        $productCategories = isset($row['categories']) && !empty($row['categories']) ? $row['categories'] : null;
                        $productLang = isset($row['store_view_code']) && !empty($row['store_view_code']) ? $row['store_view_code'] : null;
                        $productNameEn = isset($row['name']) && !empty($row['name']) ? $row['name'] : null;
                        $productNameAr = isset($row['name']) && !empty($row['name']) ? $row['name'] : null;
                        $productDescriptionEn = isset($row['short_description']) && !empty($row['short_description']) ? $row['short_description'] : " ";
                        $productDescriptionAr = isset($row['short_description']) && !empty($row['short_description']) ? $row['short_description'] : " ";
                        $productLongDescriptionEn = isset($row['description']) && !empty($row['description']) ? $row['description'] : null;
                        $productLongDescriptionAr = isset($row['description']) && !empty($row['description']) ? $row['description'] : null;
                        $productWeight = isset($row['weight']) && !empty($row['weight']) ? $row['weight'] : null;
                        $productPrice = isset($row['price']) && !empty($row['price']) && $row['price'] > 0 ? $row['price'] : null;
                        $productDiscountPrice = isset($row['special_price']) && !empty($row['special_price']) && $row['special_price'] > 0 && $row['special_price'] != 0 ? $row['special_price'] : null;
                        $productMainImage = isset($row['base_image']) && !empty($row['base_image']) ? "https://mobilaty.com/media/catalog/product" . $row['base_image'] : " ";
                        $productImagesRows = isset($row['additional_images']) && !empty($row['additional_images']) ? $row['additional_images'] : null;
                        $productAttributes = isset($row['additional_attributes']) && !empty($row['additional_attributes']) ? $row['additional_attributes'] : null;
                        $productStock = isset($row['qty']) && !empty($row['qty']) && ($row['qty']) > 0 ? $row['qty'] : 0;
                        $productSubtractStock = isset($row['is_in_stock']) && !empty($row['is_in_stock']) && ($row['is_in_stock']) ? $row['is_in_stock'] : 1;
                        $productMaxPerOrder = isset($row['max_cart_qty']) && !empty($row['max_cart_qty']) && ($row['max_cart_qty']) ? $row['max_cart_qty'] : 0;
                        $productStockAlert = isset($row['notify_on_stock_below']) && !empty($row['notify_on_stock_below']) && ($row['notify_on_stock_below']) ? $row['notify_on_stock_below'] : 0;
                        preg_match_all("/([a-zA-Z0-9_]+=(?:[<>\p{Arabic}a-zA-Z0-9×\s\-\–\/\;\'\"\:\%\.\&\@\~\+\*\(\)\=]|(?:,\s))+)/u", $productAttributes, $output_array);


                        if ($productCategories) {
                            $productCategories = explode(',', $productCategories);
                            $productCategories = array_reverse($productCategories);
                            $productCategories = explode('/', $productCategories[0]);
                            $category = $productCategories[1];
                            $subCategory = isset($productCategories[2]) ? $productCategories[2] : $category;
                        } else {
                            $category = null;
                            $subCategory = null;
                        }


                        if ($category && $subCategory) {
                            $category = Category::firstOrCreate([
                                'parent_id' => null,
                                'name' => $category
                            ]);
                            $subCategory = Category::firstOrCreate([
                                'parent_id' => $category->id,
                                'name' => $subCategory
                            ]);
                            $subCategoryID = $subCategory->id;
                        } else {
                            if (!$productLang) {
                                continue;
                            }
                        }
                        $totalCount++;
                        if ($productLang == 'en') {
                            continue;
                        }

                        if (isset($row['sku']) && $row['sku'] != null && isset($row['sku'])) {

                            $productData = [
                                "sku" => "main_" . $MainSku,
                            ];
                            if ($productLang) {
                                if ($productLang == 'sa') {
                                    $productData['name_ar'] = $productNameAr;
                                    $productData['description_ar'] = $productDescriptionAr;
                                    $productData['long_description_ar'] = $productLongDescriptionAr;
                                } else {
                                    continue;
                                }
                            } else {
                                $productData['category_id'] = $subCategoryID;
                                $productData['stock_alert'] = $productStockAlert;
                                $productData['max_per_order'] = $productMaxPerOrder;
                                $productData['subtract_stock'] = $productSubtractStock;
                                $productData['discount_price'] = $productDiscountPrice;
                                $productData['stock'] = $productStock;
                                $productData['weight'] = $productWeight;
                                $productData['price'] = $productPrice;
                                $productData['image'] = $productMainImage;
                                $productData['name'] = $productNameEn;
                                $productData['description'] = $productDescriptionEn;
                                $productData['long_description_en'] = $productLongDescriptionEn;
                            }

                            $variantData = $productData;
                            $variantData['sku'] = $MainSku;


                            $mainProduct = Product::where('sku', $productData['sku'])->first();


                            if ($mainProduct) {
                                $totalUpdatedProductsCount++;
                                $mainProduct->update($productData);
                            } else {
                                $totalCreatedProductsCount++;
                                $mainProduct = Product::create($productData);
                            }
                            if (!in_array($mainProduct->id, $mainProductIDS)) {
                                $totalMainProductCount++;
                                array_push($mainProductIDS, $mainProduct->id);
                            }

                            $variantData['parent_id'] = $mainProduct->id;

                            $variantProduct = Product::where('sku', $variantData['sku'])->where('parent_id', $mainProduct->id)->first();


                            if ($variantProduct) {
                                $totalUpdatedVariantsCount++;
                                $variantProduct->update($variantData);
                            } else {
                                $totalCreatedVariantsCount++;
                                $variantProduct = Product::create($variantData);
                            }
                            if (!in_array($variantProduct->id, $variantProductIDS)) {
                                $totalVariantProductCount++;
                                array_push($variantProductIDS, $mainProduct->id);
                            }


                            if (!$productLang) {
                                if (isset($output_array[0]) && is_array($output_array[0])) {
                                    ProductOptionValues::where('product_id', $mainProduct->id)->where('type', 0)->delete();
                                    foreach ($output_array[0] as $attribute) {
                                        $optionValues = explode('=', $attribute, 2);
                                        $optionData = isset($optionValues[0]) ? $optionValues[0] : null;
                                        $valueData = isset($optionValues[1]) ? rtrim($optionValues[1], ",") : null;

                                        if ($optionData == "mgs_brand") {
                                            $brand = Brand::firstOrCreate([
                                                'name' => $valueData
                                            ]);
                                            $variantProduct->update(['brand_id' => $brand->id]);
                                            $mainProduct->update(['brand_id' => $brand->id]);
                                        }
                                        if ($optionData && $valueData) {
                                            $option = Option::where('name_en', $optionData)->where('type', 5)->first();
                                            if (!$option) {
                                                $option = Option::create(['name_en' => $optionData, 'name_ar' => $optionData, 'type' => 5,]);
                                            }
                                            $productOptionValue = ProductOptionValues::where('product_id', $mainProduct->id)->where('option_id', $option->id)->first();
                                            if ($productOptionValue) {
                                                $productOptionValue->update(['input_en' => $valueData, 'input_ar' => $valueData]);
                                            } else {
                                                ProductOptionValues::create(['product_id' => $mainProduct->id, 'option_id' => $option->id, 'input_en' => $valueData, 'input_ar' => $valueData, 'type' => "0"]);
                                            }
                                        }
                                    }
                                }
                                if ($productImagesRows) {
                                    $productImagesRows = explode(',', $productImagesRows);
                                    if (is_array($productImagesRows)) {
                                        $variantProduct->images()->delete();
                                        foreach ($productImagesRows as $productImagesRow) {
                                            if ($productImagesRow && $productImagesRow != $row['base_image']) {
                                                $variantProduct->images()->create(["url" => "https://mobilaty.com/media/catalog/product" . $productImagesRow]);
                                            }
                                        }
                                    }
                                }
                                $x = 1;
                                while ((isset($row['option_name_' . $x]) && !empty($row['option_name_' . $x]))) {
                                    $option = Option::where('name_en', trim($row['option_name_' . $x]))->first();

                                    if ($option) {
                                        if ($x == 1) {
                                            // make first option the default
                                            $mainProduct->update(["option_default_id" => $option->id]);
                                            $variantProduct->update(["option_default_id" => $option->id]);
                                        }

                                        $value = OptionValue::where('name_en', trim($row['option_value_' . $x]))->where('option_id', $option->id)->first();
                                        $variantOptionValueData = [
                                            'product_id' => $variantProduct->id,
                                            'option_id' => $option->id,
                                            'type' => '1'
                                        ];

                                        if ($option->type == 4 && $value) {
                                            $variantOptionValueData['value_id'] = $value ? $value->id : null;
                                            //    $image = isset($row['option_image_' . $x]) ? $row['option_image_' . $x] : '';
                                            //    if (filter_var($image, FILTER_VALIDATE_URL) === FALSE) {
                                            //        $image = str_replace(' ', '-', $image);
                                            //        $image = url("storage/uploads/" . $image);
                                            //    }
                                            $image = $productMainImage;
                                            $variantOptionValueData['image'] = isset($row['option_image_' . $x]) ? $image : '';
                                            ProductOptionValues::updateOrCreate(['product_id' => $variantProduct->id, 'option_id' => $option->id], $variantOptionValueData);
                                        } elseif ($value) {
                                            $variantOptionValueData['value_id'] = $value->id;
                                            ProductOptionValues::updateOrCreate(['product_id' => $variantProduct->id, 'option_id' => $option->id], $variantOptionValueData);
                                        } else {
                                            $totalMissedVariantsCount++;
                                            $missedDetails[] = 'variant with sku ' . $MainSku . '( value ' . $row['option_value_' . $x] . ' Not Found )';
                                        }

                                    } else {
                                        $totalMissedVariantsCount++;
                                        $missedDetails[] = 'variant with sku ' . $MainSku . '( option ' . $row['option_name_' . $x] . ' Not Found )';
                                    }
                                    $x++;
                                }

                            } elseif ($productLang == "sa") {
                                if (isset($output_array[0]) && is_array($output_array[0])) {
                                    // ProductOptionValues::where('product_id', $mainProduct->id)->where('type', 0)->delete();
                                    foreach ($output_array[0] as $attribute) {
                                        $optionValues = explode('=', $attribute, 2);
                                        $optionData = isset($optionValues[0]) ? $optionValues[0] : null;
                                        $valueData = isset($optionValues[1]) ? rtrim($optionValues[1], ",") : null;
                                        if ($optionData && $valueData) {
                                            $option = Option::where('name_en', $optionData)->where('type', 5)->first();
                                            if (!$option) {
                                                $option = Option::create(['name_en' => $optionData, 'name_ar' => $optionData, 'type' => 5,]);
                                            }
                                            $productOptionValue = ProductOptionValues::where('product_id', $mainProduct->id)->where('option_id', $option->id)->first();
                                            if ($productOptionValue) {
                                                $productOptionValue->update(['input_ar' => $valueData]);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        usleep(200);
                    }
                }

            });
            $totalApprovedProductsCount = $totalCreatedProductsCount + $totalUpdatedProductsCount;
            $totalApprovedVariantsCount = $totalCreatedVariantsCount + $totalUpdatedVariantsCount;
            $totalApprovedCount = $totalApprovedProductsCount + $totalApprovedVariantsCount;
            $totalMissedCount = $totalMissedProductsCount + $totalMissedVariantsCount;
            $totalCreatedCount = $totalCreatedProductsCount + $totalCreatedVariantsCount;
            $totalUpdatedCount = $totalUpdatedProductsCount + $totalUpdatedVariantsCount;
            $data = [
                'total_count' => $totalCount,
                'total_product_count' => count($mainProductIDS),
                'total_variant_product_count' => count($variantProductIDS),
                'total_approved_count' => $totalApprovedCount,
                'total_approved_products_count' => $totalApprovedProductsCount,
                'total_approved_variants_count' => $totalApprovedVariantsCount,
                'total_missed_count' => $totalMissedCount,
                'total_missed_products_count' => $totalMissedProductsCount,
                'total_missed_variants_count' => $totalMissedVariantsCount,
                'total_created_count' => $totalCreatedCount,
                'total_created_products_count' => $totalCreatedProductsCount,
                'total_created_variants_count' => $totalCreatedVariantsCount,
                'total_updated_count' => $totalUpdatedCount,
                'total_updated_products_count' => $totalUpdatedProductsCount,
                'total_updated_variants_count' => $totalUpdatedVariantsCount,
                'missed_details' => $missedDetails,
            ];
            Log::info("IMPORT FINISHED");
            Artisan::call('cache:clear');
            $this->pushService->notifyAdmins("File Imported", "Your import has completed! You can check the result from", '', 10, json_encode($data), \Auth::id());
            return $this->jsonResponse("Success", $data);
        } catch (\Exception $e) {
            // dd($e);
            Log::error("Import File Error: " . $e->getMessage());
            return $this->errorResponse($e->getMessage(), "Invalid data", [], 422);
        }
    }

    public function magentoImportAttributesAndImages(Request $request)
    {
        $this->validate($request, ["file" => "required", "approved" => "sometimes|nullable|boolean"]);
        isset($request->approved) ? $approved = $request->approved : $approved = 1;
        $totalCount = 0;
        $totalMainProductCount = 0;
        $totalVariantProductCount = 0;
        $totalMissedProductsCount = 0;
        $totalMissedVariantsCount = 0;
        $totalCreatedProductsCount = 0;
        $totalCreatedVariantsCount = 0;
        $totalUpdatedVariantsCount = 0;
        $totalUpdatedProductsCount = 0;
        $missedDetails = [];
        $mainSkuArray = [];
        $mainProductIDS = [];
        $variantProductIDS = [];
        Log::info("Starting Import Magento Attributes Sheet");
        try {
            //$file = $request->file('file')->getRealPath();
            Excel::load($request->file, function ($reader) use (&$variantProductIDS, &$mainProductIDS, &$missedDetails, &$totalMainProductCount, &$totalVariantProductCount, &$totalCount, &$approved, &$totalMissedProductsCount, &$totalMissedVariantsCount, &$totalCreatedProductsCount, &$totalCreatedVariantsCount, &$totalUpdatedProductsCount, &$totalUpdatedVariantsCount, &$mainSkuArray) {
                // Loop through all sheets
                $rows = $reader->all();
                if (!empty($rows)) {
                    foreach ($rows as $rowKey => $row) {
                        Log::info("Row: {$rowKey}");
                        $totalCount++;
                        if ($row['store_view_code'] == 'en') {
                            continue;
                        }

                        if (isset($row["sku"]) && is_float($row['sku'])) {
                            $sku = (int)$row['sku'];
                        } elseif (isset($row["sku"])) {
                            $sku = $row['sku'];
                        }

                        $productMainImage = isset($row['base_image']) && !empty($row['base_image']) ? "https://mobilaty.com/media/catalog/product" . $row['base_image'] : " ";
                        $productLang = isset($row['store_view_code']) && !empty($row['store_view_code']) ? $row['store_view_code'] : null;
                        $productImagesRows = isset($row['additional_images']) && !empty($row['additional_images']) ? $row['additional_images'] : null;
                        $productAttributes = isset($row['additional_attributes']) && !empty($row['additional_attributes']) ? $row['additional_attributes'] : null;
                        preg_match_all("/([a-zA-Z0-9_]+=(?:[<>\p{Arabic}a-zA-Z0-9×\s\-\–\/\;\'\"\:\%\.\&\@\~\+\*\(\)\=]|(,Ä≥)|(,,)|(?:,\s))+)/u", $productAttributes, $output_array);


                        if (isset($row['sku']) && $row['sku'] != null && isset($row['sku'])) {
                            $product = Product::where('sku', $row['sku'])->whereNotNull('parent_id')->first();

                            $mainProduct = $product ? $product->parent : null;
                            if (!$product || !$mainProduct) {
                                continue;
                            }
                            if (!$productLang) {
                                if (isset($output_array[0]) && is_array($output_array[0])) {
                                    ProductOptionValues::where('product_id', $mainProduct->id)->where('type', 0)->delete();
                                    foreach ($output_array[0] as $attribute) {
                                        $optionValues = explode('=', $attribute, 2);
                                        $optionData = isset($optionValues[0]) ? $optionValues[0] : null;
                                        $valueData = isset($optionValues[1]) ? rtrim($optionValues[1], ",") : null;


                                        if ($optionData == "mgs_brand") {
                                            $brand = Brand::firstOrCreate([
                                                'name' => $valueData
                                            ]);
                                            $mainProduct->update(['brand_id' => $brand->id]);
                                        }
                                        if ($optionData && $valueData) {
                                            $option = Option::where('name_en', $optionData)->where('type', 5)->first();
                                            if (!$option) {
                                                $option = Option::create(['name_en' => $optionData, 'name_ar' => $optionData, 'type' => 5,]);
                                            }
                                            $productOptionValue = ProductOptionValues::where('product_id', $mainProduct->id)->where('option_id', $option->id)->first();
                                            if ($productOptionValue) {
                                                $productOptionValue->update(['input_en' => $valueData, 'input_ar' => $valueData]);
                                            } else {
                                                ProductOptionValues::create(['product_id' => $mainProduct->id, 'option_id' => $option->id, 'input_en' => $valueData, 'input_ar' => $valueData, 'type' => "0"]);
                                            }
                                            CategoryOption::firstOrCreate([
                                                'option_id' => $option->id,
                                                'sub_category_id' => $mainProduct->category->id
                                            ]);
                                        }
                                    }
                                }
                                if ($productImagesRows) {
                                    $productImagesRows = explode(',', $productImagesRows);
                                    if (is_array($productImagesRows)) {
                                        $product->images()->delete();
                                        foreach ($productImagesRows as $productImagesRow) {
                                            if ($productImagesRow && $productImagesRow != $row['base_image']) {
                                                $product->images()->create(["url" => "https://mobilaty.com/media/catalog/product" . $productImagesRow]);
                                            }
                                        }
                                    }
                                }

                                $x = 1;
                                while ((isset($row['option_name_' . $x]) && !empty($row['option_name_' . $x]))) {
                                    $option = Option::where('name_en', trim($row['option_name_' . $x]))->first();

                                    if ($option) {
                                        if ($x == 1) {
                                            // make first option the default
                                            $mainProduct->update(["option_default_id" => $option->id]);
                                            $product->update(["option_default_id" => $option->id]);
                                        }

                                        $value = OptionValue::where('name_en', trim($row['option_value_' . $x]))->where('option_id', $option->id)->first();
                                        $variantOptionValueData = [
                                            'product_id' => $product->id,
                                            'option_id' => $option->id,
                                            'type' => '1'
                                        ];

                                        if ($option->type == 4 && $value) {
                                            $variantOptionValueData['value_id'] = $value ? $value->id : null;
                                            //    $image = isset($row['option_image_' . $x]) ? $row['option_image_' . $x] : '';
                                            //    if (filter_var($image, FILTER_VALIDATE_URL) === FALSE) {
                                            //        $image = str_replace(' ', '-', $image);
                                            //        $image = url("storage/uploads/" . $image);
                                            //    }
                                            $image = $productMainImage;
                                            $variantOptionValueData['image'] = isset($row['option_image_' . $x]) ? $image : '';
                                            ProductOptionValues::updateOrCreate(['product_id' => $product->id, 'option_id' => $option->id], $variantOptionValueData);
                                        } elseif ($value) {
                                            $variantOptionValueData['value_id'] = $value->id;
                                            ProductOptionValues::updateOrCreate(['product_id' => $product->id, 'option_id' => $option->id], $variantOptionValueData);
                                        } else {
                                            $totalMissedVariantsCount++;
                                            $missedDetails[] = 'variant with sku ' . $sku . '( value ' . $row['option_value_' . $x] . ' Not Found )';
                                        }

                                    } else {
                                        $totalMissedVariantsCount++;
                                        $missedDetails[] = 'variant with sku ' . $sku . '( option ' . $row['option_name_' . $x] . ' Not Found )';
                                    }
                                    $x++;
                                }

                            } elseif ($productLang == "sa") {
                                if (isset($output_array[0]) && is_array($output_array[0])) {
                                    // ProductOptionValues::where('product_id', $mainProduct->id)->where('type', 0)->delete();
                                    foreach ($output_array[0] as $attribute) {
                                        $optionValues = explode('=', $attribute, 2);
                                        $optionData = isset($optionValues[0]) ? $optionValues[0] : null;
                                        $valueData = isset($optionValues[1]) ? rtrim($optionValues[1], ",") : null;
                                        if ($optionData && $valueData) {
                                            $option = Option::where('name_en', $optionData)->where('type', 5)->first();
                                            if (!$option) {
                                                $option = Option::create(['name_en' => $optionData, 'name_ar' => $optionData, 'type' => 5,]);
                                            }
                                            $productOptionValue = ProductOptionValues::where('product_id', $mainProduct->id)->where('option_id', $option->id)->first();
                                            if ($productOptionValue) {
                                                $productOptionValue->update(['input_ar' => $valueData]);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        usleep(200);
                    }
                }

            });
            $totalApprovedProductsCount = $totalCreatedProductsCount + $totalUpdatedProductsCount;
            $totalApprovedVariantsCount = $totalCreatedVariantsCount + $totalUpdatedVariantsCount;
            $totalApprovedCount = $totalApprovedProductsCount + $totalApprovedVariantsCount;
            $totalMissedCount = $totalMissedProductsCount + $totalMissedVariantsCount;
            $totalCreatedCount = $totalCreatedProductsCount + $totalCreatedVariantsCount;
            $totalUpdatedCount = $totalUpdatedProductsCount + $totalUpdatedVariantsCount;
            $data = [
                'total_count' => $totalCount,
//                'total_product_count' => count($mainProductIDS),
//                'total_variant_product_count' => count($variantProductIDS),
//                'total_approved_count' => $totalApprovedCount,
//                'total_approved_products_count' => $totalApprovedProductsCount,
//                'total_approved_variants_count' => $totalApprovedVariantsCount,
//                'total_missed_count' => $totalMissedCount,
//                'total_missed_products_count' => $totalMissedProductsCount,
//                'total_missed_variants_count' => $totalMissedVariantsCount,
//                'total_created_count' => $totalCreatedCount,
//                'total_created_products_count' => $totalCreatedProductsCount,
//                'total_created_variants_count' => $totalCreatedVariantsCount,
//                'total_updated_count' => $totalUpdatedCount,
//                'total_updated_products_count' => $totalUpdatedProductsCount,
//                'total_updated_variants_count' => $totalUpdatedVariantsCount,
//                'missed_details' => $missedDetails,
            ];
            Log::info("IMPORT FINISHED");
            Artisan::call('sitemap:generate');
            Artisan::call('cache:clear');
            $this->pushService->notifyAdmins("File Imported", "Your import has completed! You can check the result from", '', 10, json_encode($data), \Auth::id());
            return $this->jsonResponse("Success", $data);
        } catch (\Exception $e) {
            // dd($e);
            Log::error("Import File Error: " . $e->getMessage());
            return $this->errorResponse($e->getMessage(), "Invalid data", $e->getTrace(), 422);
        }
    }

    public function clone($id)
    {
        $product = Product::findOrFail($id);
        $clonedProduct = $this->productsService->cloneProcut($product);
        return $this->jsonResponse("Success", $this->productTrans->transform($clonedProduct));
    }

    public function delete($id)
    {
        Product::find($id)->delete();
        return $this->jsonResponse("Success");
    }
}
