<?php

namespace App\Http\Controllers\Admin;

use App\Http\Resources\Admin\CategoryGroupsResource;
use App\Models\Notifications\PushMessage;
use App\Http\Controllers\Controller;
use App\Models\Products\Category;
use App\Models\Products\Group;
use App\Models\Products\ProductReview;
use App\Models\Products\SubCategoryGroup;
use App\Models\Services\PushService;
use App\Models\Users\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class CategoryGroupsController extends Controller
{

    public function index()
    {
        $group = Group::get();
        return $this->jsonResponse("Success", CategoryGroupsResource::collection($group));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), Group::$validation);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }
        $data = $validator->valid();
        $group = Group::create($data);
        if (isset($data['sub_categories']) && is_array($data['sub_categories'])) {
            foreach ($data['sub_categories'] as $sub_category) {
                $category = Category::find($sub_category);
                if ($category->parent_id != null) {
                    $groupData = [
                        'group_id' => $group->id,
                        'category_id' => $category->parent->id,
                        'sub_category_id' => $category->id,
                    ];
                    SubCategoryGroup::create($groupData);
                }
            }
        }
        return $this->jsonResponse("Success", new CategoryGroupsResource($group->refresh()));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), Group::$validation);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }
        $data = $validator->valid();
        $group = Group::find($id);
        if (!$group) {
            return $this->jsonResponse("Group Not Found", $group);
        }
        $group->update($data);
        SubCategoryGroup::where('group_id', $group->id)->delete();
        if (isset($data['sub_categories']) && is_array($data['sub_categories'])) {
            foreach ($data['sub_categories'] as $sub_category) {
                $category = Category::find($sub_category);
                if ($category->parent_id != null) {
                    $groupData = [
                        'group_id' => $group->id,
                        'category_id' => $category->parent->id,
                        'sub_category_id' => $category->id,
                    ];
                    SubCategoryGroup::updateOrCreate($groupData);
                }
            }
        }
        return $this->jsonResponse("Success", new CategoryGroupsResource($group->refresh()));
    }

    public function activate($id)
    {
        $group = Group::findOrFail($id);
        $group->active = 1;
        $group->save();
        return $this->jsonResponse("Success");
    }

    public function deactivate(Request $request, $id)
    {
        $group = Group::findOrFail($id);
        $group->active = 0;
        $group->save();
        return $this->jsonResponse("Success");
    }

    public function export()
    {
        $items = Group::with('sub_categories')->get();

        return Excel::create('Groups_' . date("Ymd"), function ($excel) use ($items) {
            $excel->sheet('report', function ($sheet) use ($items) {

                $sheet->row(1, [
                    "group",
                    "group_ar",
                    "group_image",
                    "category",
                    "subcategory",
                    "subcategory_slug",
                    "group_slug"
                ]);

                foreach ($items as $key => $item) {
                    if (count($item['sub_categories']) > 0) {
                        $i = 0;
                        $subCategoryData = [];
                        foreach ($item['sub_categories'] as $subCategory) {
                            $array = [
                                $i == 0 ? $item->name_en : '',
                                $i == 0 ? $item->name_ar : '',
                                $i == 0 ? $item->image : '',
                                $subCategory->parent->name,
                                $subCategory->name,
                                $subCategory->slug,
                                $i == 0 ? $item->slug : '',
                            ];
                            $subCategoryData[] = $array;
                            $i++;
                        }
                        $data = $subCategoryData;
                    } else {
                        $data = [
                            $item->name_en,
                            $item->name_ar,
                            $item->image,
                            '',
                            '',
                            '',
                            $item->slug,
                        ];
                    }
                    $sheet->rows($data);
                }
            });
        })->download('xlsx');
    }

    public function import(Request $request)
    {
        $this->validate($request, ["file" => "required"]);
        try {
            Excel::load($request->file, function ($reader) {
                // Loop through all sheets
                $results = $reader->all();
                $parentCategory = null;
                foreach ($results as $key => $row) {
                    $groupImage = isset($row['group_image']) ? $row['group_image'] : null;
                    $subcategory_slug = isset($row['subcategory_slug']) ? $row['subcategory_slug'] : null;
                    $group_slug = isset($row['group_slug']) ? $row['group_slug'] : null;
                    if (filter_var($groupImage, FILTER_VALIDATE_URL) === FALSE) {
                        $groupImage = str_replace(' ', '-', $groupImage);
                        $groupImage = url("storage/uploads/" . $groupImage);
                    }
                    if ($row['group'] != null || $row['group_ar'] != null) {
                        if ($group_slug) {
                            $item = Group::where("slug", $group_slug)->first();
                        } else {
                            $item = Group::where("name_en", $row['group'])->first();
                        }
                        if ($item) {
                            $item->update(["name_en" => $row['group'], "name_ar" => $row['group_ar'], "image" => $groupImage,"slug"=>$group_slug]);
                            if ($row['category'] != null || $row['subcategory'] != null) {
                                if ($subcategory_slug) {
                                    $subCategory = Category::where('slug', $subcategory_slug)->where('parent_id', '!=', null)->first();
                                } else {
                                    $subCategory = Category::where("name", $row['subcategory'])->where('parent_id', '!=', null)->first();
                                }
                                if ($subCategory) {
                                    SubCategoryGroup::firstOrCreate(['sub_category_id' => $subCategory->id, 'category_id' => $subCategory->parent->id, 'group_id' => $item->id,]);
                                }
                            }
                        } else {
                            $item = Group::create(["name_en" => $row['group'], 'name_ar' => $row['group_ar'], "image" => $groupImage,"slug"=>$group_slug]);
                            if ($row['category'] != null || $row['subcategory'] != null) {
                                if ($subcategory_slug) {
                                    $subCategory = Category::where('slug', $subcategory_slug)->where('parent_id', '!=', null)->first();
                                } else {
                                    $subCategory = Category::where("name", $row['subcategory'])->where('parent_id', '!=', null)->first();
                                }
                                if ($subCategory) {
                                    SubCategoryGroup::firstOrCreate(['sub_category_id' => $subCategory->id, 'category_id' => $subCategory->parent->id, 'group_id' => $item->id,]);
                                }
                            }
                        }
                        $parentCategory = $item;
                    } else {
                        if ($parentCategory) {
                            if ($row['category'] != null || $row['subcategory'] != null) {
                                if ($subcategory_slug) {
                                    $subCategory = Category::where('slug', $subcategory_slug)->where('parent_id', '!=', null)->first();
                                } else {
                                    $subCategory = Category::where("name", $row['subcategory'])->where('parent_id', '!=', null)->first();
                                }
                                if ($subCategory) {
                                    SubCategoryGroup::firstOrCreate(['sub_category_id' => $subCategory->id, 'category_id' => $subCategory->parent->id, 'group_id' => $parentCategory->id,]);
                                }
                            }
                        }
                    }

                }
            });
        } catch (\Exception $e) {
            Log::error("HandleFileError: " . $e->getMessage());
        }

        return $this->jsonResponse("Success", null);
// return response
    }
}
