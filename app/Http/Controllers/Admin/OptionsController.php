<?php

namespace App\Http\Controllers\Admin;


use App\Models\Products\Option;
use App\Models\Products\OptionValue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class OptionsController extends Controller
{

    public function index(Request $request)
    {
        if ($request->q) {
            $options = Option::where("name_en", "LIKE", "%{$request->q}%")
                ->orWhere("name_ar", "LIKE", "%{$request->q}%")
                ->orWhere("description_en", "LIKE", "%{$request->q}%")
                ->orWhere("description_ar", "LIKE", "%{$request->q}%")
                ->orWhere("id", $request->q)
                ->with('values')
                ->orderBy('created_at', 'DESC');
        }else{
            $options = Option::with('values')->orderBy('created_at', 'DESC');
        }
        $message = 'Success';
        if ($request->page == 0){
            $options = $options->get();
            return $this->jsonResponse($message, $options);
        }else{
            $options = $options->paginate(20);
            return $this->jsonResponse($message, ['options' =>$options->items() , 'total'=>$options->total()]);


        }


    }

    public function store(Request $request)
    {
        // validate request
        $validator = Validator::make($request->all(), Option::$validation + []);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }
        $data = $validator->valid();
        $data['active'] = 1;
        $data['created_by'] = \Auth::id();
        $option = Option::create($data);

        if ($request->values && is_array($request->values)) {
            foreach ($request->values as $value) {
                $data = [
                    'name_en' => $value['name_en'],
                    'name_ar' => $value['name_ar'],
                    'option_id' => $option->id,
                    'created_by' => \Auth::id()
                ];
                if ($option->type == 2 || $option->type == 4){
                    $data['color_code'] = $value['color_code'];
                }elseif($option->type == 3){
                    $data['image'] = $value['image'];
                }
                OptionValue::create($data);
            }
        }
        $option->push($option->values);
        return $this->jsonResponse("Success", $option);
    }

    public function show($id)
    {
        $options = Option::find($id);
        if ($options) {
            $options->push($options->values);
            $message = 'Success';
        } else {
            $options =[];
            $message = 'Not Found';
        }

        return $this->jsonResponse($message, $options);
    }

    public function update($id, Request $request)
    {
        $option = Option::find($id);
        if (!$option) {
            $message = 'This option Not Found';
            $option =[];
            return $this->jsonResponse($message, $option);
        }
        // validate request
        $validator = Validator::make($request->all(), Option::$validation + []);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }
        $data = $validator->valid();
        $option->update($data);

        if ($request->values && is_array($request->values)) {
            foreach ($request->values as $value) {
                $data = [
                    'name_en' => $value['name_en'],
                    'name_ar' => $value['name_ar'],
                    'option_id' => $option->id,
                    'created_by' => \Auth::id()
                ];
                if ($option->type == 2 || $option->type == 4){
                    $data['color_code'] = $value['color_code'];
                }elseif($option->type == 3){
                    $data['image'] = $value['image'];
                }
                if (isset($value['id'])){
                    $optionValue =  OptionValue::find($value['id']);
                    $optionValue->update($data);
                }else{
                    OptionValue::create($data);
                }
            }
        }
        $option->push($option->values);
        return $this->jsonResponse("Success", $option);
    }

    public function activate($id)
    {
        $options = Option::find($id);
        if (!$options) {
            $message = 'This option Not Found';
            $options =[];
            return $this->jsonResponse($message, $options);
        }
        $data = ['active' => '1'];
        $options->update($data);
        return $this->jsonResponse("Success");
    }

    public function deactivate(Request $request, $id)
    {
        $options = Option::find($id);
        if (!$options) {
            $message = 'This option Not Found';
            $options =[];
            return $this->jsonResponse($message, $options);
        }
        $data = ['active' => '0'];
        $options->update($data);
        return $this->jsonResponse("Success");
    }

    public function destroy($id)
    {
        $options = Option::find($id);
        if (!$options) {
            $message = 'This option Not Found';
            $options =[];
            return $this->jsonResponse($message, $options);
        }

        $options->delete();
        return $this->jsonResponse("Success");
    }

    public function export()
    {
        $items = Option::with('values')->get();


        return Excel::create('Options_' . date("Ymd"), function ($excel) use ($items) {
            $excel->sheet('report', function ($sheet) use ($items) {

                $sheet->row(1, [
                    "option",
                    "option_ar",
                    "type",
                    "image",
                    "hex_code",
                    "values",
                    "values_ar",
                ]);

                foreach ($items as $key => $item) {
                    $type = '';
                    if ($item->type == 1) {
                        $type = 'text';
                    } elseif ($item->type == 2) {
                        $type = 'color code';
                    } elseif ($item->type == 3) {
                        $type = 'image';
                    } elseif ($item->type == 4) {
                        $type = 'variant image';
                    } elseif ($item->type == 5) {
                        $type = 'input';
                    }
                    if (count($item['values']) > 0) {
                        $i = 0;
                        $values = [];
                        foreach ($item['values'] as $value) {
                            $array = [
                                $i == 0 ? $item->name_en : '',
                                $i == 0 ? $item->name_ar : '',
                                $i == 0 ? $type : '',
                                $value->image,
                                $value->color_code,
                                $value->name_en,
                                $value->name_ar,
                            ];
                            $values[] = $array;
                            $i++;
                        }
                        $data = $values;
                    } else {
                        $data = [
                            $item->name_en,
                            $item->name_ar,
                            $type,
                            $item->image,
                            $item->color_code,
                            '',
                            '',
                        ];
                    }
                    $sheet->rows($data);
                }
            });
        })->download('xlsx');
    }

    public function import(Request $request)
    {
        $this->validate($request, ["file" => "required"]);
        try {
            Excel::load($request->file, function ($reader) {
                // Loop through all sheets

                $results = $reader->all();
                $optionData = null;
                foreach ($results as $key => $row) {
                    $type = 1;
                    if (isset($row['type'])){
                        if ($row['type'] == 'text') {
                            $type = 1;
                        } elseif ($row['type'] == 'color code') {
                            $type = 2;
                        } elseif ($row['type'] == 'image') {
                            $type = 3;
                        } elseif ($row['type'] == 'variant image') {
                            $type = 4;
                        } elseif ($row['type'] == 'input') {
                            $type = 5;
                        }
                    }

                    $color_code = isset($row['hex_code']) ? $row['hex_code'] : '';
                    $image = isset($row['image']) ? $row['image'] : '';
                    $data = [];
                    $valData = [];
                    $data = [
                        "name_en" => isset($row['option']) ? $row['option'] : '',
                        "name_ar" => isset($row['option_ar']) ? $row['option_ar'] : '',
                        "type" => $type,
                    ];
                    $valData = [
                        "name_en" => isset($row['values']) ? $row['values'] : '',
                        "name_ar" => isset($row['values_ar']) ? $row['values_ar'] : '',
                    ];
                    if ($row['option'] != null || $row['option_ar'] != null) {
                        $option = Option::where("name_en", $row['option'])->first();
                        if ($option) {
                            $option->update($data);
                            if ($row['values'] != null || $row['values_ar'] != null) {
                                $optionValue = OptionValue::where("name_en", $row['values'])->where('option_id', $option->id)->first();
                                if ($optionValue) {
                                    ( $option->type == 2  || $option->type == 4 )? $valData['color_code'] = $color_code : $valData['color_code'] = '';
                                    $option->type == 3 ? $valData['image'] = $image : $valData['image'] = '';
                                    $optionValue->update($valData);
                                } else {
                                    ($option->type == 2 || $option->type == 4)? $valData['color_code'] = $color_code : $valData['color_code'] = '';
                                    $option->type == 3 ? $valData['image'] = $image : $valData['image'] = '';
                                    $valData['option_id'] = $option->id;
                                    OptionValue::create($valData);
                                }
                            }
                        } else {
                            if ($row['values'] != null || $row['values_ar'] != null) {
                                $item = Option::where("name_en", $row['option'])->first();
                                if ($item) {
                                   ($optionData->type == 2 || $optionData->type == 4)  ? $valData['color_code'] = $color_code : $valData['color_code'] = '';
                                    $optionData->type == 3 ? $valData['image'] = $image : $valData['image'] = '';
                                    $valData['option_id'] = $option->id;
                                    OptionValue::create($valData);
                                } else {
                                    $option = Option::create($data);
                                }
                               ($optionData->type == 2 || $optionData->type == 4)  ? $valData['color_code'] = $color_code : $valData['color_code'] = '';
                                $optionData->type == 3 ? $valData['image'] = $image : $valData['image'] = '';
                                $valData['option_id'] = $option->id;
                                OptionValue::create($valData);
                            } else {
                                $option = Option::create($data);
                            }
                        }
                        $optionData = $option;
                    } else {
                        if ($optionData) {
                            $optionValue = OptionValue::where("name_en", $row['values'])->where('option_id', $optionData->id)->first();
                            if ($optionValue) {
                                ($optionData->type == 2 || $optionData->type == 4) ? $valData['color_code'] = $color_code : $valData['color_code'] = '';
                                $optionData->type == 3 ? $valData['image'] = $image : $valData['image'] = '';
                                $optionValue->update($valData);
                            } else {
                                ($optionData->type == 2 || $optionData->type == 4)? $valData['color_code'] = $color_code : $valData['color_code'] = '';
                                $optionData->type == 3 ? $valData['image'] = $image : $valData['image'] = '';
                                $valData['option_id'] = $optionData->id;
                                OptionValue::create($valData);
                            }
                        }
                    }
                }
            });
        } catch (\Exception $e) {

            Log::error("HandleFileError: " . $e->getMessage());
        }
        return $this->jsonResponse("Success", null);
        // return response
    }


}
