<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\Users\User;
use App\Models\Orders\Order;
use Illuminate\Http\Request;
use App\Models\Shipping\Pickup;
use App\Models\Users\ContactUs;
use App\Models\Orders\OrderState;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Models\Services\AramexService;
use App\Http\Resources\Admin\ShipmentResource;

class ShipmentsController extends Controller
{
    private $aramexService;

    public function __construct(AramexService $aramexService)
    {
        $this->aramexService = $aramexService;
    }

    public function getAvailablePickups()
    {
        $pickups = Pickup::where("status", "!=", 3)->where("pickup_time", ">", now())->get();

        return $this->jsonResponse("Success", $pickups);
    }

    public function createPickUp(Request $request, $provider)
    {
        $this->validate($request, [
            "order_ids" => "required|array|filled",
            "branch_id" => "required|integer|exists:users,id",
            "aramex_account_number" => "required|integer|in:1,2",
            "pickup_date" => "required|date|after:now",
            "shipping_notes" => "sometimes|nullable",
        ]);

        switch ($provider) {
            case 'Aramex':
                $pickup = $this->aramexService->CreatePickup($request->order_ids, Carbon::parse($request->pickup_date)->format("Y-m-d H:i:s"), $request->branch_id, $request->shipping_notes, $request->aramex_account_number);
        }
        if ($pickup === true) {
            return $this->jsonResponse("Success");
        } else {
            return $this->errorResponse($pickup, "Invalid data", $pickup, 422);
        }
    }

    public function createShipment(Request $request, $provider)
    {
        $this->validate($request, [
            "order_id" => "required|integer|exists:orders,id",
            "branch_id" => "required|integer|exists:users,id",
            "aramex_account_number" => "required|integer|in:1,2",
            // "pickup_date" => "required|date|after:now",
            "shipping_notes" => "sometimes|nullable",
            "pickup_guid" => "sometimes|nullable"
        ]);
        $branch = User::where('id', $request->branch_id)->where('type', 3)->firstOrFail();
        $order = Order::findOrFail($request->order_id);
        // switch ($provider) {
        //     case 'Aramex':
        //         $shipment = $this->aramexService->CreateShipment($request, $branch, $order);
        // }

        try {
            $shipment = $this->aramexService->CreateShipment($request, $branch, $order);
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage(), "Invalid data", $e->getTrace(), 422);
        }

        // if ($shipment) {
        //     return $this->jsonResponse("Success");
        // } else {
        //     return $this->errorResponse($shipment[0], "Invalid data", $shipment, 422);
        // }

        return $this->jsonResponse("Success", new ShipmentResource($shipment));
    }

    public function calculateRate(Request $request, $provider)
    {
        switch ($provider) {
            case 'Aramex':
                $this->aramexService->CalculateRate($request);
        }

    }

}
