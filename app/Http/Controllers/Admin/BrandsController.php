<?php

namespace App\Http\Controllers\Admin;

use App\Models\Products\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class BrandsController extends Controller
{

    public function index()
    {
        return $this->jsonResponse("Success", Brand::all());
    }

    public function show($id)
    {
        $brand = Brand::find($id);
        return $this->jsonResponse("Success", $brand);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "name" => "required",
            "name_ar" => "required",
            "image" => "required",
            "status" => 'sometimes|nullable'
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }
        $data = $validator->valid();
        $brand = Brand::create($data);
        Cache::forget('brands');
        return $this->jsonResponse("Success", $brand);
    }

    public function update(Request $request, $id)
    {
        $brand = Brand::findOrFail($id);

        $validator = Validator::make($request->all(), [
            "name" => "required",
            "name_ar" => "required",
            "image" => "required",
            "status" => 'sometimes|nullable'
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors()->first(), "Invalid data", $validator->errors(), 422);
        }
        $data = $validator->valid();
        $brand->update($data);
        Cache::forget('brands');
        return $this->jsonResponse("Success", $brand);
    }

    public function import(Request $request)
    {
        $this->validate($request, ["file" => "required"]);

        try {
            Excel::load($request->file, function ($reader) {
                $results = $reader->all();
                if (!empty($results)) {
                    foreach ($results as $row) {
                        $brand = Brand::where("name",$row['brand'])->orWhere('name_ar',$row['brand_ar'])->first();
                        if ($brand){
                            $brand->update(["name" => $row['brand'], "name_ar" => $row['brand_ar'], "image" => $row['image']]);
                        }else{
                            Brand::create(["name" => $row['brand'], "name_ar" => $row['brand_ar'], "image" => $row['image']]);
                        }
                    }
                }
            });
        } catch (\Exception $e) {
            Log::error("Import File Error: " . $e->getMessage());
            return $this->errorResponse($e->getMessage(), "Invalid data", [], 422);
        }

        // return response
        return $this->jsonResponse("Success");
    }
}
