<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\Locations\Area;
use App\Models\Locations\City;
use App\Models\Locations\District;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AreasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($city_id)
    {
        $areas = Area::with("city", "districts")->where("city_id", $city_id)->get();

        return $this->jsonResponse("Success", $areas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $city_id)
    {
        // validate request
        $this->validate($request, [
            "name" => "required",
            "name_ar" => "required",
            "delivery_fees" => "required"
        ], ["name.unique" => "An area already exist with the same name"]);

        $city = City::findOrFail($city_id);
        $request->merge(["active" => 1]);
        $area = $city->areas()->create($request->only(["name", "name_ar", "city_id", "delivery_fees", "active"]));
        $area->refresh();

        return $this->jsonResponse("Success", $area->load("city", "districts"));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($city_id, $id)
    {
        $area = Area::findOrFail($id);

        return $this->jsonResponse("Success", $area->load("city", "districts"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $city_id, $id)
    {
        $area = Area::findOrFail($id);

        // validate request
        $this->validate($request, [
            "name" => "required",
            "name_ar" => "required",
            "delivery_fees" =>  "required"
        ], ["name.unique" => "An area already exist with the same name"]);

        $area->update($request->only(["name", "name_ar", "delivery_fees"]));

        return $this->jsonResponse("Success", $area->load("city", "districts"));
    }

    public function activate($city_id, $id)
    {
        $area = Area::findOrFail($id);

        $area->active = 1;
        $area->deactivation_notes = null;
        $area->save();

        return $this->jsonResponse("Success");
    }

    public function deactivate(Request $request, $city_id, $id)
    {
        // validate request
        $this->validate($request, ["deactivation_notes" => "required"]);

        $area = Area::findOrFail($id);

        $area->active = 0;
        $area->deactivation_notes = $request->deactivation_notes;
        $area->save();

        return $this->jsonResponse("Success");
    }

    public function updateDeliveryFees(Request $request)
    {
        $this->validate($request, [
            "area_ids" => "required|array",
            "delivery_fees" => "required"
        ]);

        Area::whereIn("id", $request->area_ids)->update(["delivery_fees" => $request->delivery_fees]);

        if ($request->cascade) {
            District::whereIn("area_id", $request->area_ids)->update(["delivery_fees" => $request->delivery_fees]);
        }

        return $this->jsonResponse("Success");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $area = Area::findOrFail($id);

        $area->delete();

        return $this->jsonResponse("Success");
    }
}
