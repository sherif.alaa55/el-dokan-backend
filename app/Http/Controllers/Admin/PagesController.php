<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Pages\Page;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index()
    {
        $pages = Page::all();
        return $this->jsonResponse('success', $pages);
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            "slug" => "required|unique:pages,slug",
            "title_en" => "required",
            "title_ar" => "required",
            "content_en" => "required",
            "content_ar" => "required",
            "image_ar" => "required",
            "image_en" => "required"
        ]);
        $page = Page::create($data);
        $page->refresh();
        return $this->jsonResponse('success', $page);
    }

    public function update(Request $request, $id)
    {
        $page = Page::findOrFail($id);
        $data = $request->validate([
            "slug" => "required|unique:pages,slug",
            "title_en" => "required",
            "title_ar" => "required",
            "content_en" => "required",
            "content_ar" => "required",
            "image_ar" => "required",
            "image_en" => "required"
        ]);
        $page->update($data);
        return $this->jsonResponse('success', $page);
    }

    public function activate($id)
    {
        $page = Page::find($id);
        $page->update(['active' => 1]);
        return $this->jsonResponse('success', $page);
    }

    public function deactivate($id)
    {
        $page = Page::find($id);
        $page->update([
            'active' => 0,
        ]);
        return $this->jsonResponse('success', $page);
    }

}
