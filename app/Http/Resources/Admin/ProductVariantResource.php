<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductVariantResource extends JsonResource
{
    public function toArray($request)
    {
        $options = $this->customerVariantOptionsLists($request->header('lang'));
        $value_ids = array_map(function ($item)
        {
            if (isset($item["values"][0]["id"])) {
                return $item["values"][0]["id"];
            }
        }, $options);

        $cartRepo = app()->make("App\Models\Repositories\CartRepository");
        
        if (!$this->amount && 999999 !== auth()->id()) {
            $cartItems = $cartRepo->getUserCartItems();
            foreach ($cartItems as $item) {
                if ($item['id'] == $this->id) {
                    $this->amount = $item['amount'];
                }
            }
        }


        return [
            "id" => $this->id,
            "name" => $this->getName($request->header("lang")),
            "name_en" => $this->name,
            "name_ar" => $this->name_ar,
            "description" => $this->getDescription($request->header("lang")),
            "long_description" => $this->getLongDescription($request->header("lang")),
            "sku" => $this->sku,
            "price" => $this->price,
            "discount_price" => $this->discount_price,
            "stock" => $this->stock,
            "stock_alert" => $this->stock_alert,
            "options" => $this->customerVariantOptionsLists($request->header('lang')),
            "default_variant" => $this->default_variant,
            "order" => $this->order,
            "video" => $this->video,
            "details" => $this->getDetails($request),
            "is_favourite" => $this->is_favourite,
            "is_compare" => $this->is_compare,
            "available_soon" => (boolean)$this->available_soon,
            "preorder" => (boolean)$this->parent->preorder,
            "preorder_price" => $this->preorder_price,
            "discount_start_date" => $this->discount_start_date,
            "discount_end_date" => $this->discount_end_date,
            // "variant_image" => $this->defaultOptionValue ? $this->defaultOptionValue->image : null,
            "value_ids" => $value_ids,
            "image" => $this->image,
            "parent_id" => $this->parent_id,
            // "bundleProduct" => ProductVariantResource::collection($this->bundleProduct),
            "images" => $this->getDecodedImages(),
            "in_stock" => $this->stock > 0,
            "amount" => $this->amount
        ];
    }

    public function getDetails($request)
    {
        $details = [
            "description" => $this->getDescription($request->header("lang")),
            "brand" => $this->brand,
            "attributes" => $this->customerAttributesWithVlues($this->brand),
        ];
        if(isset($this->is_favourite)){
            $details["is_favourite"] = $this->is_favourite;
        }
        return $details;
    }
}
