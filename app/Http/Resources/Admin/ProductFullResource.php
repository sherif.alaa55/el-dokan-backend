<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductFullResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "name_ar" => $this->name_ar,
            "description" => $this->description,
            "description_ar" => $this->description_ar,
            "long_description_ar" => $this->long_description_ar,
            "long_description_en" => $this->long_description_en,
            "image" => $this->image,
            "brand_id" => $this->brand_id,
            "images" => $this->images,
            "price" => $this->price,
            "creator" => $this->creator,
            "created_at" => (string)$this->created_at,
            "discount_price" => $this->discount_price,
            "category" => $this->getCategoryTree(),
            "category_id" => $this->category_id,
            "tags" => $this->tags,
            "options" => $this->optionsWithVlues(),
            "active" => $this->active,
            "deactivation_notes" => $this->deactivation_notes,
            "sku" => $this->sku,
            "stock" => $this->stock,
            "stock_alert" => $this->stock_alert,
            "rateAVG" => $this->rate,
            "rates" => $this->allRates,
        ];
    }
}