<?php

namespace App\Http\Resources\Admin;

use App\Models\Products\Product;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        {
            $price_instance = $this->price;
            $product = Product::find($this->product->id);

            if ($product->parent_id != null){
                return [
                    "id" => $this->product_id,
                    "product" => $product,
                    "amount" => $this->amount,
                    "sub_category" => $product->parent->category->name,
                    "category" => $product->parent->category->parent->name,
                    "brand" => isset($product->parent->brand) ? $product->parent->brand->name : '',
                    "image" => $this->product->image,
                    "price" => $this->discount_price ?: $this->price,
                    "returned_quantity" => $this->returned_quantity,
                    "options" => $product->customerVariantOptionsLists($request->header('lang')),
                ];
            }else{
                return [
                    "id" => $this->product_id,
                    "product" => $this->product,
                    "amount" => $this->amount,
                    "sub_category" => $this->product->category->name,
                    "category" => $this->product->category->parent->name,
                    "brand" => isset($this->product->brand) ?  $this->product->brand->name : '',
                    "image" => $this->product->image,
                    "price" => $this->discount_price ?: $this->price,
                    "returned_quantity" => $this->returned_quantity,
                    "options" => $product->customerVariantOptionsLists($request->header('lang')),
                ];
            }
        }
    }
}
