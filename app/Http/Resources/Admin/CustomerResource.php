<?php

namespace App\Http\Resources\Admin;

use App\Models\Services\LoyalityService;
use App\Models\Settings\Settings;
use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    public function toArray($request)
    {
        $settings = Settings::getSystemSettings();

        return [
            "id" => $this->id,
            "name" => $this->name,
            "last_name" => $this->last_name,
            "email" => $this->email,
            "phone" => $this->phone,
            "birthdate" => $this->birthdate,
            "token" => $this->token,
            "rate" => $this->rating,
            "addresses" => $this->addresses->load("area", "city", "district"),
            "total_points" => $this->getCurrentPoints(),
            "is_gold" => $this->isGold(),
            "spent" => $this->spent,
            "gold_amount" => $settings->egp_gold,
            "points_to_gold" => $this->pointsToGold(),
            "points_to_expire" => $this->pointsToExpire(),
            "referal" => $this->referal,
            "primary_address" => $this->primaryAddress ?? $this->addresses->first(),
            "next_expiration_date" => (string)LoyalityService::nextExpirationDate()->format('Y-m-d'),
            "ex_rate_pts" => $settings->ex_rate_pts,
            "ex_rate_egp" => $settings->ex_rate_egp,
            "ex_rate_gold" => $settings->ex_rate_gold,
            "refer_points" => $settings->refer_points,
            "pending_days" => $settings->pending_days
        ];
    }
}
