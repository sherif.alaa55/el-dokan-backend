<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Payment\PaymentMethod;
use App\Http\Resources\Admin\PaymentMethodsResource;
class OrderResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "user" => $this->customer->load("addresses"),
            "notes" => $this->notes,
            "admin_notes" => $this->admin_notes,
            "state_id" => $this->state_id,
            "state" => $this->state,
            "sub_state" => $this->sub_state,
            "sub_state_id" => $this->sub_state_id,
            "paid_amount" => $this->invoice->paid_amount,
            "amount" => !is_null($this->invoice->discount) ?$this->invoice->discount: $this->invoice->total_amount,
            "delivery_fees" => $this->invoice->delivery_fees,
            "remaining" => $this->invoice->remaining ?: 0,
            "payment_method" => $this->payment_method,
            "payment_method_object" => new PaymentMethodsResource(PaymentMethod::find($this->payment_method)),
            "created_at" => (string)$this->created_at,
            "scheduled_at" => (string)$this->scheduled_at,
            "address" => $this->address,
            "items" => OrderItemResource::collection($this->items),
            "schedule" => $this->schedule ? $this->schedule->load("days") : null,
            "parent_id" => $this->parent_id,
            "transaction_id" => $this->transaction_id,
            "reorder_count" => $this->reorders()->count(),
            "user_agent" => $this->user_agent
        ];
    }
}
