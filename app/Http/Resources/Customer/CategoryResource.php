<?php

namespace App\Http\Resources\Customer;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name, //$this->getName($request->header('lang')),
            "name_ar" => $this->name_ar, //$this->getName($request->header('lang')),
            "description" => $this->getDescription($request->header('lang')),
            "order" => $this->order,
            "parent_id" => $this->parent_id,
            "image" => $this->image,
            "attributes" => [],
            "sub_categories" =>  CategoryResource::collection($this->subCategories)
        ];
    }
}
