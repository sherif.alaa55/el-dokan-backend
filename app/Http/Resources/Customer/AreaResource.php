<?php

namespace App\Http\Resources\Customer;

use Illuminate\Http\Resources\Json\JsonResource;

class AreaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->getName($request->header('lang')),
            "name_ar" => $this->name_ar,
            "city_id" => $this->city_id,
            "districts" => DistrictResource::collection($this->districts),
            "active" => $this->active,
            "delivery_fees" => $this->delivery_fees,
            "aramex_area_name" => $this->aramex_area_name
        ];
    }
}
