<?php

namespace App\Notifications;

use App\Mail\StockNotifications;
use App\Notifications\Channels\CustomDbChannel;
use App\Notifications\Channels\FcmChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ProductAvailable extends Notification
{
    private $product;

    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($product)
    {
        $this->product = $product;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [CustomDbChannel::class, FcmChannel::class, 'mail'];
    }

    public function toDatabase($notifiable)
    {
        return [
            "user_id" => $notifiable->id,
            "type" => 5,
            "item_id" => $this->product->id,
            "read" => 0,
            "title" => trans('notifications.productAvailableTitle', ['product_name' => $this->product->name]),
            "title_Ar" => trans('notifications.productAvailableTitle', ['product_name' => $this->product->name], 'ar'),
            "body" => trans('notifications.productAvailableBody', ['product_name' => $this->product->name]),
            "body_ar" => trans('notifications.productAvailableBody', ['product_name' => $this->product->name], 'ar'),
        ];
    }

    public function toFcm($notifiable)
    {
        return [
            "user_id" => $notifiable->id,
            "type" => 5,
            "item_id" => $this->product->id,
            "title" => trans('notifications.productAvailableTitle', ['product_name' => $this->product->name], $notifiable->getLang()),
            "body" => trans('notifications.productAvailableBody', ['product_name' => $this->product->name], $notifiable->getLang()),
            "data" => null
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->subject("{$this->product->name} " . trans('email.isAvailable'))->view(
            'emails.stock_notifier', ["product" => $this->product, "user" =>$notifiable]
        );

    }
}
