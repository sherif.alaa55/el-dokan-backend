<?php

return [
    'Cash On Delivery' => [
        'id' => 1,
        'name_ar' => 'الدفع عند الاستلام',
        'is_online' => 0,
        'active' => 1,
        "icon" => 'images/payment_methods/cash_payment.png'
    ],
    'Credit / Debit card' => [
        'id' => 2,
        'name_ar' => 'بطاقة الائتمان \ خصم مباشر',
        'is_online' => 1,
        'active' => 1,
        "icon" => 'images/payment_methods/credit_payment.png'
    ],
    'ValU' => [
        'id' => 3,
        'name_ar' => 'فاليو',
        'is_online' => 1,
        'active' => 1,
        "icon" => 'images/payment_methods/valU_payment.png'
    ],
    'Fawry' => [
        'id' => 4,
        'name_ar' => 'فوري',
        'is_online' => 1,
        'active' => 0,
        "icon" => 'images/payment_methods/fawry.png'
    ],
    'Premium' => [
        'id' => 5,
        'name_ar' => 'بريميوم',
        'is_online' => 1,
        'active' => 1,
        "icon" => 'images/payment_methods/premium.png'
    ]
];
