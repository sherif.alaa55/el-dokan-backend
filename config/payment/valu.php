<?php

return [
    "store_id" => env('VALU_STORE_ID'),
    "aggregator_id" => env('VALU_AGGREGATOR_ID'),
    "user_name" => env('VALU_USER_NAME'),
    "vendor_name" => env('VALU_VENDOR_NAME')
];
