<?php

return [
    "authApiKey" => env('WE_ACCEPT_API_KEY'),
    "merchantID" => env('WE_ACCEPT_MERCHANT_ID'),
    "integrationID" => env('WE_ACCEPT_INTEGRATION_ID'),
    "iframeID" => env('WE_ACCEPT_IFRAME_ID'),
    "hmacHash" => env('WE_ACCEPT_HMAC_HASH'),
];
