<?php

return [
    "merchant_email" => env('PAYTAPS_MERCHANT_EMAIL'),
    "secret_key" => env('PAYTAPS_SECRET_KEY'),
    "site_url" => env('PAYTAPS_SITE_URL'),
    "live" => env('PAYTAPS_LIVE', false)
];
