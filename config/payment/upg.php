<?php

return [
    "merchant_id" => env('UPG_MERCHANT_ID'),
    "terminal_id" => env('UPG_TERMINAL_ID'),
    "secure_key" => env('UPG_SECURE_KEY'),
    "secure_key2" => env('UPG_SECURE_KEY2')
];
