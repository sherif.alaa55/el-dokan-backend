<?php

return [
	"success" => "تم بنجاح",

	"topPromotions" => "أفضل العروض",
	"mostRecent" => "المنتجات الأحدث",
	"mostBought" => "الأكثر شراءآ",

	"stateCreated" => "تم الطلب",
	"stateProcessing" => "جارى التحضير",
	"stateDelivering" => "جارى التسليم",
	"stateCompleted" => "مكتمل",

	"promoDeactivated" => "الرمز الترويجي غير مفعل",
	"promoExpired" => "الرمز الترويجي منتهي الصلاحية",
	"promoUsed" => "الرمز الترويجي مستخدم من قبل",
	"promoMissing" => "الرمز الترويجي غير موجود",

	"checkoutNotes" => "قد يتم تغيير مصاريف التوصيل",

	"linkSent" => "تم ارسال الرمز",
	"welcomeMessage" => "شكرآ ﻹستخدامك موبيلاتي يمكنك الآن التبضع و الحصول علي كل ما تحتاجه بسهولة فائقة. استمتع أيضآ بعروضنا و خصوماتنا الأسبوعية. مرحبآ بك فى  موبيلاتي 🔥",
	"welcomeMessageTitle" => "مرحبا بكم في موبيلاتي",

	"errorIncorrectPassword" => "يوجد خطأ بالإيميل او كلمة المرور التى سجلتها",
	"errorAccountDeactivated" => "لقد تم تعطيل حسابك..رجاءآ تواصل مع إدارة أسواق.كوم لكى نستطيع مساعتدك",
	"errorNameMin" => "الاسم يجب ان يكون 3 احرف علي الاقل",
	"errorEmailUsed" => "هذا الإيميل مسجل من قبل",
	"errorEmailNotFound" => "البريد الاكتروني غير مسجل",
	"errorPhoneUsed" => "رقم الهاتف مسجل من قبل",
	"errorIncorrectCode" => "رمز المرور خاطئ",
	"errorInvalidAddress" => "رجاء ادخال جميع الحقول",
	"errorPromoNotExist" => "هذا الكوبون غير صالح للإستخدام",
	"errorPromoExpired" => "هذا الكوبون منتهى الصلاحية",
	"errorPromoDeactivated" => "هذا الكوبون غير مفعل",
	"errorPromoAlreadyUsed" => "هذا الكوبون مستخدم من قبل",
	"errorPromoCartItem" => "عربة التسوق لا تحتوي علي المنتج الخاص بالكوبون",
	"errorPromoMinimumAmount" => "اجمالي الطلب اقل من الحد الادني للكوبون",
	"errorPromoFirstOrderOnly" => "الكوبون يطبق علي الطلب الاول فقط",
	"errorNoPhone" => "عليك تحديث التطبيق اولا لنتأكد من رقم هاتفك",
	"errorUpdateApp" => "برجاء تأكيد رقم الهاتف",
	"errorItemsNotExists" => "لا توجد منتجات ف السلة",
	"errorIncorrectVerification" => "كود التأكيد غير صحيح!",
	"errorPhoneVerified" => "رقم هاتفك مؤكد بالفعل",
	"errorMaxProduct" => "الحد الاقصي لطلب :product هو :quantity قطعة",
	"errorStoreClosed" => "شكراً لارسال طلبك، نحيطكم علماً بانه تم استلام الطلب و سنبدأ بالتجهيز في اقرب وقت علماً ان طلبكم سيتم توصيله في موعد اقصاه ٧٢ ساعه",
	"errorAramexLocation" => "هذا العوا خارج نطاق التوصيل حاليا",
];
