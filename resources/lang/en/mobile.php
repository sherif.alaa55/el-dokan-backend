<?php

return [
	"success" => "Success",

	"topPromotions" => "Top Pormotions",
	"mostRecent" => "Most Recent",
	"mostBought" => "Most Selling",

	"stateCreated" => "Created",
	"stateProcessing" => "Processing",
	"stateDelivering" => "Delivering",
	"stateCompleted" => "Completed",

	"promoDeactivated" => "Promo code deactivated",
	"promoExpired" => "Promo code expired",
	"promoUsed" => "You Already used this promo code",
	"promoMissing" => "Promo code does not exist",

	"checkoutNotes" => "Delivery fees may be changed",

	"linkSent" => "A reset link has been sent to your email",
	"welcomeMessage" => "Thanks for using mobilaty 🙂 You can shop all your product needs and enjoy our weekly promotions and offers 🔥",
	"welcomeMessageTitle" => "Welcome to mobilaty",

	"errorIncorrectPassword" => "Password or email are incorrect, kindly check them again.",
	"errorAccountDeactivated" => "Your account is deactivated, please contact the support.",
	"errorNameMin" => "Name must be at least 3 characters",
	"errorEmailUsed" => "This email is already used",
	"errorEmailNotFound" => "The email is not registered",
	"errorPhoneUsed" => "This phone number is already used",
	"errorIncorrectCode" => "Incorrect Code",
	"errorInvalidAddress" => "Please complete all fields",
	"errorPromoNotExist" => "Promo code does not exist",
	"errorPromoExpired" => "Promo code expired",
	"errorPromoDeactivated" => "Promo code deactivated",
	"errorPromoAlreadyUsed" => "You Already used this promo code",
	"errorPromoCartItem" => "Cart does not include products in this promo code.",
	"errorPromoMinimumAmount" => "Order amount doesn't meet the minumum amount for this promo code",
	"errorPromoMinimumAmountWithList" => "This promo for special items and it's amount doesn't meet the minumum amount for this promo code",
	"errorPromoFirstOrderOnly" => "Promo code is only applicable for first orders only",
	"errorNoPhone" => "You must update the app first so we can verify your phone number",
	"errorUpdateApp" => "please verify your phone",
    "errorItemsNotExists" => "There is no items in cart",
    "errorIncorrectVerification" => "Incorrect Verification Code!",
	"errorPhoneVerified" => "Your phone number is already verified",
	"errorMaxProduct" => "Max quantity for ordering :product is :quantity",
	"errorAramexLocation" => "We currently don\'t deliver to this address",
	"errorStoreClosed" => "Thank you for sending your order, we acknowledge that we received it
Please note it takes 72 hours maximum to deliver it.",
];
