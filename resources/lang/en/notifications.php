<?php

return [
	"productAvailableTitle" => "Product :product_name is not available",
	"productAvailableBody" => "Product :product_name is not available",

	"orderStateChangedTitle" => "Order Status Change",
	"orderStateChangedBody" => "Order #:order_id Status is now :order_state",

	"orderPlacedTitle" => "Order Created",
	"orderPlacedBody" => "Your order #:order_id is now :state_name",

	"orderEditedTitle" => "Order Edited",
	"orderEditedBody" => "Your Order #:order_id has been edited",

	"welcomeMessageTitle" => "Welcome to mobilaty",
	"welcomeMessageBody" => "Thanks for using mobilaty 🙂 You can shop all your product needs and enjoy our weekly promotions and offers 🔥",

	"cancelSms" => "Dear customer, your online order on mobilaty website has been cancelled, for inquiries call 19853",

    "cancelOrderTitle" => "Order ID :orderId is now cancelled",
    "cancelOrderBody" => "Order Cancelled",

    "completedOrderTitle" => "Your order number #:orderId has been completed.",
    "completedOrderBody" => "Order Completed",

    "preparedOrderTitle" => "Your order number #:orderId has been prepared.",
    "preparedOrderBody" => "Order Prepared",

    "deliveryOrderTitle" => "Your order number #:orderId is on its way.",
    "deliveryOrderBody" => "Order On Delivery",

    "deliveringOrderTitle" => "Order number #:orderId is now Delivering.",
    "deliveringOrderBody" => "Order Delivering",

    "placingOrderTitle" => "You have just placed order #:orderId.",
    "placingOrderBody" => "Placing Order",

    "placedOrderTitle" => "Your Scheduled order had been placed , you will receive it in the time you selected.Thanks for shopping with us.",
    "placedOrderBody" => "Order Reminder",

    "stockAvailableTitle" => ":productName is now available", "Product in Stock",
    "stockAvailableBody" => "Product in Stock"

];
