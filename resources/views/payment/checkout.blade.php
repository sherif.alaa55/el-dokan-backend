<html>
	
    <head> 
    
       <script src="https://qnbalahli.test.gateway.mastercard.com/checkout/version/49/checkout.js"
               	 data-error="errorCallback"
               	 data-cancel="cancelCallback"
                 data-complete="{{URL::to('api/customer/orders/receipt?token=' . $token)}}"
                >
       </script>

       <script type="text/javascript">
            function errorCallback(error) {
                window.location = '{!!env("WEBSITE_URL") . "/account/orders/{$order_id}?failed=1"!!}';
            }
           
            function cancelCallback() {
                window.location = '{!!env("WEBSITE_URL") . "/account/orders/{$order_id}?failed=1"!!}';
            }
       
       
			Checkout.configure({
                merchant: "{{$merchantId}}",
                order: {
                    amount: "{{json_encode($order_amount)}}",
                    currency: "{{($order_currency)}}",
                    description: 'Customer Id #{{$customerId}}',
                    id: "{{json_encode($order_id)}}",
                },
                interaction: {
                    merchant: {
                        name: 'El-Dokan Payment',
                        logo: '{{asset("images/logo.png")}}'
                    },
                    displayControl: {
                        billingAddress: 'HIDE',
                        customerEmail: 'HIDE',
                        orderSummary: 'HIDE',
                        shipping: 'HIDE'
                    },
                },
                session: {
                    id: "{{($session_id)}}"
                }
            });
            // Checkout.showPaymentPage();
            Checkout.showLightbox();
        </script>
    
    </head>
    
    <body>
    	  		
    </body>
</html>