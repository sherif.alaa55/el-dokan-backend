<?php

use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'ex_rate_pts' => 0,
            'ex_rate_egp' => 0,
            'ex_rate_gold' => 0,
            'egp_gold' => 0,
            'pending_days' => 0,
            'refer_points' => 0,
            'refer_minimum' => 0
        ]);
    }
}
