<?php

use App\Models\Payment\PaymentCredential;
use App\Models\Payment\PaymentMethod;
use Illuminate\Database\Seeder;

class PaymentMethods extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $paymentMethods = config('payment.all_methods');
        PaymentMethod::whereBetween('id', [0, 100000])->delete();
        foreach ($paymentMethods as $configName => $configMethod) {
            $method = PaymentMethod::firstOrCreate([
                'name' => $configName
            ], [
                'id' => $configMethod['id'],
                'name_ar' => $configMethod['name_ar'],
                'is_online' => $configMethod['is_online'],
                'icon' => $configMethod['icon'],
                'active' => $configMethod['active']
            ]);
            if(isset($configMethod['credentials'])) {
                foreach ($configMethod['credentials'] as $key => $credential) {
                    $credentialData = [
                        'method_id' => $method->id
                    ];
                    $defaultArr = [];
                    if(is_string($key)) {
                        $credentialData['name'] = $key;
                        $defaultArr['default'] = $credential;
                    } else {
                        $credentialData['name'] = $credential;
                    }
                    PaymentCredential::updateOrCreate($credentialData, $defaultArr);
                }
            }
            $this->command->info("{$configName} -> seeded");
        }
    }
}
