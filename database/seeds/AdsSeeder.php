<?php

use Illuminate\Database\Seeder;

class AdsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ads')->insert([
            ["image" => "http://via.placeholder.com/328x177", "banner_ad" => 1, "created_at" => date("Y-m-d"), "active" => 1, 'type' => 3],
            ["image" => "http://via.placeholder.com/328x177", "banner_ad" => 1, "created_at" => date("Y-m-d"), "active" => 1, 'type' => 3],
            ["image" => "http://via.placeholder.com/328x177", "banner_ad" => 1, "created_at" => date("Y-m-d"), "active" => 1, 'type' => 3],
            ["image" => "http://via.placeholder.com/328x177", "banner_ad" => 1, "created_at" => date("Y-m-d"), "active" => 1, 'type' => 3],
            ["image" => "http://via.placeholder.com/328x177", "banner_ad" => 1, "created_at" => date("Y-m-d"), "active" => 1, 'type' => 3],
            ["image" => "http://via.placeholder.com/328x177", "banner_ad" => 1, "created_at" => date("Y-m-d"), "active" => 1, 'type' => 3]
        ]);
    }
}
