<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQtyPromotions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('promotions', function (Blueprint $table) {
            $table->integer("qty")->unsigned();
            $table->integer("list_id")->unsigned()->nullable();
            $table->foreign("list_id")->references("id")->on('lists')->onDelete("cascade");
            $table->integer("brand_id")->unsigned()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('promotions', function (Blueprint $table) {
            $table->dropColumn("qty");
            $table->dropForeign("promotions_list_id_foreign");
            $table->dropColumn("list_id");
        });
    }
}
