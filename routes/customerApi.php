<?php

use App\Jobs\TestBeans;
use App\Models\Locations\City;
use App\Models\Orders\Order;
use App\Models\Services\SmsService;
use App\Models\Users\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;


Route::get('pay/{provider}/responseCallback', 'OrdersController@responseCallBack');
Route::post('pay/{provider}/processesCallback', 'OrdersController@processesCallBack');
// authentication and registraion api's
Route::post("auth", "AuthController@login");
Route::post("auth/logout", "AuthController@logout");
Route::post("auth/guest", "AuthController@guest");
Route::post("auth/signup", "AuthController@register");
Route::post("auth/social", "AuthController@socialAuth");
Route::post("auth/social_signup", "AuthController@socialRegister");

// forget password api's
Route::post("auth/forget_password", "AuthController@forgetPassword");
Route::post("auth/validate_code", "AuthController@validateCode");
Route::post("auth/reset_password", "AuthController@resetPassword");

Route::post("newsletter", "AuthController@subscribeNewsletter");
Route::post("contact_us", "AuthController@contactUs");



Route::group(["middleware" => ["jwt.auth", "active"]], function () {

    Route::group(["middleware" => "useSession"], function () {
        Route::get("orders/pay_order", "OrdersController@payOrder");
        Route::get("orders/receipt", "OrdersController@orderReceipt");
    });

    Route::get("ads", "AdsController@index");

    Route::get("page/{slug}", "PagesController@getPageDetails");
    Route::get("page", "PagesController@getAllPages");

    Route::get("stores", "StoresController@getAllStores");

    Route::get("home", "HomeController@index");
    Route::get("test", "HomeController@test");
    Route::get("home2", "HomeController@newIndex");
    Route::get("home/initialize", "HomeController@initialize");
    Route::get("lists/{id}", "ListsController@index");

    Route::get("notifications", "NotificationsController@index");
    Route::post("notifications/read", "NotificationsController@markRead");

    Route::get('rewards', "RewardsController@getRewards");
    Route::post('rewards/redeem', "RewardsController@redeemReward");

    // profile edit
    Route::get("profile/points_history", "ProfileController@getPointHistory");
    Route::get("profile", "ProfileController@getProfile");
    Route::post("profile/edit", "ProfileController@editProfile");
    Route::post("profile/edit_phone", "ProfileController@editPhone");
    Route::post("profile/resend_verification", "ProfileController@resendVerification");
    Route::post("profile/change_password", "ProfileController@changePassword");
    Route::post("profile/verify_phone", "ProfileController@verifyPhone");
    Route::post("profile/add_token", "ProfileController@addToken");

    // addresses api's
    Route::post("profile/addresses", "ProfileController@createAddress");
    Route::post("profile/addresses/{id}", "ProfileController@updateAddress");
    Route::post("profile/addresses/remove/{id}", "ProfileController@removeAddress");
    Route::post("profile/addresses/primary/{id}", "ProfileController@setPrimaryAddress");

    // settings api's
    Route::post("profile/notification_settings", "ProfileController@updateNotificationSettings");
    Route::post("profile/language_settings", "ProfileController@updateLanguageSettings");

    // products
    Route::get("products/home", "ProductsController@home");
    Route::get("categories/products/{id}", "CategoriesController@getProducts");
    Route::get("products/search", "ProductsController@search");
    Route::post("products/search", "ProductsController@search_products");
    Route::get("products/comparisons", "ProductsController@myComparisons");
    Route::post("products/set_compare/{id}", "ProductsController@setCompare");
    Route::post("products/remove_compare/{id}", "ProductsController@removeCompare");
    Route::post("products/set_products_compare", "ProductsController@setProductsCompare");
    Route::post("products/remove_products_compare", "ProductsController@removeProductsCompare");
    Route::get("products/favourites", "ProductsController@myFavourites");
    Route::get("products/top_promotions", "ProductsController@topPromotions");
    Route::get("products/most_bought", "ProductsController@mostBought");
    Route::get("products/{id}", "ProductsController@show");
    Route::post("products/favourite/{id}", "ProductsController@favourite");
    Route::post("products/add_products_favourite", "ProductsController@addProductsToFavourite");
    Route::post("products/remove_favourites", "ProductsController@removeAllFavourites");
    Route::post("products/unfavourite/{id}", "ProductsController@unfavourite");
    Route::post("products/notify/{id}", "ProductsController@notifyAvailability");


    Route::post("products/review", "ProductReviewsController@addRate");

    // Medical
    Route::get("prescriptions", "PrescriptionsController@index");
    Route::post("prescriptions", "PrescriptionsController@storeBase64");
    Route::post("prescriptions/store_multipart", "PrescriptionsController@store");

    // Orders api's
    Route::any("orders/delivery_fees", "OrdersController@getDeliveryFees");
    Route::get("orders", "OrdersController@index");
    Route::get("orders/{id}", "OrdersController@show");
    Route::post("orders/check_promo", "OrdersController@checkPromo");
    Route::post("orders/check_promo_v2", "OrdersController@checkPromoV2");
    Route::post("orders", "OrdersController@createOrder")->middleware('active');
    Route::post("orders/validate_cart", "OrdersController@validateCart");
    Route::post("orders/rate/{id}", "OrdersController@rateOrder");
    Route::post("orders/cancel/{id}", "OrdersController@cancelOrder");
    Route::post("orders/edit_late/{id}", "OrdersController@editLateSchedule");
    Route::post("orders/cancel_late/{id}", "OrdersController@cancelLateSchedule");
    Route::post("orders/edit_schedule/{id}", "OrdersController@editSchedule");
    Route::post("orders/cancel_schedule/{id}", "OrdersController@cancelSchedule");
    Route::post("valu/get_plans", "ValuController@getPlans");
    Route::post("valu/verify", "ValuController@verifyCustomer");
    Route::resource('carts', 'CartController');
    Route::get("order_cancellation_reasons", "OrderCancellationReasonController@index");


    Route::post("upload", "UploadsController@upload");
});
